$(function(){
    var files = [];
    var countOpenedFiles = 1;
    $('.files-list').resizable({
        handles:'e'
    });
    var load = function(){
        var self = this;
        if ($(this).parent().hasClass('open')){
            $(this).parent().removeClass('open');
            $(this).next().slideUp(500);
        }   else    {
            getFiles($(this).parent().attr('data-path'), function (files) {
                var e = $(self).next();
                var html = "<div class='file-list-folder'>";
                for (var i = 0; i < files.length; i++) {
                    html += "<div data-path='" + $(self).parent().attr('data-path') + '/' + files[i].name + "' class='file-list-file " + files[i].type + "'>" + files[i].name + "</div>";
                }
                html += "</div>";
                e.html(html).slideDown(500);
                $(self).parent().addClass('open');
            });
        }
    };
    $('.files-list .panel-heading').click(load);
    var getFiles = function(path, callback){
        if (files[path]==undefined){
            admin.ajax('Core/get_files',{path:path},function(res){
                var data = res.DATA;
                files[path] = data;
                getFiles(path,callback);
            });
            return;
        }
        callback(files[path]);
    }
    $('.files-list').on('click','.file-list-file',function(e){
        e.stopPropagation();
        var self = this;
        if ($(this).hasClass('file')){
            admin.ajax('Core/get_file_data',{path:$(this).attr('data-path')},function(res){
                var data = res.DATA;
                $('.files-editor-open').append('<li><a href="#file' + countOpenedFiles + '" data-toggle="tab" title="' + $(self).attr('data-path') +'">' + data.name + '</a></li>');
                $('.files-editor-open>li.active').removeClass('active');
                $('.files-editor-open>li').last().addClass('active');

                $('.file-editor-panel .tab-content').append('<div class="tab-pane active" id="file' + countOpenedFiles + '"><div class="editor" id="editor' + countOpenedFiles + '"></div><div class="file-buttons"><input type="button" class="btn btn-primary" value="Сохранить"></div></div>');
                $('.file-editor-panel .tab-content .tab-pane.active').removeClass('active');

                var editor1 = ace.edit("editor" + countOpenedFiles);
                editor1.setValue(data.data);
                editor1.getSession().on('change',function(e){
                    console.log('change');
                });
                $('.file-editor-panel .tab-content .tab-pane').last().addClass('active').data('editor',editor1);

                if (data.extension=='php'){
                    editor1.getSession().setMode('ace/mode/php');
                }   else if (data.extension=='js'){
                    editor1.getSession().setMode('ace/mode/javascript');
                }   else if (data.extension=='html'){
                    editor1.getSession().setMode('ace/mode/html');
                }   else if (data.extension=='css'){
                    editor1.getSession().setMode('ace/mode/css');
                }

                countOpenedFiles++;
            });
            return;
        }   else if($(this).hasClass('dir')){
            var self = this;
            if ($(this).hasClass('open')){
                $(this).removeClass('open');
                $(this).children('.file-list-folder').slideUp(500);
            }   else    {
                getFiles($(this).attr('data-path'), function (files) {
                    var e = $(self);
                    var path = '';
                    if ($(self).attr('data-path')!='/') path = $(self).attr('data-path');
                    if ($(self).children('.file-list-folder').length==0){
                        var html = e.html()+"<div class='file-list-folder' style='display:none'>";
                        for (var i = 0; i < files.length; i++) {
                            html += "<div data-path='" + path + '/' + files[i].name + "' class='file-list-file " + files[i].type + "'>" + files[i].name + "</div>";
                        }
                        html += "</div>";
                        e.html(html);
                    }
                    $(self).addClass('open');
                    e.children('.file-list-folder').slideDown(500);
                });
            }
        }
    });
});