@if(env('APP_DEBUG','false')=='true')
    @include('core.debug-css')
    @include('core.debug-js')
@endif
@extends('/templates/'.$template.'/main')
@include('site/'.$view)