<div class="modal fade" id="modal-load-files" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-close" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Загрузка файлов</h4>
            </div>
            <div class="modal-body">
                <div class="files">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal-close">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    /*

     <div class="row">
     <label for="">Файл 1 <span>(ожидание загрузки)</span></label>
     <div class="progress progress-striped active">
     <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
     <span class="sr-only">45% Complete</span>
     </div>
     </div>
     </div>
     */
    var onRegisterModalLoadFiles = function(){
        var self = this;
        var files = [];
        var folder_id = -1;
        self.startLoad = function(formData){
            self.isError = false;
            self.isAllLoad = false;
            files = $(formData).find('input[type=file]').get(0).files;
            folder_id = $(formData).find('input[type=hidden]').val();
            $('#modal-load-files .files').html("");
            self.load(0);
            for (var i = 0; i<files.length; i++){
                var file = files[i];
                $('#modal-load-files .files').append(''+
                    '<div class="row">'+
                        '<label>' + file.name +'</label>'+
                        '<div class="progress progress-striped active">'+
                            '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            }
        };
        self.load = function(file_id){
            if (!files[file_id]){
                self.isAllLoad = true;
                if (!self.isError) self.modal('hide');
                return;
            }
            var fd = new FormData();
            fd.append('file',files[file_id]);
            fd.append('folder_id',folder_id);
            fd.append('_token','{{csrf_token()}}');
            $.ajax({
                url:'/ajax/Files/load_files',
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                },
                data: fd,
                method:'post',
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.addEventListener('progress',function(e){
                        if (e.lengthComputable){
                            var percentComplete = Math.ceil(e.loaded/e.total*100);
                            $('#modal-load-files .files .progress-bar').eq(file_id).css('width',percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function(res){
                    res = JSON.parse(res);
                    $('#modal-load-files .files .progress').eq(file_id).removeClass('active').removeClass('progress-striped');
                    $('#modal-load-files .files .progress-bar').eq(file_id).css('width','100%').addClass('progress-bar-' + (res['ERROR']?'danger':'success'));
                    self.load(file_id+1);
                    self.isError = self.isError || res['ERROR'];
                }
            });
        }
    };
</script>