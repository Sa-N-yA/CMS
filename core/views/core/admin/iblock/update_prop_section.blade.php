@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Изменение свойства раздела для инфоблока {{$iblock->name}}</div>
        <div class="panel-body">
            <form class="form admin-form-ajax" id="create-section-prop-form" role="form" method="POST" data-action="IBlock/update_section_prop">
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">ID:</label>
                    <div id="id" class="col-md-6">
                        <input type="text" readonly class="form-control" name="id" value="<?=$prop->id?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название свойства</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="<?=$prop->name?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" readonly class="form-control" name="alias" value="<?=$prop->alias?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-md-4 control-label">Тип свойства</label>
                    <div id="type" class="col-md-6">
                        <select class="form-control" readonly name="type" value="">
                            <option value="">Выберите тип</option>
                            @foreach($type_props as $key=>$value)
                                <option value="{{$key}}"
                                @if($prop->type==$key)
                                selected
                                @endif>{{$value['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Параметры</label>
                    <div class="checkbox">
                        <label for="required" class="col-md-4 control-label">Обязательно для заполнения
                            <input type="hidden" name="required" value="0">
                            <input type="checkbox"{{($prop->required?' checked':'')}} name="required" value="1">
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="many" class="col-md-4 control-label">Множественное
                            <input type="hidden" name="many" value="0">
                            <input type="checkbox"{{($prop->many?' checked':'')}} name="many" value="1">
                        </label>
                    </div>
                </div>
                @foreach($type_props as $key=>$value)
                    <div class="iblock_prop_config"
                         @if($prop->type==$key)
                            style="display:block"
                         @endif
                         id="iblock_prop_config_{{$key}}">
                        @foreach($value['config'] as $keyVal=>$conf)
                            <div class="form-group">
                                <label for="{{$key}}_{{$keyVal}}" class="col-md-4 control-label">{{$conf['name']}}</label>
                                <div id="{{$key}}_{{$keyVal}}" class="col-md-6">
                                @if($conf['type']=='number')
                                    <input type="number" value="{{$prop->config[$keyVal]}}" class="form-control" name="{{$key}}_{{$keyVal}}"/>
                                @elseif($conf['type']=='array')
                                    <select class="form-control" name="{{$key}}_{{$keyVal}}">
                                        @foreach($conf['values'] as $key=>$value)
                                            <option{{$prop->config[$keyVal]==$key?' selected':''}} value="{{$key}}">{{$value['name']}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            $('select[name=type]').change(function(){
                $('.iblock_prop_config').hide();
                $('#iblock_prop_config_' + $(this).val()).show();
            });
            admin.registerFormAjax($('#create-section-prop-form'),function(res){
                location.href = "/admin/iblock/{{$iblock->id}}/list_props_section";
            },function(){
            });
        });
    </script>
@endsection
