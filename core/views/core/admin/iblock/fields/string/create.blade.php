<div class="form-group">
    <label for="string_type" class="col-md-4 control-label">Тип строки</label>
    <div id="string_type" class="col-md-6">
        <select class="form-control" name="string_type">
            @foreach(App\Http\Controllers\Core\Modules\IBlockFields\StringFieldController::$types as $key=>$value)
                <option value="{{$key}}" {{isset($values['string_type']) && $values['string_type'] == $key?'selected':''}}>{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="string_min" class="col-md-4 control-label">Минимальная длина</label>
    <div id="string_min" class="col-md-6">
        <input type="number" class="form-control" name="string_min" value="{{isset($values['string_min'])?$values['string_min']:''}}"/>
    </div>
</div>
<div class="form-group">
    <label for="string_max" class="col-md-4 control-label">Максимальная длина</label>
    <div id="string_max" class="col-md-6">
        <input type="number" class="form-control" name="string_max" value="{{isset($values['string_max'])?$values['string_max']:''}}"/>
    </div>
</div>