<div class="form-group">
    <label for="iblock_item_type" class="col-md-4 control-label">Инфоблок</label>
    <div id="iblock_item_type" class="col-md-6">
        <select class="form-control" name="iblock_item_type">
            @foreach(App\Http\Controllers\Core\Modules\IBlockFields\IBlockItemFieldController::$types as $key=>$value)
                <option value="{{$key}}" {{isset($values['iblock_item_type']) && $values['iblock_item_type'] == $key?'selected':''}}>{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>