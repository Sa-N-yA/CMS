@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание свойства раздела для инфоблока {{$iblock->name}}</div>
        <div class="panel-body">
            <form class="form admin-form-ajax" id="create-section-prop-form" role="form" method="POST" data-action="IBlock/create_section_prop">
                <input type="hidden" name="iblock_id" value="{{$iblock->id}}"/>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название свойства</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-md-4 control-label">Тип свойства</label>
                    <div id="type" class="col-md-6">
                        <select class="form-control" name="type" value="">
                            <option value="">Выберите тип</option>
                            @foreach($props as $prop)
                                <option value="{{$prop->getAlias()}}">{{$prop->getName()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Параметры</label>
                    <div class="checkbox">
                        <label for="require" class="col-md-4 control-label">Обязательно для заполнения
                            <input type="hidden" name="required" value="0">
                            <input type="checkbox" name="required" value="1">
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="many" class="col-md-4 control-label">Множественное
                            <input type="hidden" name="many" value="0">
                            <input type="checkbox" name="many" value="1">
                        </label>
                    </div>
                </div>
                @foreach($props as $prop)
                    <div class="iblock_prop_config" id="iblock_prop_config_{{$prop->getAlias()}}">
                        @include($prop->getFormCreateView(),['active'=>false])
                    </div>
                @endforeach
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Создать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            $('select[name=type]').change(function(){
                $('.iblock_prop_config').hide();
                $('#iblock_prop_config_' + $(this).val()).show();
            });
            admin.registerFormAjax($('#create-section-prop-form'),function(res){
                location.href = "/admin/iblock/{{$iblock->id}}/list_props_section";
            },function(){
            });
        });
    </script>
@endsection
