@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание инфоблока</div>
        <div class="panel-body">
            <form class="form admin-form-ajax" role="form" method="POST" data-action="IBlock/update_iblock">
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">ID:</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="id" value="<?=$iblock->id?>" readonly>
                    </div>
                </div><div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название инфоблока</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="<?=$iblock->name?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="<?=$iblock->alias?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerFormAjax($('.admin-form-ajax'),function(res){
                location.href = "/admin/iblock/list";
            },function(){

            });
        });
    </script>
@endsection