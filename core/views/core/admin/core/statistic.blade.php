@extends('core.admin.layout')

@section('content')
<div id="flotr">

</div>

<script>
    $(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
        var s1 = [{!!$values!!}];
        var ticks = [{!!$keys!!}];

        plot1 = $.jqplot('flotr', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8).
            animate: false,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    });
</script>
<style>
    body{
        overflow-x: hidden;
    }
</style>
@endsection
