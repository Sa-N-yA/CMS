@extends('core.admin.layout')

@section('content')

    <div class="panel panel-default{{!$isMakeBackup?'':' hidden'}}" id="backup-non-making" style="width: 34%;float: left;margin-right: 2%;">
        <div class="panel-heading">
            <h3 class="panel-title">Создание новой резервной копии</h3>
        </div>
        <form class="admin-form-ajax" id="form-make-backup" data-action="Backup/run">
            <div class="panel-body">
                <div class="form-group">
                    <label for="">Список исключений для файлов</label>
                    <div class="input_ignore_block">
                        <input type="text" class="form-control input_ignore" value="/core/storage" name="ignore_1">
                        <input type="text" class="form-control input_ignore" name="ignore_2">
                        <input type="text" class="form-control input_ignore" name="ignore_3">
                    </div>
                    <input data-count="3" type="button" class="btn btn-default add_ignore" value="Еще">
                </div>
                <div class="form-group">
                    <input type="hidden" name="make_db" value="0">
                    <label for="make_db"><input name="make_db" value="1" checked type="checkbox">Создавать резервную копию базы данных</label>
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-primary pull-right" value="Создать"/>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
    <div class="panel panel-default{{$isMakeBackup?'':' hidden'}}" id="backup-making" style="width: 34%;float: left;margin-right: 2%;">
        <div class="panel-heading">
            <h3 class="panel-title">Создание новой резервной копии</h3>
        </div>
        <div class="panel-body">
            <label class="general-step">Шаг 1. Инициализация</label>
            <div class="progress progress-striped active progress-general">
                <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                </div>
            </div>
            <label class="files-step hidden">Заархивировано файлов: 500. Всего файлов: 5400</label>
            <div class="progress progress-striped active progress-file hidden">
                <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default hidden" id="backup-making-success" style="width: 34%;float: left;margin-right: 2%;">
        <div class="panel-heading">
            <h3 class="panel-title">Создание новой резервной копии</h3>
        </div>
        <div class="panel-body">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>
                Резервная копия была успешно создана
            </div>
        </div>
    </div>
    <div class="panel panel-default" style="width: 64%;float: left;">
        <div class="panel-heading">
            <h3 class="panel-title">Список резервных копий</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 15%;">Название</th>
                    <th style="width: 15%">Размер</th>
                    <th style="width: 15%">Дата создания</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @foreach($backups as $backup)
                    <tr>
                        <td>{{$backup['id']}}</td>
                        <td><a href="/admin/core/backups/{{$backup['name']}}/download"
                               download="true">{{$backup['name']}}</a></td>
                        <td>{{$backup['size']}}</td>
                        <td>{{$backup['date']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                        data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/core/backups/{{$backup['name']}}/download">Скачать</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <script>
        var stepListenBackup = function(){
            admin.ajax('Backup/status', {}, function(res){
                var steps = {
                    start: {text:'Шаг 1. Инициализация', number: 0},
                    database: {text:'Шаг 2. Создание резервной копии базы данных', number: 2},
                    count: {text:'Шаг 3. Подсчет количества файлов для архивирования', number: 3},
                    file: {text:'Шаг 4. Архивирование файлов', number: 4}
                };
                var data = res.DATA;
                if (data.length == 0){
                    stepListenBackup();
                    return;
                }
                if (data.status=='success'){
                    $('#backup-making').addClass('hidden');
                    $('#backup-making-success').removeClass('hidden');
                    return;
                }   else    {
                    $('.general-step').html(steps[data.status].text);
                    $('.progress-general .progress-bar').css('width', steps[data.status].number*20 + '%');
                    if (data.status=='file'){
                        $('.progress-file .progress-bar').css('width', (data.ready/data.count*100) + '%');
                        $('.files-step').html('Заархивировано файлов: ' + data.ready + '. Всего файлов: ' + data.count);
                        $('.files-step').removeClass('hidden');
                        $('.progress-file').removeClass('hidden');
                    }   else    {
                        $('.files-step').addClass('hidden');
                        $('.progress-file').addClass('hidden');
                    }
                }
                stepListenBackup();
            });
        };
        $(document).ready(function(){
            $('.add_ignore').click(function(){
                var count = $(this).attr('data-count');
                count++;
                $(this).attr('data-count',count);
                $('.input_ignore_block').append('<input type="text" class="form-control input_ignore" name="ignore_' + count + '">');
            });
            $('#backup-making-success .close').click(function(){
                location.reload();
            });
            admin.registerFormAjax($('#form-make-backup'),function(){
                $('#backup-non-making').addClass('hidden');
                $('#backup-making').removeClass('hidden');
                stepListenBackup();
            },function(){
            });
        });
        @if ($isMakeBackup)
        stepListenBackup();
        @endif
    </script>
@endsection