@include($view)

@if (isset($sections))
    @foreach($sections as $section)
        @yield($section)
    @endforeach
@endif