<?php
 return array (
  'versionName' => '1.3.0',
  'version' => '8',
  'server' => 'http://cms.webmaster38.ru/',
  'modules' => 
  array (
    'Core' => 
    array (
      'enabled' => true,
      'alias' => 'core',
      'name' => 'Система',
    ),
    'Site' => 
    array (
      'enabled' => true,
      'alias' => 'site',
      'name' => 'Сайт',
    ),
    'IBlock' => 
    array (
      'enabled' => true,
      'alias' => 'iblock',
      'name' => 'Информационные блоки',
    ),
    'Users' => 
    array (
      'enabled' => true,
      'alias' => 'users',
      'name' => 'Пользователи',
    ),
    'WebForms' => 
    array (
      'enabled' => true,
      'alias' => 'webforms',
      'name' => 'Веб-формы',
    ),
  ),
  'cache' => true,
) ;