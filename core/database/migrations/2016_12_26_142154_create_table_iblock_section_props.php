<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIBlockSectionProps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iblock_section_props', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iblock_id')->index();
            $table->string('name');
            $table->string('alias')->index();
            $table->string('type');
            $table->enum('required',['0','1']);
            $table->enum('many',['0','1']);
            $table->text('config');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
