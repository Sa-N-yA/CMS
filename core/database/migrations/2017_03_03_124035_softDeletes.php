<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('iblock_items', function(Blueprint $table)
        {
            $table->softDeletes();
            $table->dateTime('active_from')->nullable()->default(null);
            $table->dateTime('active_to')->nullable()->default(null);
        });
        Schema::table('iblock_sections', function(Blueprint $table)
        {
            $table->softDeletes();
            $table->dateTime('active_from')->nullable()->default(null);
            $table->dateTime('active_to')->nullable()->default(null);
        });
        Schema::table('iblock', function(Blueprint $table)
        {
            $table->softDeletes();
        });
        Schema::table('files', function(Blueprint $table)
        {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('iblock', function(Blueprint $table){
            $table->dropSoftDeletes();
        });
        Schema::table('iblock_items', function(Blueprint $table){
            $table->dropSoftDeletes();
            $table->dropColumn('active_from');
            $table->dropColumn('active_to');
        });
        Schema::table('iblock_sections', function(Blueprint $table){
            $table->dropSoftDeletes();
            $table->dropColumn('active_from');
            $table->dropColumn('active_to');
        });
        Schema::table('files', function(Blueprint $table){
            $table->dropSoftDeletes();
        });
    }
}
