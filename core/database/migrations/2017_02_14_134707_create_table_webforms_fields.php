<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWebformsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webforms_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('webform_id')->index();
            $table->string('name');
            $table->string('alias')->unique();
            $table->enum('type',['number','string','text','visual_text']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webforms_fields', function (Blueprint $table) {
            //
        });
    }
}
