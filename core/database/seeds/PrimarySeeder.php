<?php

use Illuminate\Database\Seeder;

class PrimarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 16; $i<1000000; $i++){
            DB::table('iblock_section_props_values')->insert([
                'prop_id'=>7,
                'value'=>rand(1,5)==1?'develop5@masproject.pro':str_random(16),
                'section_id'=>$i
            ]);
        }
    }
}
