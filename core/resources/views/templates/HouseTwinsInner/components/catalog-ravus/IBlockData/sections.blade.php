<section class="section section_2">
    <div class="b-inner">
        <h3 class="section__title wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
            <span class="title__yellow">
                {{@$result['section']['name']}}
            </span>
        </h3>

        <div class="products">
            @foreach($result['items'] as $item)
                <a href="/ravus/{{@$result['section']['alias']}}/{{$item['alias']}}/" class="products__item wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="products__image">
                        <img src="{{@$result['files'][$item['image']]['full']}}" alt="...">
                    </div>
                    <div class="products__title">
                        {{$item['name']}}
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</section>