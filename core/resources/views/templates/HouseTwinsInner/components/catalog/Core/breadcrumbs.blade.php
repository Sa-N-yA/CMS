<div class="breadcrumb">
    @foreach($result as $item)
        <div class="breadcrumb-item">
            <a href="{{$item['link']}}" title="{{$item['title']}}">
                {{$item['title']}}
            </a>
        </div>
    @endforeach
</div>