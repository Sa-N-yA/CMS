<ul class="list-submenu menu-fixed js-stick">
    @foreach($result['items'] as $item)
    <li class="list-submenu__item">
        <a href="{{$item['link']}}" class="list-submenu__link">
            {{$item['name']}}
        </a>
    </li>
    @endforeach
</ul>