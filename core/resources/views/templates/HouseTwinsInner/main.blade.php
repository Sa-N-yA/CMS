@extends('templates.HouseTwins.main')
@section('inner-menu')
    @component([
        'name'=>'Core:Menu',
        'template'=>'inner-menu',
        'params'=>[
            'type'=>'Pages',
            'ignore'=>['/']
        ]
    ])
@endsection
@section('b_menu_class',' b-menu_relative')