<div class="clients-wrapper wow fadeInUp" data-wow-delay=".2s">
    <div class="clients-text">
        Качество работы модельеров ателье House Twins уже оценили известные люди и компании города Иркутска!
    </div>
    <div class="slider-cloth js-slick-clients">
        <!--slide-->
        @for($i=0; $i<count($result['items']); $i+=4)
            <div>
                <div class="clients">
                    <div class="stretch">
                        <div class="stretch__item">
                            @if (isset($result['items'][$i]))
                                <div class="clients__item">
                                    <div class="clients__image">
                                        <img src="{{@$result['files'][$result['items'][$i]['photo']]['full']}}" alt="..." />
                                    </div>
                                    <div class="clients__desc">
                                        <div class="clients__rating">
                                            <span class="clients__star{{$result['items'][$i]['rating']<1?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i]['rating']<2?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i]['rating']<3?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i]['rating']<4?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i]['rating']<5?'':' active'}}"></span>
                                        </div>
                                        <h5 class="clients__name">
                                            {{$result['items'][$i]['name']}}
                                        </h5>
                                        <div class="clients__text">
                                            {{$result['items'][$i]['text']}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if (isset($result['items'][$i+1]))
                                <div class="clients__item">
                                    <div class="clients__image">
                                        <img src="{{@$result['files'][$result['items'][$i+1]['photo']]['full']}}" alt="..." />
                                    </div>
                                    <div class="clients__desc">
                                        <div class="clients__rating">
                                            <span class="clients__star{{$result['items'][$i+1]['rating']<1?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+1]['rating']<2?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+1]['rating']<3?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+1]['rating']<4?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+1]['rating']<5?'':' active'}}"></span>
                                        </div>
                                        <h5 class="clients__name">
                                            {{$result['items'][$i+1]['name']}}
                                        </h5>
                                        <div class="clients__text">
                                            {{$result['items'][$i+1]['text']}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="stretch__item">
                            @if (isset($result['items'][$i+2]))
                                <div class="clients__item">
                                    <div class="clients__image">
                                        <img src="{{@$result['files'][$result['items'][$i+2]['photo']]['full']}}" alt="..." />
                                    </div>
                                    <div class="clients__desc">
                                        <div class="clients__rating">
                                            <span class="clients__star{{$result['items'][$i+2]['rating']<1?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+2]['rating']<2?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+2]['rating']<3?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+2]['rating']<4?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+2]['rating']<5?'':' active'}}"></span>
                                        </div>
                                        <h5 class="clients__name">
                                            {{$result['items'][$i+2]['name']}}
                                        </h5>
                                        <div class="clients__text">
                                            {{$result['items'][$i+2]['text']}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if (isset($result['items'][$i+3]))
                                <div class="clients__item">
                                    <div class="clients__image">
                                        <img src="{{@$result['files'][$result['items'][$i+3]['photo']]['full']}}" alt="..." />
                                    </div>
                                    <div class="clients__desc">
                                        <div class="clients__rating">
                                            <span class="clients__star{{$result['items'][$i+3]['rating']<1?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+3]['rating']<2?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+3]['rating']<3?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+3]['rating']<4?'':' active'}}"></span>
                                            <span class="clients__star{{$result['items'][$i+3]['rating']<5?'':' active'}}"></span>
                                        </div>
                                        <h5 class="clients__name">
                                            {{$result['items'][$i+3]['name']}}
                                        </h5>
                                        <div class="clients__text">
                                            {{$result['items'][$i+3]['text']}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>