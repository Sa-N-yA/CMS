<ul class="list-menu-bottom list-menu-bottom_hide-mobile js-list-menu">
    @foreach($result['items'] as $item)
        <li class="list-menu-bottom__item">
            <a href="{{$item['link']}}" class="list-menu-bottom__link">
                {{$item['name']}}
            </a>
        </li>
    @endforeach
</ul>