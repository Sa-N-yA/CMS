<div class="jcarousel">
    <ul>
        @foreach($result['items'] as $item)
            <li class="jcarousel__slide" style="background: url({{@$result['files'][$item['image']]['full']}});">
                <div class="b-inner_slider">
                    <div class="slider-text-1 wow fadeInDown">
                        <span>{{$item['name']}}</span>
                    </div>
                    <div class="slider-text-2 wow fadeInUp" data-wow-delay=".2s">
                        {{$item['subtitle']}}
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <a href="#" class="jcarousel-control-prev"></a>
    <a href="#" class="jcarousel-control-next"></a>
</div>