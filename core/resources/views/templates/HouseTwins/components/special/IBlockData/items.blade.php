<div class="special">
    <h3 class="section__title wow fadeInUp">
        <span class="title__yellow">
            Спецпредложения
        </span>
    </h3>
    <div class="special__content">
        @foreach($result['items'] as $item)
            <a href="#" class="special__item wow fadeIn" data-wow-delay="1.2s">
                <div class="special__hover">
                    <div>
                        Назначить<br />
                        время визита
                    </div>
                </div>
                <div class="special__desc">
                    <h4 class="special__title">
                        {{$item['name']}}
                    </h4>
                    <div class="special__price">
                        {{$item['new_price']}} руб.
                    </div>
                    <div class="special__oldprice">
                        {{$item['old_price']}} руб.
                    </div>
                </div>
                <div class="special__image">
                    <img src="{{$result['files'][$item['image']]['full']}}" alt="..." />
                </div>
            </a>
        @endforeach
    </div>
</div>