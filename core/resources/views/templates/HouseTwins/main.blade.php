<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	@yield('head')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="/css/style.css" />
	<link rel="stylesheet" href="/css/fancybox/fancybox.css" />
	<link rel="stylesheet" href="/css/jcarousel.responsive.css" />
	<link rel="stylesheet" href="/css/slick.css" />
	<link rel="stylesheet" href="/css/slick-theme.css" />
	<link rel="stylesheet" href="/css/animate.css" />

	<link rel="icon" type="image/png" href="img/favicon.png" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="/js/jquery.fitvids.js"></script>
	<script src="/js/jquery.sticky-kit.min.js"></script>
	<script src="/js/jquery.jcarousel.min.js"></script>
	<script src="/js/jcarousel.responsive.js"></script>
	<script src="/js/slick.min.js"></script>
	<script src="/js/wow.min.js"></script>
	<script src="/js/fancybox.js"></script>
	<script src="/js/main.js"></script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Philosopher:400,400i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Prata" rel="stylesheet">

</head>
<body>
<div class="wrapper">
	<div class="b-menu @yield('b_menu_class')">
		<ul class="list-header">
			<li class="list-header__item list-header__item_1">
				<ul class="list-social">
					<li class="list-social__item">
						<a href="#" class="list-social__link list-social__link_fb"></a>
					</li>
					<li class="list-social__item">
						<a href="#" class="list-social__link list-social__link_vk"></a>
					</li>
					<li class="list-social__item">
						<a href="#" class="list-social__link list-social__link_in"></a>
					</li>
				</ul>
			</li>
			<li class="list-header__item list-header__item_2">
				<a href="/" class="logo">
					<img src="/img/logo.png" alt="..." />
				</a>
			</li>
			<li class="list-header__item list-header__item_3 right">
				<span class="header-address">
					г. Иркутск ул.Горького, 36В
				</span>
				<a href="tel:839520000" class="header-tel">
					8 3952 0000
				</a>
			</li>
		</ul>
	</div>
		<div class="b-page">
		    @yield('inner-menu','')
		    @yield('content')
			<section class="section section_footer">
				<div class="b-inner b-inner_footer">
					<div class="footer">
						<h3 class="footer__title wow fadeInUp">
							<span>Отправьте запрос сейчас - получите подарок!</span>
						</h3>
						<div class="feedback wow fadeIn" data-wow-delay=".4s">
							<form class="form">
								<input type="hidden" name="webform_id" value="8">
								<div class="stretch">
									<div class="stretch__item stretch__item_21">
										<input type="text" name="name" class="form__input form__input_mb" placeholder="Ваше имя" />
										<input type="tel" name="phone" class="form__input" placeholder="Ваш телефон" />
									</div>
									<div class="stretch__item stretch__item_53">
										<textarea name="comment" class="form__textarea" placeholder="Ваш комментарий"></textarea>
									</div>
									<div class="stretch__item stretch__item_21">
										<input type="submit" class="form__submit" value="Отправить" />
									</div>
								</div>
							</form>
							<div class="feedback__desc">
								Наш специалист свяжется с Вами в ближайшее время
							</div>
							<div class="contacts">
								<div class="stretch">
									<div class="stretch__item middle">
										<ul class="list-contacts">
											<li class="list-contacts__item">
												Салон ателье HOUSE TWINS
											</li>
											<li class="list-contacts__item">
												<a href="tel:839520000" class="list-contacts__link">
													8 3952 0000
												</a>
											</li>
											<li class="list-contacts__item">
												<a href="mailto:house-twins@mail.ru" class="list-contacts__link">
													house-twins@mail.ru
												</a>
											</li>
										</ul>
									</div>
									<div class="stretch__item middle">
										<div class="contacts__graphic">
											Вт -Вс - с 10:00 до 19:00 ч. без обеда.<br />
											Пн - выходной но Вы можете оставить заявку для обратного звонка
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="footer__copyright">
							<a href="http://webmaster38.ru">
								<img src="img/copyright.png" alt="..." />
							</a>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</body>
</html>