@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Редактирование поля веб-формы {{$webform['name']}}</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-section-prop-form" role="form" method="POST" data-action="WebForms/edit_field">
                <input type="hidden" name="field_id" value="{{$field['id']}}"/>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название поля</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{$field['name']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" readonly value="{{$field['alias']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-md-4 control-label">Тип свойства</label>
                    <div id="type" class="col-md-6">
                        <select class="form-control" readonly="" name="type" value="">
                            <option value="">Выберите тип</option>
                            @foreach($props as $prop)
                                <option value="{{$prop->getAlias()}}"{{$prop->getAlias()==$field['type']?' selected':''}}>{{$prop->getName()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Параметры</label>
                    <div class="checkbox col-md-4">
                        <label for="require" class="control-label">Обязательно для заполнения
                            <input type="hidden" name="required" value="0">
                            <input type="checkbox" name="required"{{$field['required']?' checked':''}} value="1">
                        </label>
                    </div>
                </div>
                @foreach($props as $prop)
                    <div class="iblock_prop_config"{!! $field['type']==$prop->getAlias()?' style="display:block;"':'' !!} id="iblock_prop_config_{{$prop->getAlias()}}">
                        @include($prop->getFormCreateView(),['active'=>false,'values'=>unserialize($field['config'])])
                    </div>
                @endforeach
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Создать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            $('select[name=type]').change(function(){
                $('.iblock_prop_config').hide();
                $('#iblock_prop_config_' + $(this).val()).show();
            });
            admin.registerFormAjax($('#create-section-prop-form'),function(res){
                location.href = "/admin/webforms/{{$webform['id']}}/fields";
            },function(){
            });
        });
    </script>
@endsection
