@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Результат #{{$result['id']}}</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" role="form" method="POST" data-action="WebForms/edit_webform">
                <div class="form-group">
                    <label for="id" class="col-md-4 control-label">ID</label>
                    <div id="id" class="col-md-6">
                        <input type="text" readonly class="form-control" name="id" value="{{$result['id']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Пользователь</label>
                    <div id="name" class="col-md-6">
                        <input type="text" readonly class="form-control" name="name" value="@if($result['user']){{$result['user']['name']}} ({{$result['user']['id']}})@elseАнонимный пользователь@endif">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">IP адрес</label>
                    <div id="name" class="col-md-6">
                        <input type="text" readonly class="form-control" name="name" value="{{$result['ip']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Дата и время</label>
                    <div id="name" class="col-md-6">
                        <input type="text" readonly class="form-control" name="name" value="{{$result['created_at']}}">
                    </div>
                </div>
                @foreach($result['answers'] as $answer)
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">{{$answer['field']['name']}}</label>
                        <div class="col-md-6">
                            {{$answer['value']}}
                        </div>
                    </div>
                @endforeach
            </form>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerFormAjax($('.admin-form-ajax'),function(res){
                location.href = "/admin/webforms/list";
            },function(){

            });
        });
    </script>
@endsection