@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список полей веб-формы {{$webform['name']}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Название</th>
                    <th style="width: 10%;">Псевдоним</th>
                    <th style="width: 15%">Тип поля</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @foreach($fields as $field)
                    <tr>
                        <td>{{$field['id']}}</td>
                        <td><a href="/admin/webforms/{{$webform['id']}}/fields/{{$field['id']}}/edit">{{$field['name']}}</a></td>
                        <td>{{$field['alias']}}</td>
                        <td>{{$field['type']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/webforms/{{$webform['id']}}/fields/{{$field['id']}}/edit">Редактировать</a></li>
                                    <li><a class="admin-btn-ajax" data-method="WebForms/remove_field" data-id="{{$field['id']}}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/webforms/{{$webform['id']}}/fields/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection