@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание инфоблока</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" role="form" method="POST" data-action="WebForms/edit_webform">
                <div class="form-group">
                    <label for="id" class="col-md-4 control-label">ID</label>
                    <div id="id" class="col-md-6">
                        <input type="text" readonly class="form-control" name="id" value="{{$webform['id']}}">
                    </div>
                </div><div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название веб-формы</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{$webform['name']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="{{$webform['alias']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-mail для оповещений</label>
                    <div id="email" class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{$webform['email']}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerFormAjax($('.admin-form-ajax'),function(res){
                location.href = "/admin/webforms/list";
            },function(){

            });
        });
    </script>
@endsection