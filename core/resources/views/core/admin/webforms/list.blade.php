@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список вебформ</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Название</th>
                    <th style="width: 10%;">Псевдоним</th>
                    <th style="width: 15%;">Количество полей</th>
                    <th style="width: 15%">Количество результатов</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @foreach($webforms as $webform)
                    <tr>
                        <td>{{$webform['id']}}</td>
                        <td><a href="/admin/webforms/{{$webform['id']}}/edit">{{$webform['name']}}</a></td>
                        <td>{{$webform['alias']}}</td>
                        <td>{{$webform['fields_count']}}</td>
                        <td>{{$webform['results_count']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/webforms/{{$webform['id']}}/fields/create">Создать поле</a></li>
                                    <li><a href="/admin/webforms/{{$webform['id']}}/fields/">Список полей</a></li>
                                    <li><a href="/admin/webforms/{{$webform['id']}}/edit">Редактировать</a></li>
                                    <li><a class="admin-btn-ajax" data-method="WebForms/remove_webform" data-id="{{$webform['id']}}">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/webforms/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection