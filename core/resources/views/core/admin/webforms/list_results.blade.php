@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список результатов веб-формы {{$webform['name']}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Пользователь</th>
                    <th style="width: 27%;">IP адрес</th>
                    <th style="width: 27%;">Дата создания</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @foreach($results as $result)
                    <tr>
                        <td>{{$result['id']}}</td>
                        <td>@if($result['user']){{$result['user']['name']}} ({{$result['user']['id']}})@elseАнонимный пользователь@endif</td>
                        <td>{{$result['ip']}}</td>
                        <td>{{$result['created_at']}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/webforms/{{$webform['id']}}/results/{{$result['id']}}">Просмотр</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection