<div class="form-group">
    <label for="number_min" class="col-md-4 control-label">Минимальное значение</label>
    <div id="number_min" class="col-md-6">
        <input type="number" class="form-control" name="number_min" value="{{isset($values['number_min'])?$values['number_min']:''}}"/>
    </div>
</div>
<div class="form-group">
    <label for="number_max" class="col-md-4 control-label">Максимальное значение</label>
    <div id="number_max" class="col-md-6">
        <input type="number" class="form-control" name="number_max" value="{{isset($values['number_max'])?$values['number_max']:''}}"/>
    </div>
</div>