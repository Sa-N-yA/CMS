@foreach($pages as $page)
    @if(isset($selected))
        <option value="{{$page->id}}"{{$page->id==$selected?' selected':''}}>@for($i = 0; $i<=$depth; $i++)..@endfor{{$page->name}}</option>
        @include('core.admin.site.option_tree_pages',['pages'=>$page->children,'depth'=>$depth+1,'selected'=>$selected])
    @else
        <option value="{{$page->id}}">@for($i = 0; $i<=$depth; $i++)..@endfor{{$page->name}}</option>
        @include('core.admin.site.option_tree_pages',['pages'=>$page->children,'depth'=>$depth+1])
    @endif
@endforeach