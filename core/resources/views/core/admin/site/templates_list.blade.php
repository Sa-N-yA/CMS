@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список инфоблоков</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Название</th>
                    <th style="width: 10%;">Разработчик</th>
                    <th style="width: 10%;">Псевдоним</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @foreach($templates as $template)
                    <tr>
                        <td>{{$template->id}}</td>
                        <td><a href="/admin/site/templates/{{$template->id}}/edit">{{$template->name}}</a></td>
                        <td>{{$template->developer}}</td>
                        <td>{{$template->alias}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/iblock/{{$template->id}}/create_section">Создать шаблон компонента</a></li>
                                    <li><a href="/admin/iblock/{{$template->id}}/create_prop_section">Список шаблонов компонентов</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/admin/site/templates/{{$template->id}}/edit">Редактировать</a></li>
                                    <li><a data-method="Site/template_remove" data-alias="{{$template['alias']}}" class="admin-btn-ajax" href="#">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/site/templates/create" class="btn btn-default">Создать</a>
            <a href="/admin/site/templates/refresh" class="btn btn-default">Обновить</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection