@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание нового шаблона</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-user-form" role="form" method="POST" data-action="Site/template_create">
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Описание</label>
                    <div id="description" class="col-md-6">
                        <textarea name="description" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Разработчик</label>
                    <div id="developer" class="col-md-6">
                        <input type="text" class="form-control" name="developer" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="source" class="col-md-4 control-label">Исходный код</label>
                    <div id="source" class="col-md-6">
                        <a href="#" class="btn btn-default modal-template-source-open">Открыть в редакторе</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary btn-submit">
                            Создать
                        </button>
                    </div>
                </div>
                <input type="hidden" name="source" value=""/>
            </form>
        </div>
    </div>
    <div class="modal fade template-src-modal template-source-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Исходный код шаблона</h4>
                </div>
                <div class="modal-body">
                    <div id="editor" style="height: 100%;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('.modal-template-source-open').click(function(){
                $('.template-src-modal').modal('show');
                return false;
            });
            var editor = ace.edit("editor");
            editor.getSession().setMode("ace/mode/html");
            $('.template-src-modal').on('hidden.bs.modal', function () {
                $('input[name=source]').val(editor.getSession().getValue());
            });
            admin.registerFormAjax($('.admin-form-ajax'),function(){
                location.href = "/admin/site/templates";
            },function(){
            });
        });
    </script>
@endsection
