@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание нового шаблона</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-user-form" role="form" method="POST" data-action="Site/page_create">
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" class="form-control" name="alias" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Подключаемый вид</label>
                    <div id="view" class="col-md-6">
                        <input type="text" class="form-control" name="view" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-md-4 control-label">Title</label>
                    <div id="title" class="col-md-6">
                        <input type="text" class="form-control" name="title" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="meta_description" class="col-md-4 control-label">Meta description</label>
                    <div id="meta_description" class="col-md-6">
                        <input type="text" class="form-control" name="meta_description" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="meta_keywords" class="col-md-4 control-label">Meta keywords</label>
                    <div id="meta_keywords" class="col-md-6">
                        <input type="text" class="form-control" name="meta_keywords" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="sort" class="col-md-4 control-label">Сортировка</label>
                    <div id="sort" class="col-md-6">
                        <input type="number" class="form-control" name="sort" value="1000">
                    </div>
                </div>
                <div class="form-group">
                    <label for="sort" class="col-md-4 control-label">Шаблон</label>
                    <div id="sort" class="col-md-6">
                        <select name="template" class="form-control">
                            @foreach($templates as $template)
                                <option value="{{$template->id}}">{{$template->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="parent" class="col-md-4 control-label">Родительская страница</label>
                    <div id="parent" class="col-md-6">
                        <select name="parent" class="form-control">
                            <option value="0">Корневая страница</option>
                            @include('core.admin.site.option_tree_pages',['pages'=>$parent_pages,'depth'=>0])
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="complex" class="col-md-4 control-label"></label>
                    <div id="complex" class="col-md-6">
                        <input type="hidden" name="complex" value="N">
                        <input type="checkbox"{{$page['complex']=='Y'?' checked':''}} value="Y" name="complex">Комплексная страница
                    </div>
                </div>
                <div class="form-group">
                    <label for="source" class="col-md-4 control-label">Исходный код</label>
                    <div id="source" class="col-md-6">
                        <a href="#" class="btn btn-default modal-template-source-open">Открыть в редакторе</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary btn-submit">
                            Создать
                        </button>
                    </div>
                </div>
                <input type="hidden" name="source" value=""/>
            </form>
        </div>
    </div>
    <div class="modal fade template-src-modal template-source-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Исходный код страницы</h4>
                </div>
                <div class="modal-body">
                    <div id="editor" style="height: 100%;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('.modal-template-source-open').click(function(){
                $('.template-src-modal').modal('show');
                return false;
            });
            var editor = ace.edit("editor");
            editor.getSession().setMode("ace/mode/html");
            $('.template-src-modal').on('hidden.bs.modal', function () {
                $('input[name=source]').val(editor.getSession().getValue());
            });
            admin.registerFormAjax($('.admin-form-ajax'),function(){
                location.href = "/admin/site/pages";
            },function(){
            });
            var source = "{!! str_replace("\r\n",'\n',str_replace('/','\/',addslashes($template['source']))) !!}";
            editor.getSession().setValue(source);
            $('input[name=source]').val(source);
        });
    </script>
@endsection
