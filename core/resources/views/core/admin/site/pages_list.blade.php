@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Список страниц</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;"></th>
                    <th style="width: 5%;">#</th>
                    <th style="width: 27%;">Название</th>
                    <th style="width: 10%;">Псевдоним</th>
                    <th style="width: 10%;">Подкл. вид</th>
                    <th style="width: 10%;">Сорт.</th>
                    <th style="width: 13%"></th>
                </tr>
                </thead>
                @if($cur_id!==false)
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a href="/admin/site/pages/{{$cur_id}}/list"><b>...</b></a></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
                @foreach($pages as $page)
                    <tr>
                        @if($page->count==0)
                            <td class="pages-list-one"></td>
                        @else
                            <td class="pages-list-many"></td>
                        @endif
                        <td>{{$page->id}}</td>
                        @if($page->count==0)
                            <td><a href="/admin/site/pages/{{$page->id}}/edit">{{$page->name}}</a></td>
                        @else
                            <td><a href="/admin/site/pages/{{$page->id}}/list">{{$page->name}}</a></td>
                        @endif
                        <td>{{$page->alias}}</td>
                        <td>{{$page->view}}</td>
                        <td>{{$page->sort}}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Действия <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/site/pages/{{$page->id}}/edit">Редактировать</a></li>
                                    <li><a data-method="Site/page_remove" data-id="{{$page['id']}}" class="admin-btn-ajax" href="#">Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <a href="/admin/site/pages/create" class="btn btn-default">Создать</a>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerButtonAjax($('.admin-btn-ajax'),function(){
                $(this).parents('tr').remove();
            });
        });
    </script>
@endsection