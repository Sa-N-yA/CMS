@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Изменение группы</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-user-form" role="form" method="POST" data-action="User/update_group">
                <div class="form-group">
                    <label for="id" class="col-md-4 control-label">ID</label>
                    <div id="id" class="col-md-6">
                        <input type="text" class="form-control" readonly name="id" value="{{$group->id}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{$group->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirm_password" class="col-md-4 control-label">Гейты группы</label>
                    <div class="col-md-6">
                        <table class="table table-stripped">
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 40%;">Название</th>
                                <th style="width: 40%;">Дата создания</th>
                                <th style="width: 15%;"></th>
                            </tr>
                            @foreach($gates as $gate)
                                <tr>
                                    <td>{{$gate->id}}</td>
                                    <td>{{$gate->name}}</td>
                                    <td>{{$gate->created_at}}</td>
                                    <td>
                                        <input type="hidden" name="gate_{{$gate->id}}" value="{{$gate->active?'1':'0'}}">
                                        <button class="btn btn-default btn-xs group-select{{$gate->active?' active':''}}"></button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-primary btn-submit">
                            Создать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerFormAjax($('#create-user-form'),function(res){
                location.href = "/admin/users/groups/list";
            },function(){
            });
            $('.group-select').click(function(e){
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('active')){
                    $(this).prev().val('0');
                    $(this).removeClass('active');
                }   else    {
                    $(this).prev().val('1');
                    $(this).addClass('active');
                }
            });
            $('.btn-submit').click(function(){
                $('#create-user-form').submit();
            });
        });
    </script>
@endsection
