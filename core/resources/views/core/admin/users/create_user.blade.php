@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Создание нового пользователя</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-user-form" role="form" method="POST" data-action="User/create_user">
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">ФИО</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-mail</label>
                    <div id="email" class="col-md-6">
                        <input type="email" class="form-control" name="email" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Пароль</label>
                    <div id="password" class="col-md-6">
                        <input type="password" class="form-control" name="password" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation" class="col-md-4 control-label">Подтвердите пароль</label>
                    <div id="password_confirmation" class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm_password" class="col-md-4 control-label">Группы пользователя</label>
                    <div class="col-md-6">
                        <table class="table table-stripped">
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 40%;">Название</th>
                                <th style="width: 40%;">Дата создания</th>
                                <th style="width: 15%;"></th>
                            </tr>
                            @foreach($groups as $group)
                                <tr>
                                    <td>{{$group->id}}</td>
                                    <td>{{$group->name}}</td>
                                    <td>{{$group->created_at}}</td>
                                    <td>
                                        <input type="hidden" name="group_{{$group->id}}" value="0">
                                        <button class="btn btn-default btn-xs group-select"></button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-primary btn-submit">
                            Создать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            admin.registerFormAjax($('#create-user-form'),function(res){
                location.href = "/admin/users/list";
            },function(){
            });
            $('.group-select').click(function(e){
                e.stopPropagation();
                e.preventDefault();
                if ($(this).hasClass('active')){
                    $(this).prev().val('0');
                    $(this).removeClass('active');
                }   else    {
                    $(this).prev().val('1');
                    $(this).addClass('active');
                }
            });
            $('.btn-submit').click(function(){
                $('#create-user-form').submit();
            });
        });
    </script>
@endsection
