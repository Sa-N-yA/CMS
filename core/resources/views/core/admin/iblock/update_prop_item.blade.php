@extends('core.admin.layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Изменение свойства элемента для инфоблока {{$iblock->name}}</div>
        <div class="panel-body">
            <form class="form form-horizontal admin-form-ajax" id="create-section-prop-form" role="form" method="POST" data-action="IBlock/update_item_prop">
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">ID:</label>
                    <div id="id" class="col-md-6">
                        <input type="text" readonly class="form-control" name="id" value="<?=$prop->id?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Название свойства</label>
                    <div id="name" class="col-md-6">
                        <input type="text" class="form-control" name="name" value="<?=$prop->name?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-md-4 control-label">Псевдоним</label>
                    <div id="alias" class="col-md-6">
                        <input type="text" readonly class="form-control" name="alias" value="<?=$prop->alias?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="col-md-4 control-label">Тип свойства</label>
                    <div id="type" class="col-md-6">
                        <select class="form-control" readonly name="type" value="">
                            <option value="">Выберите тип</option>
                            @foreach($props as $_prop)
                                <option value="{{$_prop->getAlias()}}"
                                @if($prop->type==$_prop->getAlias())
                                selected
                                @endif>{{$_prop->getName()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Параметры</label>
                    <div class="checkbox col-md-4">
                        <label for="require" class="control-label">Обязательно для заполнения
                            <input type="hidden" name="required" value="0">
                            <input type="checkbox" name="required" value="1">
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="many" class="control-label">Множественное
                            <input type="hidden" name="many" value="0">
                            <input type="checkbox" name="many" value="1">
                        </label>
                    </div>
                </div>
                @foreach($props as $_prop)
                    <div {!!$prop->type==$_prop->getAlias()?'style="display:block"':''!!} class="iblock_prop_config" id="iblock_prop_config_{{$_prop->getAlias()}}">
                        @include($_prop->getFormCreateView(),['values'=>$prop->config])
                    </div>
                @endforeach
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function(){
            $('select[name=type]').change(function(){
                $('.iblock_prop_config').hide();
                $('#iblock_prop_config_' + $(this).val()).show();
            });
            admin.registerFormAjax($('#create-section-prop-form'),function(res){
                location.href = "/admin/iblock/{{$iblock->id}}/list_props_item";
            },function(){
            });
        });
    </script>
@endsection
