<select class="form-control" name="prop_{{$prop->alias}}{{$many}}">
    <option></option>
    @foreach(explode(',',$prop->config['select_values']) as $c)
        <option value="{{$c}}"{{$value==$c?' selected':''}}>{{$c}}</option>
    @endforeach
</select>
