<div class="form-group">
    <label for="file_type" class="col-md-4 control-label">Тип файла</label>
    <div id="file_type" class="col-md-6">
        <select class="form-control" name="file_type">
            @foreach(App\Http\Controllers\Core\Modules\IBlockFields\FileFieldController::$types as $key=>$value)
                <option value="{{$key}}" {{isset($values['file_type']) && $values['file_type'] == $key?'selected':''}}>{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>