<div class="modal fade" id="modal-items" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close modal-close" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор элемента инфоблока</h4>
            </div>
            <div class="modal-body">
                <table class="table table-collsapsed">
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-7">Название</th>
                        <th class="col-md-2">Псевдоним</th>
                        <th class="col-md-2"></th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default modal-close">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var onRegisterModalItems = function () {
        this.sections = {};
        this.loadSections = function (section_id) {
            var self = this;
            self.sections[0] = "Корневой раздел";
            admin.ajax('IBlock/get_items', {
                iblock_id: iblock_id,
                filter: {sections_filter: [['parent_id', section_id]], items_filter: [['section_id', section_id]]},
                fields: ['name', 'alias']
            }, function (res) {
                $('.modal-items-select').attr('data-id', section_id);
                var table = self.find('table');
                table.find('tr.modal-items').remove();
                if (section_id!=0) {
                    table.append('<tr class="modal-items">' +
                        '<td></td>' +
                        '<td><a data-id="' + self.currentSection + '" href="#"><strong>...</strong></a></td>' +
                        '<td></td>' +
                        '<td></td>' +
                        '</tr>');
                }
                self.currentSection = section_id;
                for (var r in res['DATA']['sections']) {
                    var r = res['DATA']['sections'][r];
                    self.sections[r.id] = r.name;
                    table.append('<tr class="modal-items">' +
                            '<td>' + r.id + '</td>' +
                            '<td><a data-id="' + r.id + '" href="#">' + r.name + '</a></td>' +
                            '<td>' + r.alias + '</td>' +
                            '<td></td>' +
                            '</tr>');
                }
                for (var r in res['DATA']['items']) {
                    var r = res['DATA']['items'][r];
                    self.sections[r.id] = r.name;
                    table.append('<tr class="modal-items">' +
                            '<td>' + r.id + '</td>' +
                            '<td>' + r.name + '</td>' +
                            '<td>' + r.alias + '</td>' +
                            '<td><button data-id="' + r.id + '" class="btn btn-default modal-success btn-xs">Выбрать</button></td>' +
                            '</tr>');
                }
                table.find('a').click(function () {
                    self.loadSections($(this).attr('data-id'));
                    return false;
                });
            });
        };
    }
</script>