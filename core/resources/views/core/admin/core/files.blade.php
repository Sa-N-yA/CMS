@extends('core.admin.layout')

@section('content')
    <div class="files-inner">
        <div class="files-list" style="width: 20%;">
            <div class="files-list-inner">
                <div class="panel panel-default" data-path="/core/resources/views">
                    <div class="panel-heading">Виды <div class="caret"></div></div>
                    <div class="panel-body" style="display: none;">
                    </div>
                </div>
                <div class="panel panel-default" data-path="/core/app/Site">
                    <div class="panel-heading">Сайт <div class="caret"></div></div>
                    <div class="panel-body" style="display: none;">
                    </div>
                </div>
                <div class="panel panel-default" data-path="/core/config">
                    <div class="panel-heading">Настройки <div class="caret"></div></div>
                    <div class="panel-body" style="display: none;">
                    </div>
                </div>
                <div class="panel panel-default" data-path="">
                    <div class="panel-heading">Система <div class="caret"></div></div>
                    <div class="panel-body" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <script src="/admin/common/js/ace.js" type="text/javascript" charset="utf-8"></script>

        <div class="file-editor-panel" style="position: absolute;width: 80%;height: 100%;right: 0;">
            <ul class="files-editor-open nav nav-tabs">
                {{--<li class="active"><a href="#file1" data-toggle="tab" title="/core/public/index.php">index.php</a></li>
                <li><a href="#file2" data-toggle="tab">AjaxModificatorController.php</a></li>
                <li><a href="#file3" data-toggle="tab">Сообщения</a></li>
                <li><a href="#file4" data-toggle="tab">Настройки</a></li>--}}
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                {{--<div class="tab-pane active" id="file1">
                    123456789

                </div>
                <div class="tab-pane" id="file2">
                    <div class="editor" id="editor2">
                        <html>123</html>
                    </div>
                </div>
                <div class="tab-pane" id="file3">
                    <div class="editor" id="editor3">
                        <html>12345</html>
                    </div>
                </div>
                <div class="tab-pane" id="file4">
                    <div class="editor" id="editor4">
                        <html>1234567</html>
                    </div>
                </div>--}}
            </div>




            <script>
                /*var editor1 = ace.edit("editor1"); // теперь обращаться к редактору будем через editor
                var editor2 = ace.edit("editor2"); // теперь обращаться к редактору будем через editor
                var editor3 = ace.edit("editor3"); // теперь обращаться к редактору будем через editor
                var editor4 = ace.edit("editor4"); // теперь обращаться к редактору будем через editor
                editor1.getSession().setMode("ace/mode/javascript");
                editor2.getSession().setMode("ace/mode/javascript");
                editor3.getSession().setMode("ace/mode/javascript");
                editor4.getSession().setMode("ace/mode/javascript");*/
                // Далее весь экшон будет проходить тут!
            </script>
        </div>
    </div>
@endsection