@section('content')
<section class="section section_contacts border_top">
        <div class="contact">
            <div class="contact__bg"></div>
            
	        <div class="contact__info-wrapper wow fadeIn" data-wow-duration="2s">
                <div class="contact__info-wrapper-2">
                    <div class="contact__info-content">
                        <h1 class="contact__title wow fadeInUp" data-wow-delay=".4s">
                            Контакты
                        </h1>
                        <!--<div class="contact__desc wow fadeInUp" data-wow-delay=".7s">
                            Родоначальницами бренда <b>«HOUSE TWINS»</b> являются портные близнецы Анна и Евгения.
                            Этим ремеслом они занимаются уже более пятнадцати лет. За это время они наработали
                            колоссальный опыт и зарекомендовали себя как профессионалы своего дела что
                            подтверждает исключительно высокое качество изготавливаемых предметов гардероба.
                        </div>-->
                        <div class="contact__address">
									<span class="contact__text">
										<div class="wow fadeIn" data-wow-delay="1s">
                                            г.Иркутск, ул. Карла Маркса, 21в
                                        </div>
										<a href="tel:83952721144" class="contact__link contact__link_tel wow fadeInLeft" data-wow-delay="1.3s">
                                            тел: 8 (3952) 72-11-44
                                        </a>
										<a href="mailto:info@house-twins.ru" class="contact__link contact__link_email wow fadeInRight" data-wow-delay="1.3s">
                                            email: info@house-twins.ru
                                        </a>
									</span>
                        </div>
                        <div class="contact__social wow fadeInUp" data-wow-delay="1.6s">
                            <div class="contact__social-title">
                                Социальные сети
                            </div>
                            <ul class="list-social">
                                <li class="list-social__item">
                                    <a href="//www.facebook.com/Салон-ателье-HOUSE-TWINS-1396851713691753" target="_blank" class="list-social__link list-social__link_fb"></a>
                                </li>
                                <li class="list-social__item">
                                    <a href="//vk.com/house_twins" target="_blank" class="list-social__link list-social__link_vk"></a>
                                </li>
                                <li class="list-social__item">
                                    <a href="//www.instagram.com/house.twins/" target="_blank" class="list-social__link list-social__link_in"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contact__map" id="js-map">
                <a class="dg-widget-link"
                   href="http://2gis.ru/irkutsk/firm/1548640653208202/center/104.28675413131715,52.28651124285217/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
                    на карте Иркутска</a>
                <div class="dg-widget-link"><a
                        href="http://2gis.ru/irkutsk/firm/1548640653208202/photos/1548640653208202/center/104.28675413131715,52.28651124285217/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии
                        компании</a></div>
                <div class="dg-widget-link"><a
                        href="http://2gis.ru/irkutsk/center/104.286732,52.284352/zoom/16/routeTab/rsType/bus/to/104.286732,52.284352╎HOUSE TWINS, ателье?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти
                        проезд до HOUSE TWINS, ателье</a></div>
                <script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script>
                <script charset="utf-8">new DGWidgetLoader({
                        "width": '100%',
                        "height": 740,
                        "borderColor": "#a3a3a3",
                        dragging : false,
                        touchZoom: false,
                        scrollWheelZoom: false,
                        doubleClickZoom: false,
                        boxZoom: false,
                        geoclicker: false,
                        zoomControl: false,
                        fullscreenControl: false,
                        "pos": {"lat": 52.28651124285217, "lon": 104.28675413131715, "zoom": 16},
                        "opt": {"city": "irkutsk"},
                        "org": [{"id": "1548640653208202"}]
                    });</script>

            </div>

        </div>
    </section>
    @endsection