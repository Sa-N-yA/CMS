@section('content')
<section class="section section_nopb section_gray">
    <div class="b-inner">
        <h1 class="section__title section__title_noline wow fadeInUp">
            Реставрация одежды в салоне ателье HOUSE TWINS
        </h1>
        <div class="content content_1000 wow fadeInUp" data-wow-delay=".3s">
            <p>
                Родоначальницами бренда <b>«HOUSE TWINS»</b> являются портные близнецы Анна и Евгения.
                Этим ремеслом они занимаются уже более пятнадцати лет. За это время они наработали
                колоссальный опыт и зарекомендовали себя как профессионалы своего дела что подтверждает
                исключительно высокое качество изготавливаемых предметов гардероба.
            </p>
            <h3 class="content__title">
                Как мы реставрируем ваши вещи
            </h3>
            <div class="video video_mb js-video wow fadeInUp" data-wow-delay=".6s">
                <iframe src="https://www.youtube.com/embed/C8NAYW-Z54o?rel=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="section section_2 section_bf">
    <div class="b-inner">
        <h3 class="section__title section__title_noline wow fadeInUp" data-wow-delay=".8s">
            Фото результатов
        </h3>
        
<div class="slider-before-wrapper">
    <div class="stretch">
        <div class="stretch__item stretch__item_bf-left stretch__item_26">
            <div class="slider-pag" id="js-slider-pagination">
                                <div class="slider-pag__item js-gal active wow fadeInLeft" data-wow-delay="0.2s" data-slide="0">
                    <div class="slider-pag__title">
                        Ремонт изделий из трикотажа                    </div>
                    <div class="slider-pag__desc">
                        платья, свитера, жилеты, брюки спортивные, майки, футболки                    </div>
                </div>
                                <div class="slider-pag__item js-gal active wow fadeInLeft" data-wow-delay="0.3s" data-slide="1">
                    <div class="slider-pag__title">
                        Ремонт изделий из трикотажа                    </div>
                    <div class="slider-pag__desc">
                        платья, свитера, жилеты, брюки спортивные, майки, футболки                    </div>
                </div>
                                <div class="slider-pag__item js-gal active wow fadeInLeft" data-wow-delay="0.4s" data-slide="2">
                    <div class="slider-pag__title">
                        Ремонт изделий из трикотажа                    </div>
                    <div class="slider-pag__desc">
                        платья, свитера, жилеты, брюки спортивные, майки, футболки                    </div>
                </div>
                            </div>
        </div>
        <div class="stretch__item stretch__item_bf-right stretch__item_68">
            <div class="fabric-text">
                <div class="fabric-text__item">
                    До
                </div>
                <div class="fabric-text__item">
                    После
                </div>
            </div>
            <div class="slider-fabric wow fadeInRight" data-wow-delay=".2s" id="js-slider-fabric">
                                    <div class="slider-fabric__item">
                        <div class="before-after js-bf" data-bf-slide="0">
                            <img src="/upload/iblock/db8/a1.jpg" border="0" alt="" width="802" height="503" />                            <img src="/upload/iblock/bec/b1.jpg" border="0" alt="" width="802" height="503" />                        </div>
                    </div>
                                    <div class="slider-fabric__item">
                        <div class="before-after js-bf" data-bf-slide="1">
                            <img src="/upload/iblock/8a5/a1.jpg" border="0" alt="" width="802" height="503" />                            <img src="/upload/iblock/845/b1.jpg" border="0" alt="" width="802" height="503" />                        </div>
                    </div>
                                    <div class="slider-fabric__item">
                        <div class="before-after js-bf" data-bf-slide="2">
                            <img src="/upload/iblock/625/a1.jpg" border="0" alt="" width="802" height="503" />                            <img src="/upload/iblock/d80/b1.jpg" border="0" alt="" width="802" height="503" />                        </div>
                    </div>
                            </div>
        </div>
    </div>
</div>    </div>
</section>

<section class="section section_nopb">
    <div class="b-inner">
        <h3 class="section__title section__title_noline wow fadeInUp">
            Отзывы
        </h3>
        <div class="clients-wrapper wow fadeInUp" data-wow-delay=".2s">
            <div class="clients-text">
                Качество работы модельеров ателье House Twins уже оценили известные люди и компании города Иркутска!
            </div>
            <div class="slider-cloth js-slick-clients">
        <div>
        <div class="clients">
            <div class="stretch">
                <div class="stretch__item">
                                            <div class="clients__item">
                            <div class="clients__image">
                                <img src="/upload/iblock/7db/fon.jpg" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                                                        </div>
                                <h5 class="clients__name">
                                    Екатерина Малехина </h5>
                                <div class="clients__text">
                                    Всей семьей, уже много лет заказываем пошив одежыды в этом ателье. Шьют быстро и очень качественно! По цене выходит дешевле магазинов средней категории. Спасибо вам!                                </div>
                            </div>
                        </div>
                                        <!--
                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                                                                <span class="clients__star"></span>
                                                                    </div>
                                <h5 class="clients__name">
                                     </h5>
                                <div class="clients__text">
                                                                    </div>
                            </div>
                        </div>
                    -->                </div>
                <div class="stretch__item">                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="/upload/iblock/2f9/fon.jpg" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                                                        </div>
                                <h5 class="clients__name">
                                    Анастасия Палилова </h5>
                                <div class="clients__text">
                                    Заказывал в этом ателье костюм Casual и коллекцию рубашек, сшили быстро и качество порадовало. Раньше во многих ателье шил на заказ, но нигде не устраивало, по цене качеству. Теперь шьюсь только в ателье House Twins.                                </div>
                            </div>
                        </div>
                                        <!--
                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                                                                <span class="clients__star"></span>
                                                                    </div>
                                <h5 class="clients__name">
                                     </h5>
                                <div class="clients__text">
                                                                    </div>
                            </div>
                        </div>
                    -->                </div>
            </div>
        </div>
    </div>
        <div>
        <div class="clients">
            <div class="stretch">
                <div class="stretch__item">
                                            <div class="clients__item">
                            <div class="clients__image">
                                <img src="/upload/iblock/593/fon.jpg" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                            <span class="clients__star active"></span>
                                                                                                        </div>
                                <h5 class="clients__name">
                                    Ирина Кольцова </h5>
                                <div class="clients__text">
                                    Работаем с ателье HOUSE TWINS уже ни первый год, заказываем шторы, постельные принадлежности, одежду для персонала, для гостиничного комплекса. Довольны качеством и приемлемой ценой. Всегда рекомендуем всем друзьям и знакомым, от которых слышим только хорогие отзывы.                                </div>
                            </div>
                        </div>
                                        <!--
                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                                                                <span class="clients__star"></span>
                                                                    </div>
                                <h5 class="clients__name">
                                     </h5>
                                <div class="clients__text">
                                                                    </div>
                            </div>
                        </div>
                    -->                </div>
                <div class="stretch__item">                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="/upload/iblock/965/fon.jpg" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                        <span class="clients__star active"></span>
                                                                                                        </div>
                                <h5 class="clients__name">
                                    Нина Афанасьева </h5>
                                <div class="clients__text">
                                    Давно уже с мужем сменили магазины на ателье. Вещи по качеству не уступают дорогим брендам, а каким-то дают фору. У мужа нестандартная фигура и всегда было проблематично подобрать подходящую одежду. Заказываем пошив одежды в ателье HOUSE TWINS уже больше 7 лет, более чем довольны, много друзей шьются в этом же.                                </div>
                            </div>
                        </div>
                                        <!--
                        <div class="clients__item">
                            <div class="clients__image">
                                <img src="" alt="">
                            </div>
                            <div class="clients__desc">
                                <div class="clients__rating">
                                                                            <span class="clients__star active"></span>
                                                                                                                <span class="clients__star"></span>
                                                                    </div>
                                <h5 class="clients__name">
                                     </h5>
                                <div class="clients__text">
                                                                    </div>
                            </div>
                        </div>
                    -->                </div>
            </div>
        </div>
    </div>
    </div>        </div>
    </div>
</section>
@endsection