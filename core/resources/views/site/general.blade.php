@section('content')
    <div class="b-page">
        <div class="jcarousel-wrapper">
            <!-- Carousel -->
            @component([
                'name'=>'IBlockData:items',
                'template'=>'general-banner',
                'modificators'=>['Images'],
                'params'=>[
                    'iblock_id'=>5,
                    'fields'=>['name','prop.image','prop.subtitle'],
                    'ImagesModificator'=>[
                        'items.*.image'=>[
                            [
                                'name'=>'full',
                                'width'=>1920,
                                'height'=>948
                            ]
                        ]
                    ]
                ]
            ])
            <div class="menu-bottom">
                <div class="menu-slide-wrapper js-stick">
                    <div class="menu-slide menu-slide_hide js-menu">
                        Меню
                    </div>
                    @component([
                        'name'=>'Core:menu',
                        'template'=>'banner',
                        'params'=>[
                            'type'=>'Pages',
                            'ignore'=>['/'],
                        ]
                    ])
                </div>
            </div>
        </div>
        <section class="section section_2">
            <div class="b-inner">
                <h3 class="section__title wow fadeInUp">
						<span class="title__yellow">
							«HOUSE TWINS»<br/>
							Реализация твоего стиля - индивидуальный пошив <br/>
							и реставрация одежды
						</span>
                </h3>
                <div class="content content_1000 wow fadeIn" data-wow-delay=".3s">
                    <p>
                        Ни для кого не секрет что одежда и аксессуары являются неотъемлемой
                        частью стиля современного человека, в стиле подчеркивается индивидуальность.
                        Ни для кого не секрет что даже самая качественная одежда, выпущенная известным
                        брендом, не сравнится с одеждой, сшитой на заказ.
                    </p>
                    <p>
                        Потому что только при индивидуальном пошиве можно изготовить одежду,
                        сидящую по фигуре, со всей эргономичностью и функционалом, удовлетворяющими
                        любые требования.
                    </p>
                    <p class="italic">
                        Первое впечатление не произведешь второй раз...
                    </p>
                </div>
            </div>
        </section>
        <section class="section section_3">
            <div class="b-inner">
                <h3 class="section__title section__title_noline wow fadeInUp" data-wow-delay=".3s">
                    Почему Вам стоит выбрать именно «HOUSE TWINS»?
                </h3>
                <div class="content content_1000 wow fadeIn" data-wow-delay=".3s">
                    <p>
                        Родоначальницами бренда <b>«HOUSE TWINS»</b> являются портные близнецы Анна и Евгения.
                        Этим ремеслом они занимаются уже более пятнадцати лет. За это время они
                        наработали колоссальный опыт и зарекомендовали себя как профессионалы
                        своего дела что подтверждает исключительно высокое качество изготавливаемых
                        предметов гардероба.
                    </p>
                    <p>
                        Выбирая <b>«HOUSE TWINS»</b>, Вы выбираете личных модельеров,
                        которые помогут подобрать стиль максимально подчеркивающий
                        Вашу индивидуальность и безупречность вкуса.
                    </p>
                    <p>
                        Кроме того, стоит отметить что индивидуальный пошив это не единственное направление,
                        в котором работает салон ателье <b>«HOUSE TWINS»</b>. Здесь Вы также можете приобрести
                        мужские и женские аксессуары.
                    </p>
                </div>
                <div class="advant">
                    <div class="stretch">
                        <div class="stretch__item stretch__item_20">
                            <div class="advant__item advant__item_i1 wow fadeInUp">
                                15 лет опыта
                            </div>
                        </div>
                        <div class="stretch__item stretch__item_20">
                            <div class="advant__item advant__item_i2 advant__item_min wow fadeInUp"
                                 data-wow-delay=".2s">
                                Более 7000<br/>
                                довольных клиентов
                            </div>
                        </div>
                        <div class="stretch__item stretch__item_20">
                            <div class="advant__item advant__item_i3 wow fadeInUp" data-wow-delay=".4s">
                                Приятные цены
                            </div>
                        </div>
                        <div class="stretch__item stretch__item_20">
                            <div class="advant__item advant__item_i4 advant__item_min wow fadeInUp"
                                 data-wow-delay=".6s">
                                Индивидуальный подход<br/>
                                и безупречное качество
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section_2">
            <div class="b-inner">
                <h3 class="section__title wow fadeInUp">
						<span class="title__yellow">
							Процесс создания
						</span>
                </h3>
                <div class="content content_1000 wow fadeIn" data-wow-delay=".3s">
                    <p>
                        Процесс создания одежды в ателье HOUSE TWINS это увлекательный
                        и интересный процесс, абсолютно не требующий больших временных затрат,
                        а также оставляющий только положительные эмоции от участия в создании вещи совместно
                        с высококвалифицированными модельерами салона.
                    </p>
                    <p>
                        Удобное территориальное расположение, в центре города Иркутска по улице Горького,
                        куда можно беспрепятственно и быстро добраться как на автомобиле, так и на общественном
                        транспорте из любой точки города.
                    </p>
                </div>
                <div class="process">
                    <div class="process__item process__item_1 wow fadeInDown">
                        <div class="process__num">
                            <span>1</span>
                        </div>
                        <div class="process__desc">
                            Снятие ваших мерок
                        </div>
                        <div class="process__line process__line_1 wow fadeInUp" data-wow-delay=".8s"></div>
                    </div>
                    <div class="process__item process__item_2 wow fadeInDown" data-wow-delay="1s">
                        <div class="process__num">
                            <span>2</span>
                        </div>
                        <div class="process__desc">
                            Выбор ткани
                        </div>
                        <div class="process__line process__line_2 wow fadeInDown" data-wow-delay="1.8s"></div>
                    </div>
                    <div class="process__item process__item_3 wow fadeInDown" data-wow-delay="2s">
                        <div class="process__num">
                            <span>3</span>
                        </div>
                        <div class="process__desc">
                            Создание модели<br/>
                            по вашим меркам
                        </div>
                        <div class="process__line process__line_3 wow fadeInUp" data-wow-delay="2.8s"></div>
                    </div>
                    <div class="process__item process__item_4 wow fadeInDown" data-wow-delay="3s">
                        <div class="process__num">
                            <span>4</span>
                        </div>
                        <div class="process__desc">
                            Примерка модели,<br/>
                            подгон по вашей фигуре
                        </div>
                        <div class="process__line process__line_4 wow fadeInDown" data-wow-delay="3.8s"></div>
                    </div>
                    <div class="process__item process__item_5 wow fadeInDown" data-wow-delay="4s">
                        <div class="process__num">
                            <span>5</span>
                        </div>
                        <div class="process__desc">
                            Пошив изделия
                        </div>
                        <div class="process__buttons">
                            <a href="#" class="button button_dark">
                                Контакты
                            </a>
                        </div>
                    </div>
                </div>
                @component([
                    'name'=>'IBlockData:items',
                    'template'=>'special',
                    'modificators'=>['Images'],
                    'params'=>[
                        'iblock_id'=>6,
                        'fields'=>['name','prop.image','prop.old_price','prop.new_price'],
                        'limit'=>3,
                        'ImagesModificator'=>[
                            'items.*.image'=>[
                                [
                                    'name'=>'full',
                                    'width'=>240,
                                    'height'=>432
                                ]
                            ]
                        ]
                    ]
                ])
            </div>
        </section>
        <section class="section section_4">
            <div class="b-inner">
                <h3 class="section__title section__title_noline section__title_white wow fadeInUp">
                    Подарочный сертификат
                </h3>
                <div class="sertificat">
                    <div class="stretch">
                        <div class="stretch__item stretch__item_47">
                            <div class="sertificat__image wow fadeInLeft" data-wow-delay=".2s">
                                <img src="img/sertificat.png" alt="..."/>
                            </div>
                            <div class="sertificat__buttons">
                                <a href="#" class="button button_sertificat wow fadeIn" data-wow-delay=".6s">
                                    Заказать сертификат
                                </a>
                            </div>
                        </div>
                        <div class="stretch__item stretch__item_47">
                            <div class="sertificat__desc wow fadeInRight" data-wow-delay=".2s">
                                <p>
                                    Выбор подарка зачастую заводит человека в тупик, что подарить?
                                    Понравится ли? По поводу или без повода выбирая подарок хочется порадовать человека.
                                </p>
                                <p>
                                    Приобретя сертификат на индивидуальный пошив одежды в салоне <b>«HOUSE TWINS»</b>,
                                    вы не ошибетесь в выборе достойного подарка и будете уверены, что он непременно
                                    понравиться человеку. Воплощение индивидуального стиля, безупречный покрой,
                                    все это подчеркнет статус владельца одежды.
                                </p>
                                <p>
                                    Вы можете приобрести Подарочный сертификат любого наминала в салоне
                                    ателье либо заказать доставку в офис или на дом. Оставьте заявку на
                                    сайте или позвоните по одному из телефонов ателье.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section_5">
            <div class="b-inner">
                <h3 class="section__title section__title_noline wow fadeInUp">
                    7000 видов тканей
                </h3>
                <div class="cloth wow fadeInUp" data-wow-delay=".2s">
                    <div class="slider-cloth js-slick-cloth">
                        <div>
                            <img src="img/cloth1.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth2.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth3.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth4.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth1.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth2.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth3.png" alt="..."/>
                        </div>
                        <div>
                            <img src="img/cloth4.png" alt="..."/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section_2">
            <div class="b-inner">
                <h3 class="section__title wow fadeInUp">
						<span class="title__yellow">
							Клиенты салона ателье «House Twins»
						</span>
                </h3>
                @component([
                    'name'=>'IBlockData:items',
                    'template'=>'clients',
                    'modificators'=>['Images','Normalize'],
                    'params'=>[
                        'iblock_id'=>7,
                        'fields'=>['name','prop.text','prop.photo','prop.rating'],
                        'ImagesModificator'=>[
                            'items.*.photo'=>[
                                [
                                    'name'=>'full',
                                    'width'=>120,
                                    'height'=>120
                                ]
                            ]
                        ],
                        'NormalizeModificator'=>['items']
                    ]
                ])
            </div>
        </section>
@endsection