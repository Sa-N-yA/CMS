@section('content')
    @component([
        'name'=>'IBlockData:main',
        'template'=>'catalog-ravus',
        'params'=>[
            'iblock_id'=>8,
            'base_uri'=>'ravus/man',
            'sections_uri'=>'/',
            'sections_view'=>'ravus.man-sections',
            'items_uri'=>'/#SECTION_ALIAS#/',
            'items_view'=>'ravus.man-section',
            'items_section_fields'=>['name']
        ]
    ])

@endsection