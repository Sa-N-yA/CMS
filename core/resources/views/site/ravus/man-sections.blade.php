@section('content')
    <section class="section section_ravus">
        <div class="b-inner">
            <div class="ravus">
                <div class="ravus__logo wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">
                    <img src="/img/ravus_logo.png" alt="...">
                </div>
                <div class="ravus__border wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                    <div class="ravus__desc">
                        <strong>ЖИЗНЬ В ДВИЖЕНИИ</strong>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section_submenu border_top">
        <div class="b-inner">
            <div class="content content_1000 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                @component([
                    'name'=>'Core:breadcrumbs',
                    'template'=>'catalog',
                    'params'=>[]
                ])
            </div>
        </div>
    </section>
    @component([
        'name'=>'IBlockData:sections',
        'template'=>'catalog-ravus',
        'modificators'=>['Images','SectionData'],
        'params'=>[
            'iblock_id'=>8,
            'filter'=>[['parent_id',1]],
            'fields'=>['name','alias','prop.image'],
            'SectionDataModificator'=>[
                'section_id'=>1,
                'iblock_id'=>8,
                'fields'=>['name','alias']
            ],
            'ImagesModificator'=>[
                'items.*.image'=>[
                    [
                        'name'=>'full',
                        'width'=>204,
                        'height'=>275
                    ]
                ]
            ]
        ]
    ])
@endsection