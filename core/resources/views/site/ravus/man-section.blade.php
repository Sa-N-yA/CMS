@section('content')
        <section class="section section_ravus">
            <div class="b-inner">
                <div class="ravus">
                    <div class="ravus__logo wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">
                        <img src="/img/ravus_logo.png" alt="...">
                    </div>
                    <div class="ravus__border wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                        <div class="ravus__desc">
                            <strong>ЖИЗНЬ В ДВИЖЕНИИ</strong>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section_submenu border_top">
            <div class="b-inner">
                <div class="content content_1000 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    @component([
                        'name'=>'Core:breadcrumbs',
                        'template'=>'catalog',
                        'params'=>[]
                    ])
                </div>
            </div>
        </section>
        <section class="section section_2">
            <div class="b-inner">
                <h3 class="section__title wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                    <span class="title__yellow">Ветровки</span>
                </h3>
                <div class="products">
                    <a href="/catalog/ravus/man/vetrovki/vetrovka-the-north-face/" class="products__item wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <div class="products__image">
                            <img src="/upload/iblock/554/1.jpg" alt="Ветровка The North Face">
                        </div>
                        <div class="products__title">
                            Ветровка The North Face                    </div>
                        <div class="products__price">
                        <span class="products__oldprice">
                            10380 руб
                        </span>
                        <span class="products__newprice">
                            7480 руб
                        </span>
                        </div>
                    </a>
                </div>
            </div>
        </section>
@endsection