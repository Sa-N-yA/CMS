@section('content')
    <section class="section section_ravus">
        <div class="b-inner">
            <div class="ravus">
                <div class="ravus__logo wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">
                    <img src="/img/ravus_logo.png" alt="...">
                </div>
                <div class="ravus__border wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                    <div class="ravus__desc">
                        <strong>ЖИЗНЬ В ДВИЖЕНИИ</strong>
                    </div>
                </div>
            </div>
            <div class="gender">
                <a href="/ravus/man/" class="gender__item gender__item_left wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <div class="gender__overlay"></div>
                    <div class="gender__border">
                        <div class="gender__top"></div>
                        <div class="gender__right"></div>
                        <div class="gender__bottom"></div>
                        <div class="gender__left"></div>
                    </div>
                    <div class="gender__label">
                        Мужская<br>
                        одежда
                    </div>
                    <img src="/img/man.jpg" alt="..." class="gender__img">
                </a>

                <a href="/ravus/woman/" class="gender__item gender__item_right wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                    <div class="gender__overlay"></div>
                    <div class="gender__border">
                        <div class="gender__top"></div>
                        <div class="gender__right"></div>
                        <div class="gender__bottom"></div>
                        <div class="gender__left"></div>
                    </div>
                    <div class="gender__label">
                        Женская<br>
                        одежда
                    </div>
                    <img src="/img/girl.jpg" alt="..." class="gender__img">
                </a>
            </div>
        </div>
    </section>
    <section class="section section_2">
        <div class="b-inner">
            <h3 class="section__title wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
						<span class="title__yellow">
							«Какой бы ни была ваша цель, вы сможете ее достигнуть, если только захотите потрудиться.»
						</span>
            </h3>
            <div class="content content_1000 wow fadeIn" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeIn;">
                <p>Бренд RAVUS берет свои истоки на территории Сибири, в его концепции заложена идиология компании, которая основана на фундаментальных и в тоже время естественных фещях для человека, таких как: преданность семье и родине, любовь к спорту и конечно же стремление к победам.</p>
                <p>Учавствуя в спортивных мероприятиях различного рода, команда RAVUS несет в себе эти ценности что еще больше объединяет компанию.</p>
                <p><strong>«</strong><strong>RAVUS</strong><strong>»</strong> это спортивный бренд мужской и женской одежды и спортивных аксессуаров.&nbsp;</p>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="b-inner">
            <h3 class="section__title wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
						<span class="title__white">
							Аксессуары
						</span>
            </h3>

            <div class="products">
                <a href="/catalog/individual/man/aksessuary/sharf-eleganzza/" class="products__item wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <div class="products__image">
                        <img src="/upload/iblock/505/2.jpg" alt="">
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection