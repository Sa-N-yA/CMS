@section('content')
    @component([
        'name'=>'IBlockData:main',
        'template'=>'catalog-ravus',
        'params'=>[
            'iblock_id'=>8,
            'base_uri'=>'ravus/woman',
            'sections_uri'=>'/',
            'sections_view'=>'ravus.woman-sections',
            'section_uri'=>'/#SECTION_ALIAS#/',
            'section_view'=>'ravus.man-section',
        ]
    ])

@endsection