@section('content')
<section class="section section_2">
        <div class="b-inner">
            <h3 class="section__title wow fadeInUp">
						<span class="title__yellow">
							«HOUSE TWINS»<br />
							Реализация твоего стиля - индивидуальный пошив <br />
							и реставрация одежды
						</span>
            </h3>
            <div class="content content_1000 wow fadeIn" data-wow-delay=".3s">
                <p>Когда встает вопрос об индивидуальном пошиве как альтернативе магазинам, можно выделить несколько аспектов дающих неоспоримые преимущества пошиву на заказ.</p>
<p>Ручной труд высоко ценился во все времена, потому как при ручной работе каждой создаваемой детали отводится особое внимание.</p>
<p>Кропотливый труд, приверженность мастера делу всей жизни, безупречный покрой и идеально сидящая вещь на Вашей фигуре &ndash; в результате появляется произведение искусства, которое не оставит равнодушным даже самого требовательного человека и подпитывает мастера на созидание нового.</p>
<p>В салоне ателье <strong>HOUSE </strong><strong>TWINS</strong> помогут Вам в создании индивидуального предмета гардероба, от выбора ткани до разработки любой сложности модели и воплощении в реальность.</p>
<p>Так же существует возможность пошива за короткий срок от 3 до 10 дней в зависимости сложности вещи.</p>
            </div>
        </div>
    </section>
    <section class="section section_3">
        <div class="b-inner">
            <h3 class="section__title section__title_noline wow fadeInUp" data-wow-delay=".3s">
                Почему Вам стоит выбрать именно «HOUSE TWINS»?
            </h3>
            <div class="content content_1000 wow fadeIn" data-wow-delay=".3s">
                <p>
                    Родоначальницами бренда <b>«HOUSE TWINS»</b> являются портные близнецы Анна и Евгения.
                    Этим ремеслом они занимаются уже более пятнадцати лет. За это время они
                    наработали колоссальный опыт и зарекомендовали себя как профессионалы
                    своего дела что подтверждает исключительно высокое качество изготавливаемых
                    предметов гардероба.
                </p>
                <p>
                    Выбирая <b>«HOUSE TWINS»</b>, Вы выбираете личных модельеров,
                    которые помогут подобрать стиль максимально подчеркивающий
                    Вашу индивидуальность и безупречность вкуса.
                </p>
                <p>
                    Кроме того, стоит отметить что индивидуальный пошив это не единственное направление,
                    в котором работает салон ателье <b>«HOUSE TWINS»</b>. Здесь Вы также можете приобрести
                    мужские и женские аксессуары.
                </p>
            </div>
            <div class="advant">
                <div class="stretch">
                    <div class="stretch__item stretch__item_20">
                        <div class="advant__item advant__item_i1 wow fadeInUp">
                            15 лет опыта
                        </div>
                    </div>
                    <div class="stretch__item stretch__item_20">
                        <div class="advant__item advant__item_i2 advant__item_min wow fadeInUp" data-wow-delay=".2s">
                            Более 7000<br />
                            довольных клиентов
                        </div>
                    </div>
                    <div class="stretch__item stretch__item_20">
                        <div class="advant__item advant__item_i3 wow fadeInUp" data-wow-delay=".4s">
                            Приятные цены
                        </div>
                    </div>
                    <div class="stretch__item stretch__item_20">
                        <div class="advant__item advant__item_i4 advant__item_min wow fadeInUp" data-wow-delay=".6s">
                            Индивидуальный подход<br />
                            и безупречное качество
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection