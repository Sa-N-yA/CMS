<?php
namespace App\Http\Controllers\Core\Agents;

use DB;
use Cache;
use App\Http\Controllers\Core\CacheController;

class IBlockActivitiesAgentController extends AgentController{
    public static function check(){
        $cache = Cache::get('agents_iblock_activities');
        return $cache==null;
    }
    public static function run(){
        $iblocks = [];
        $sections = DB::select('SELECT id, iblock_id FROM iblock_sections WHERE
          active_from IS NOT NULL AND
          active_to IS NOT NULL AND (
            (
              deleted_at IS NULL AND
              (active_from >= NOW() OR active_to <= NOW())
            ) OR (
              deleted_at IS NOT NULL AND
              active_from <= NOW() AND active_to >= NOW()
            )
          )');
        $section_ids = [];
        foreach($sections as $section){
            $section_ids[] = $section->id;
            if (!in_array($section->iblock_id, $iblocks)){
                $iblocks[] = $section->iblock_id;
            }
        }
        $items = DB::select('SELECT id, iblock_id FROM iblock_items WHERE
          active_from IS NOT NULL AND
          active_to IS NOT NULL AND (
            (
              deleted_at IS NULL AND
              (active_from >= NOW() OR active_to <= NOW())
            ) OR (
              deleted_at IS NOT NULL AND
              active_from <= NOW() AND active_to >= NOW()
            )
          )');
        $item_ids = [];
        foreach($items as $item){
            $item_ids[] = $item->id;
            if (!in_array($item->iblock_id, $iblocks)){
                $iblocks[] = $item->iblock_id;
            }
        }
        if (count($item_ids)!=0){
            DB::update('UPDATE iblock_items SET
                deleted_at = IF (active_from <= NOW() AND active_to >= NOW(), NULL, NOW())
                WHERE
                  id IN ('.implode(',',$item_ids).')');
        }

        if (count($section_ids)!=0){
            DB::update('UPDATE iblock_sections SET
                deleted_at = IF (active_from <= NOW() AND active_to >= NOW(), NULL, NOW())
                WHERE
                  id IN ('.implode(',',$section_ids).')');
        }
        foreach($iblocks as $iblock){
            CacheController::deleteByTag('iblock_'.$iblock);
        }
        Cache::put('agents_iblock_activities','Y',1);
    }
}