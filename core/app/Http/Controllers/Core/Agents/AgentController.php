<?php

namespace App\Http\Controllers\Core\Agents;
use App\Http\Controllers\Controller;
use Config;

abstract class AgentController extends Controller{
    static function run(){}
    static function check(){}
    static function exec(){
        $agents = config('app.agents');
        foreach ($agents as $agent){
            if ($agent::check()){
                $agent::run();
            }
        }
    }
}