<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\Modules\IBlockFields\IBlockFieldController;
use App\IBlock;
use App\IBlockSection;
use App\IBlockSectionProp;
use App\IBlockItem;
use App\IBlockItemProp;
use IBlockModule;
use Session;
use Config;
use App\File;

class IBlockController extends MainController
{
    public function buildSubMenu()
    {
        $iblocks = IBlock::all();
        $children = [
            [
                'text'=>'Список',
                'href'=>'/admin/iblock/list'
            ],
            [
                'text'=>'Создать новый инфоблок',
                'href'=>'/admin/iblock/create'
            ],[
                'text'=>'-'
            ]
        ];
        foreach($iblocks as $iblock){
            $children[] = [
                'text'=>$iblock->name,
                'href'=>'/admin/iblock/'.$iblock->id
            ];
        }
        return $children;
    }
    public static function getRoutes()
    {
        return [
            'create' => 'create_iblock',
            '{$id}/update' => 'update_iblock',
            '{$id}/create_prop_item' => 'create_prop_item',
            '{$id}/create_prop_section' => 'create_prop_section',
            '{$id}/props_section/{$id}/update' => 'update_prop_section',
            '{$id}/props_item/{$id}/update' => 'update_prop_item',
            '{$id}' => 'list_items_iblock',
            '{$id}/{$id}' => 'list_items_iblock',
            '{$id}/create_section' => 'create_section',
            '{$id}/create_item' => 'create_item',
            '{$id}/sections/{$id}/update' => 'update_section',
            '{$id}/items/{$id}/update' => 'update_item',
            '{$id}/items/{$id}/copy' => 'copy_item',
            '{$id}/{$id}/create_section' => 'create_section',
            '{$id}/{$id}/create_item' => 'create_item',
            '{$id}/list_props_item' => 'list_props_item',
            '{$id}/list_props_section' => 'list_props_section',
            '.*' => 'list_iblocks'
        ];
    }

    public function list_iblocks()
    {
        $iblocks = IBlock::all();
        return view("core.admin.iblock.list_iblocks", ['iblocks' => $iblocks]);
    }

    public function create_iblock()
    {
        return view("core.admin.iblock.create_iblock",[
            'scripts' => ['/admin/common/js/translit.js'],
        ]);
    }

    public function update_iblock($opt)
    {
        $iblock_id = $opt[1];
        $iblock = IBlock::findOrFail($iblock_id);
        return view("core.admin.iblock.update_iblock", [
            'scripts' => ['/admin/common/js/translit.js'],
            'iblock' => $iblock
        ]);
    }

    public function create_prop_item($opt)
    {
        $iblock_id = $opt[1];
        $iblock = IBlock::findOrFail($iblock_id);
        $iblocks = IBlock::all();
        $props = [];
        foreach (IBLockFieldController::$fields as $field) {
            $props[] = new $field;
        }
        return view("core.admin.iblock.create_prop_item", [
            'scripts' => ['/admin/common/js/translit.js'],
            'iblock' => $iblock,
            'props' => $props]
        );
    }

    public function update_prop_item($opt)
    {
        $iblock_id = $opt[1];
        $iblock = IBlock::findOrFail($iblock_id);
        $props = IBLockFieldController::$fields;
        foreach ($props as &$_prop) {
            $_prop = new $_prop;
        }
        $prop = IBlockItemProp::findOrFail($opt[2]);
        $prop->config = unserialize($prop->config);
        return view("core.admin.iblock.update_prop_item", [
            'scripts' => ['/admin/common/js/translit.js'],
            'iblock' => $iblock,
            'prop' => $prop,
            'props' => $props
        ]);
    }

    public function create_prop_section($opt)
    {
        $iblock_id = $opt[1];
        $iblock = IBlock::findOrFail($iblock_id);
        $iblocks = IBlock::all();
        $props = [];
        foreach (IBLockFieldController::$fields as $field) {
            $props[] = new $field;
        }
        return view("core.admin.iblock.create_prop_section", [
            'scripts' => ['/admin/common/js/translit.js'],
            'iblock' => $iblock,
            'props' => $props
        ]);
    }

    public function update_prop_section($opt)
    {
        $iblock_id = $opt[1];
        $iblock = IBlock::findOrFail($iblock_id);
        $props = IBLockFieldController::$fields;
        foreach ($props as &$_prop) {
            $_prop = new $_prop;
        }
        $prop = IBlockSectionProp::findOrFail($opt[2]);
        $prop->config = unserialize($prop->config);
        return view("core.admin.iblock.update_prop_section", [
            'scripts' => ['/admin/common/js/translit.js'],
            'iblock' => $iblock,
            'prop' => $prop,
            'props' => $props
        ]);
    }

    public function list_items_iblock($opt)
    {
        $iblock_id = intval($opt[1]);
        $section_id = 0;
        if (count($opt) == 3) $section_id = $opt[2];

        $thisIBlock = IBlock::findOrFail($iblock_id);
        $thisSection = null;
        if ($section_id != 0) $thisSection = IBlockSection::findOrFail($section_id);

        $filters = [];
        if (request()->input('filter') == 'Y') {
            $_filter_id = request()->input('id');
            $_filter_name = request()->input('name');
            $_filter_type = request()->input('type');
            $_filter_active = request()->input('active');
            $filterSections = [];
            $filterItems = [];
            if (!empty($_filter_id) && is_numeric($_filter_id)) {
                $filterSections[] = ['id', $_filter_id];
                $filterItems[] = ['id', $_filter_id];
                $filters['id'] = $_filter_id;
            }
            if (!empty($_filter_name)) {
                $filterSections[] = ['name', 'like', '%' . $_filter_name . '%'];
                $filterItems[] = ['name', 'like', '%' . $_filter_name . '%'];
                $filters['name'] = $_filter_name;
            }
            if (!empty($_filter_active)) {
                if ($_filter_active == 'y') {
                    $filterSections[] = ['active', 'y'];
                    $filterItems[] = ['active', 'y'];
                    $filters['active'] = 'y';
                } else if ($_filter_active == 'n') {
                    $filterSections[] = ['active', 'n'];
                    $filterItems[] = ['active', 'n'];
                    $filters['active'] = 'n';
                } else if ($_filter_active == 'all') {
                    $filterSections[] = ['active', 'all'];
                    $filterItems[] = ['active', 'all'];
                    $filters['active'] = 'all';
                }
            }
            if ($_filter_type == 'items') {
                $filters['type'] = 'items';
                $items = IBlockModule::GetItems($iblock_id, ['name','sort', 'alias', 'created_at'], $filterItems);
                $sections = ['items' => []];
            } else if ($_filter_type == 'sections') {
                $filters['type'] = 'sections';
                $sections = IBlockModule::GetSections($iblock_id, ['name','sort', 'alias', 'created_at'], $filterSections);
                $items = ['items' => []];
            } else {
                $filters['type'] = 'all';
                $sections = IBlockModule::GetSections($iblock_id, ['name','sort', 'alias', 'created_at'], $filterSections);
                $items = IBlockModule::GetItems($iblock_id, ['name','sort', 'alias', 'created_at'], $filterItems);
            }
        } else {
            $filterSections[] = ['parent_id', $section_id];
            $filterSections[] = ['active', 'all'];
            $filterItems[] = ['section_id', $section_id];
            $filterItems[] = ['active', 'all'];
            $sections = IBlockModule::GetSections($iblock_id, ['name','sort', 'alias', 'created_at'], $filterSections);
            $items = IBlockModule::GetItems($iblock_id, ['name','sort', 'alias', 'created_at'], $filterItems);
        }


        return view('core.admin.iblock.list_items_iblock', ['filter' => $filters, 'iblock' => $thisIBlock, 'section' => $thisSection, 'sections' => $sections, 'items' => $items]);
    }


    public function update_item($opt)
    {
        $iblock = IBlock::findOrFail($opt[1]);
        $props = $iblock->itemProps;
        $propsArray = ['name', 'alias', 'sort', 'section_id', 'active_from', 'active_to'];

        foreach ($props as &$prop) {
            $propsArray[] = 'prop.' . $prop->alias;
            $prop->config = unserialize($prop->config);
        }

        $item = IBlockModule::GetItemByID($opt[1], $opt[2], $propsArray, 'ALL');
        $arItemsID = [];
        $arSectionsID = [];
        $arFilesID = [];
        foreach ($props as $prop) {
            if (isset(IBlockFieldController::$fields[$prop->type])) {
                $prop->class = new IBlockFieldController::$fields[$prop->type];
            }
            $tmpArray = false;
            if ($prop->type == 'iblock_item') {
                $tmpArray = $arItemsID;
            } else if ($prop->type == 'iblock_section') {
                $tmpArray = $arSectionsID;
            } else if ($prop->type == 'file') {
                $tmpArray = $arFilesID;
            }
            if ($tmpArray === false) continue;
            if (isset($item[$prop->alias])) {
                if ($prop->many == '1') {
                    foreach ($item[$prop->alias] as $p) {
                        if (!in_array($p, $tmpArray)) $tmpArray[] = $p;
                    }
                } else {
                    if (!in_array($item[$prop->alias], $tmpArray)) $tmpArray[] = $item[$prop->alias];
                }
            }
            if ($prop->type == 'iblock_item') {
                $arItemsID = $tmpArray;
            } else if ($prop->type == 'iblock_section') {
                $arSectionsID = $tmpArray;
            } else if ($prop->type == 'file') {
                $arFilesID = $tmpArray;
            }
        }
        $propItems = [];
        if ($arItemsID != []) {
            $arItemsID = IBlockItem::whereIn('id', $arItemsID)->get();
            foreach ($arItemsID as &$arItem) {
                $propItems[$arItem->id] = [
                    'id' => $arItem->id,
                    'name' => $arItem->name
                ];
            }
        }
        $propSections = [];
        if ($arSectionsID != []) {
            $arSectionsID = IBlockSection::whereIn('id', $arSectionsID)->get();
            foreach ($arSectionsID as &$arSection) {
                $propSections[$arSection->id] = [
                    'id' => $arSection->id,
                    'name' => $arSection->name
                ];
            }
        }
        $propFiles = [];
        if ($arFilesID != []) {
            $arFilesID = File::whereIn('id', $arFilesID)->get();
            foreach ($arFilesID as &$arFile) {
                $propFiles[$arFile->id] = [
                    'id' => $arFile->id,
                    'name' => $arFile->name
                ];
            }
        }

        if ($item['section_id'] == 0) {
            $section = ['id' => 0, 'name' => 'Корневой раздел'];
        } else {
            $section = IBlockModule::GetSectionByID($opt[1], $item['section_id'], ['name'], 'ALL');
            if (!$section) abort(404);
        }

        return view('core.admin.iblock.update_item', [
            'scripts' => ['//cdn.tinymce.com/4/tinymce.min.js','/admin/common/js/translit.js'],
            'modals' => ['sections', 'items', 'files', 'load_files'],
            'iblock' => $iblock,
            'section' => $section,
            'item' => $item,
            'props' => $props,
            'prop_items' => $propItems,
            'prop_sections' => $propSections,
            'files' => $propFiles
        ]);
    }

    public function update_section($opt)
    {
        $iblock = IBlock::findOrFail($opt[1]);
        $props = $iblock->sectionProps;
        $propsArray = ['name', 'alias', 'sort', 'parent_id', 'active_from', 'active_to'];

        foreach ($props as &$prop) {
            $propsArray[] = 'prop.' . $prop->alias;
            $prop->config = unserialize($prop->config);
        }

        $item = IBlockModule::GetSectionByID($opt[1], $opt[2], $propsArray, 'ALL');
        $arItemsID = [];
        $arSectionsID = [];
        $arFilesID = [];
        foreach ($props as $prop) {
            if (isset(IBlockFieldController::$fields[$prop->type])) {
                $prop->class = new IBlockFieldController::$fields[$prop->type];
            }
            $tmpArray = false;
            if ($prop->type == 'iblock_item') {
                $tmpArray = $arItemsID;
            } else if ($prop->type == 'iblock_section') {
                $tmpArray = $arSectionsID;
            } else if ($prop->type == 'file') {
                $tmpArray = $arFilesID;
            }
            if ($tmpArray === false) continue;
            if (isset($item[$prop->alias])) {
                if ($prop->many == '1') {
                    foreach ($item[$prop->alias] as $p) {
                        if (!in_array($p, $tmpArray)) $tmpArray[] = $p;
                    }
                } else {
                    if (!in_array($item[$prop->alias], $tmpArray)) $tmpArray[] = $item[$prop->alias];
                }
            }
            if ($prop->type == 'iblock_item') {
                $arItemsID = $tmpArray;
            } else if ($prop->type == 'iblock_section') {
                $arSectionsID = $tmpArray;
            } else if ($prop->type == 'file') {
                $arFilesID = $tmpArray;
            }
        }
        $propItems = [];
        if ($arItemsID != []) {
            $arItemsID = IBlockItem::whereIn('id', $arItemsID)->get();
            foreach ($arItemsID as &$arItem) {
                $propItems[$arItem->id] = [
                    'id' => $arItem->id,
                    'name' => $arItem->name
                ];
            }
        }
        $propSections = [];
        if ($arSectionsID != []) {
            $arSectionsID = IBlockSection::whereIn('id', $arSectionsID)->get();
            foreach ($arSectionsID as &$arSection) {
                $propSections[$arSection->id] = [
                    'id' => $arSection->id,
                    'name' => $arSection->name
                ];
            }
        }
        $propFiles = [];
        if ($arFilesID != []) {
            $arFilesID = File::whereIn('id', $arFilesID)->get();
            foreach ($arFilesID as &$arFile) {
                $propFiles[$arFile->id] = [
                    'id' => $arFile->id,
                    'name' => $arFile->name
                ];
            }
        }

        if ($item['parent_id'] == 0) {
            $section = ['id' => 0, 'name' => 'Корневой раздел'];
        } else {
            $section = IBlockModule::GetSectionByID($opt[1], $item['parent_id'], ['name'], 'ALL');
            if (!$section) abort(404);
        }
        return view('core.admin.iblock.update_section', [
            'scripts' => ['//cdn.tinymce.com/4/tinymce.min.js','/admin/common/js/translit.js'],
            'modals' => ['sections', 'items', 'files', 'load_files'],
            'iblock' => $iblock,
            'section' => $section,
            'item' => $item,
            'props' => $props,
            'prop_items' => $propItems,
            'prop_sections' => $propSections,
            'files' => $propFiles
        ]);
    }

    public function copy_item($opt)
    {
        $iblock = IBlock::findOrFail($opt[1]);
        $props = $iblock->itemProps;
        $propsArray = ['name', 'alias', 'sort', 'section_id', 'active_from', 'active_to'];

        foreach ($props as &$prop) {
            $propsArray[] = 'prop.' . $prop->alias;
            $prop->config = unserialize($prop->config);
        }

        $item = IBlockModule::GetItemByID($opt[1], $opt[2], $propsArray, 'ALL');
        $arItemsID = [];
        $arSectionsID = [];
        $arFilesID = [];
        foreach ($props as $prop) {
            if (isset(IBlockFieldController::$fields[$prop->type])) {
                $prop->class = new IBlockFieldController::$fields[$prop->type];
            }
            $tmpArray = false;
            if ($prop->type == 'iblock_item') {
                $tmpArray = $arItemsID;
            } else if ($prop->type == 'iblock_section') {
                $tmpArray = $arSectionsID;
            } else if ($prop->type == 'file') {
                $tmpArray = $arFilesID;
            }
            if ($tmpArray === false) continue;
            if (isset($item[$prop->alias])) {
                if ($prop->many == '1') {
                    foreach ($item[$prop->alias] as $p) {
                        if (!in_array($p, $tmpArray)) $tmpArray[] = $p;
                    }
                } else {
                    if (!in_array($item[$prop->alias], $tmpArray)) $tmpArray[] = $item[$prop->alias];
                }
            }
            if ($prop->type == 'iblock_item') {
                $arItemsID = $tmpArray;
            } else if ($prop->type == 'iblock_section') {
                $arSectionsID = $tmpArray;
            } else if ($prop->type == 'file') {
                $arFilesID = $tmpArray;
            }
        }
        $propItems = [];
        if ($arItemsID != []) {
            $arItemsID = IBlockItem::whereIn('id', $arItemsID)->get();
            foreach ($arItemsID as &$arItem) {
                $propItems[$arItem->id] = [
                    'id' => $arItem->id,
                    'name' => $arItem->name
                ];
            }
        }
        $propSections = [];
        if ($arSectionsID != []) {
            $arSectionsID = IBlockSection::whereIn('id', $arSectionsID)->get();
            foreach ($arSectionsID as &$arSection) {
                $propSections[$arSection->id] = [
                    'id' => $arSection->id,
                    'name' => $arSection->name
                ];
            }
        }
        $propFiles = [];
        if ($arFilesID != []) {
            $arFilesID = File::whereIn('id', $arFilesID)->get();
            foreach ($arFilesID as &$arFile) {
                $propFiles[$arFile->id] = [
                    'id' => $arFile->id,
                    'name' => $arFile->name
                ];
            }
        }

        if ($item['section_id'] == 0) {
            $section = ['id' => 0, 'name' => 'Корневой раздел'];
        } else {
            $section = IBlockModule::GetSectionByID($opt[1], $item['section_id'], ['name'], 'ALL');
            if (!$section) abort(404);
        }

        return view('core.admin.iblock.copy_item', [
            'scripts' => ['//cdn.tinymce.com/4/tinymce.min.js','/admin/common/js/translit.js'],
            'modals' => ['sections', 'items', 'files', 'load_files'],
            'iblock' => $iblock,
            'section' => $section,
            'item' => $item,
            'props' => $props,
            'prop_items' => $propItems,
            'prop_sections' => $propSections,
            'files' => $propFiles
        ]);
    }

    public function list_props_section($opt)
    {
        if (isset($opt[1])) {
            $iblock = IBlock::findOrFail($opt[1]);
            $props = IBlockSectionProp::where('iblock_id', $iblock->id)->paginate();
            foreach ($props as &$prop) {
                if (isset(IBlockFieldController::$fields[$prop->type])){
                    $field = new IBlockFieldController::$fields[$prop->type];
                    $prop->typeName = $field->getName();
                }
            }
            return view('core.admin.iblock.list_props_section', [
                'props' => $props,
                'iblock' => $iblock
            ]);
        } else {
            abort(404);
        }
    }

    public function list_props_item($opt)
    {
        if (isset($opt[1])) {
            $iblock = IBlock::findOrFail($opt[1]);
            $props = IBlockItemProp::where('iblock_id', $iblock->id)->paginate();
            foreach ($props as &$prop) {
                if (isset(IBlockFieldController::$fields[$prop->type])){
                    $field = new IBlockFieldController::$fields[$prop->type];
                    $prop->typeName = $field->getName();
                }
            }
            return view('core.admin.iblock.list_props_item', [
                'props' => $props,
                'iblock' => $iblock
            ]);
        } else {
            abort(404);
        }
    }

    public function create_item($opt)
    {
        $iblock = IBlock::findOrFail($opt[1]);
        $section = null;
        if (count($opt) == 3) $section = IBlockSection::findOrFail($opt[2]);
        $props = IBLockItemProp::where('iblock_id', $iblock->id)->get();
        foreach ($props as &$prop) {
            if (isset(IBlockFieldController::$fields[$prop->type])) {
                $prop->class = new IBlockFieldController::$fields[$prop->type];
            }
            $prop['config'] = unserialize($prop['config']);
        }
        return view('core.admin.iblock.create_item', [
            'scripts' => ['//cdn.tinymce.com/4/tinymce.min.js','/admin/common/js/translit.js'],
            'modals' => ['sections', 'items', 'files', 'load_files'],
            'iblock' => $iblock,
            'section' => $section,
            'props' => $props
        ]);
    }

    public function create_section($opt)
    {
        $iblock = IBlock::findOrFail($opt[1]);
        $section = null;
        if (count($opt) == 3) $section = IBlockSection::findOrFail($opt[2]);
        $props = IBLockSectionProp::where('iblock_id', $iblock->id)->get();
        foreach ($props as &$prop) {
            if (isset(IBlockFieldController::$fields[$prop->type])) {
                $prop->class = new IBlockFieldController::$fields[$prop->type];
            }
            $prop['config'] = unserialize($prop['config']);
        }
        return view('core.admin.iblock.create_section', [
            'scripts' => ['//cdn.tinymce.com/4/tinymce.min.js','/admin/common/js/translit.js'],
            'modals' => ['sections', 'items', 'files', 'load_files'],
            'iblock' => $iblock,
            'section' => $section,
            'props' => $props
        ]);
    }
}