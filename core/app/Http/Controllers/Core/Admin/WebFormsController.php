<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\Modules\WebFormsFields\WebFormsFieldController;
use App\Http\Controllers\Core\Modules\WebFormsModuleController;
use Session;
use Config;
use Gate;
use WebForm;
use WebFormsModule;

class WebFormsController extends MainController
{
    public function buildSubMenu()
    {
        $showResult = Gate::check('webforms_show_result');
        $create = Gate::check('webforms_create');
        $forms = WebFormsModule::getWebForms();
        $menu = [];
        $menu[] = [
            'text'=>'Список',
            'href'=>'/admin/webforms/'
        ];
        if ($create){
            $menu[] = [
                'text'=>'Создать',
                'href'=>'/admin/webforms/create'
            ];
        }
        foreach($forms as $form){
            $menu[] = [
                'text' => '-'
            ];
            $menu[] = [
                'text'=>$form['name']
            ];
            if ($showResult){
                $menu[] = [
                    'text'=>'Результаты',
                    'href'=>'/admin/webforms/'.$form['id'].'/results'
                ];
            }
            if ($create){
                $menu[] = [
                    'text'=>'Настройки',
                    'href'=>'/admin/webforms/'.$form['id'].'/edit'
                ];
            }
        }
        return $menu;
    }

    public static function getRoutes()
    {
        return [
            'list' => 'list_webforms',
            'create' => 'create_webform',
            '{$id}/edit' => 'edit_webform',
            '{$id}/fields/create' => 'create_field',
            '{$id}/fields' => 'list_fields',
            '{$id}/results' => 'list_results',
            '{$id}/fields/{$id}/edit' => 'edit_field',
            '{$id}/results/{$id}' => 'show_result',
            '.*' => 'list_webforms'
        ];
    }

    public function list_webforms(){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        $forms = WebFormsModuleController::getWebForms(true);
        return view('core.admin.webforms.list',['webforms'=>$forms]);
    }

    public function create_webform(){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        return view('core.admin.webforms.create');
    }

    public function edit_webform($opt){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        if (!isset($opt[1])){
            abort(404);
        }
        $webform = WebFormsModuleController::getWebFormByID($opt[1]);
        if ($webform===null) abort(404);
        return view('core.admin.webforms.edit',['webform'=>$webform]);
    }

    public function create_field($opt){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        if (!isset($opt[1])){
            abort(404);
        }
        $webform = WebFormsModuleController::getWebFormByID($opt[1]);
        if ($webform==null) abort(404);
        $props = [];
        foreach (WebFormsFieldController::$fields as $field) {
            $props[] = new $field;
        }
        return view('core.admin.webforms.create_field',['webform'=>$webform,'props'=>$props]);
    }

    public function edit_field($opt){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        if (!isset($opt[1],$opt[2])){
            abort(404);
        }
        $webform = WebFormsModuleController::getWebFormByID($opt[1]);
        if ($webform==null) abort(404);
        $field = WebFormsModuleController::getFieldByID($opt[2]);
        if ($field==null || $webform['id']!=$field['webform_id']) abort(404);
        $props = [];
        foreach (WebFormsFieldController::$fields as $_field) {
            $props[] = new $_field;
        }
        return view('core.admin.webforms.edit_field',['webform'=>$webform,'field'=>$field,'props'=>$props]);
    }

    public function list_fields($opt){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        if (!isset($opt[1])){
            abort(404);
        }
        $webform = WebFormsModuleController::getWebFormByID($opt[1]);
        if ($webform==null) abort(404);
        $fields = WebFormsModuleController::getFields($opt[1]);
        foreach($fields as &$field){
            if (isset(WebFormsFieldController::$fields[$field['type']])){
                $_field = new WebFormsFieldController::$fields[$field['type']];
                $field['type'] = $_field->getName();
            }
        }
        return view('core.admin.webforms.list_fields',[
            'webform'=>$webform,
            'fields'=>$fields
        ]);
    }

    public function list_results($opt){
        if (!Gate::check("webforms_module")){
            abort(403);
        }
        if (!isset($opt[1])){
            abort(404);
        }
        $webform = WebFormsModuleController::getWebFormByID($opt[1]);
        if ($webform==null) abort(404);
        $results = WebFormsModuleController::getResults($opt[1]);
        return view('core.admin.webforms.list_results',[
            'webform'=>$webform,
            'results'=>$results
        ]);
    }

    public function show_result($opt){
        if (!Gate::check('webforms_module')){
            abort(403);
        }
        $result = WebFormsModuleController::getResultByID($opt[2]);
        if ($result==null || $result['webform_id']!=$opt[1]) abort(404);
        return view('core.admin.webforms.result',[
            'result'=>$result
        ]);
    }
}