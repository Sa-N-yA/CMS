<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use Session;
use Config;
use Gate;
use App\User;
use App\Group;

class UsersController extends MainController
{
    public function buildSubMenu()
    {
        return [
            [
                'text'=>'Список',
                'href'=>'/admin/users/list'
            ],
            [
                'text'=>'Группы',
                'href'=>'/admin/users/groups/list'
            ]
        ];
    }
    public static function getRoutes()
    {
        return [
            'list' => 'list_users',
            'create' => 'create_user',
            'gates' => 'list_gates',
            'gates/list' => 'list_gates',
            'groups' => 'list_groups',
            'groups/list' => 'list_groups',
            'groups/create' => 'create_group',
            'groups/{$id}/update' => 'update_group',
            '{$id}/update' => 'update_user',
            '.*' => 'list_users'
        ];
    }
    public static function list_users(){
        if (!Gate::check('users_module')) abort(403);
        $users = User::all();
        return view('core.admin.users.list_users',['users'=>$users]);
    }
    public static function list_gates(){
        if (!Gate::check('users_module')) abort(403);
        $gates = \App\Gate::all();
        return view('core.admin.users.list_gates',['gates'=>$gates]);
    }
    public static function list_groups(){
        if (!Gate::check('users_module')) abort(403);
        $groups = \App\Group::all();
        return view('core.admin.users.list_groups',['groups'=>$groups]);
    }
    public static function create_user(){
        if (!Gate::check('users_module')) abort(403);
        $groups = Group::all();
        return view('core.admin.users.create_user',['groups'=>$groups]);
    }
    public static function create_group(){
        if (!Gate::check('users_module')) abort(403);
        $gates = \App\Gate::all();
        return view('core.admin.users.create_group',['gates'=>$gates]);
    }
    public static function update_group($opt){
        if (!Gate::check('users_module')) abort(403);
        $gates = \App\Gate::all();
        $user = Group::where('id',$opt[1])->with('gates')->firstOrFail();
        foreach($gates as &$gate) $gate->active = false;
        foreach($user->gates as $gate){
            foreach($gates as &$_gate){
                if ($_gate->id == $gate->id) $_gate->active = true;
            }
        }
        return view('core.admin.users.update_group',['gates'=>$gates,'group'=>$user]);
    }
    public static function update_user($opt){
        if (!Gate::check('users_module')) abort(403);
        $groups = Group::all();
        $user = User::where('id',$opt[1])->with('groups')->firstOrFail();
        foreach($groups as &$group) $group->active = false;
        foreach($user->groups as $group){
            foreach($groups as &$_group){
                if ($_group->id == $group->id) $_group->active = true;
            }
        }
        return view('core.admin.users.update_user',['groups'=>$groups,'user'=>$user]);
    }
}