<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CacheController;
use Session;
use Config;
use Cache;
use Gate;
use File;
use Storage;
use Response;
use DB;

class CoreController extends MainController
{
    public function buildSubMenu()
    {
        if (Gate::check('core_module')){
            $menu = [];
            $menu[] = [
                'text'=>'Статистика сайта',
                'href'=>'/admin/core/statistic/'
            ];
            $menu[] = [
                'text'=>'-'
            ];
            if (Gate::check('cache_manage')){
                $menu[] = [
                    'text'=>'Очистить кеш',
                    'href'=>'/admin/core/clear_cache'
                ];
                $menu[] = [
                    'text'=>(CacheController::useCache()?'Отключить':'Включить').' кеш',
                    'href'=>'/admin/core/toogle_cache'
                ];
            }
            if (Gate::check('access_system_file')){
                if ($menu!=[]) $menu[] = ['text'=>'-'];
                $menu[] = [
                    'text'=>'Системные файлы',
                    'href'=>'/admin/core/files'
                ];
            }
            if (Gate::check('backup_manage')){
                if ($menu!=[]) $menu[] = ['text'=>'-'];
                $menu[] = [
                    'text'=>'Резервные копии',
                    'href'=>'/admin/core/backups'
                ];
            }
            return $menu;
        }   else    {
            return [];
        }
        /*return [
            [
                'text'=>'Список',
                'href'=>'/admin/users/list'
            ],
            [
                'text'=>'Группы',
                'href'=>'/admin/users/groups/list'
            ],
            [
                'text'=>'Гейты',
                'href'=>'/admin/users/gates/list'
            ]
        ];*/
    }
    private static function format_size($size){
        $metrics[0] = 'байт';
        $metrics[1] = 'Кбайт';
        $metrics[2] = 'Мбайт';
        $metrics[3] = 'Гбайт';
        $metrics[4] = 'Тбайт';
        $metric = 0;
        while(floor($size / 1024) > 0){
            $metric ++;
            $size /= 1024;
        }
        $result = round($size, 1) . " " .
            (isset($metrics[$metric]) ? $metrics[$metric] : '???');
        return $result;
    }

    public static function getRoutes()
    {
        return [
            'clear_cache' => 'clear_cache',
            'toogle_cache' => 'toogle_cache',
            'files' => 'files',
            'backups' => 'backups',
            'statistic' => 'statistic',
            'backups/(backup(.*))/download'=>'backup_download',
            '.*' => 'main'
        ];
    }

    public static function files(){
        return view('core.admin.core.files',[
            'scripts'=>['/admin/common/js/jquery-ui.min.js','/admin/common/js/files.js'],
            'styles'=>['/admin/common/css/jquery-ui.min.css','/admin/common/css/files.css']
        ]);
    }

    public static function clear_cache(){
        if (!Gate::check('cache_manage')) abort(403);
        Cache::flush();
        return redirect()->back();
    }

    public static function toogle_cache(){
        if (!Gate::check('cache_manage')) abort(403);
        $conf = Config::get('cms');
        $conf['cache'] = !$conf['cache'];
        $data = var_export($conf, true);
        File::put(base_path() . DIRECTORY_SEPARATOR. 'config'.DIRECTORY_SEPARATOR.'cms.php', "<?php\n return $data ;");

        return redirect()->back();
    }

    public static function backups(){
        if (!Gate::check('backup_manage')) abort(403);
        $backupsTmp = Storage::disk('backups')->files();
        $backups = [];
        $i = 1;
        foreach($backupsTmp as $backup){
            $backups[] = [
                'id'=>$i++,
                'name'=>$backup,
                'size'=>self::format_size(Storage::disk('backups')->size($backup)),
                'date'=>date('Y-m-d H:i:s', Storage::disk('backups')->lastModified($backup))
            ];
        }
        $isMakeBackup = false;
        if (Cache::has('backup')){
            $cache = unserialize(Cache::get('backup'));
            $isMakeBackup = $cache['status'] != 'success';
        }
        return view('core.admin.core.backup',['backups'=>$backups,'isMakeBackup'=>$isMakeBackup]);
    }

    public static function statistic($opt){
        $result = DB::table('inquiries')->get();
        $res = [
            '<100'=>0,
            '100-200'=>0,
            '200-300'=>0,
            '300-400'=>0,
            '400-500'=>0,
            '500-600'=>0,
            '600-700'=>0,
            '700-800'=>0,
            '800-900'=>0,
            '900-1000'=>0,
            '1000-1100'=>0,
            '1100-1200'=>0,
            '1200-1300'=>0,
            '1300-1400'=>0,
            '1400-1500'=>0,
            '>1500'=>0
        ];
        foreach($result as $r){
            if ($r->time_generate<=100){
                $res['<100']++;
                continue;
            }
            if ($r->time_generate>1500){
                $res['>1500']++;
                continue;
            }
            for($i = 1; $i<15; $i++){
                if ($r->time_generate>$i*100 && $r->time_generate<=($i+1)*100){
                    $res[($i*100).'-'.(($i+1)*100)]++;
                }
            }
        }
        $values = implode(',',array_values($res));
        $keys = "'".implode("','",array_keys($res))."'";
        return view('core.admin.core.statistic',[
            'scripts'=>['/admin/common/js/flotr.js','/admin/common/js/statistic.js'],
            'styles'=>['/admin/common/css/flotr.css'],
            'values'=>$values,
            'keys'=>$keys
        ]);
    }

    public static function backup_download($opt){
        if (!Gate::check('backup_manage')) abort(403);
        $backupName = $opt[1];
        if (Storage::disk('backups')->exists($backupName)){
            if (ob_get_level()) {
                ob_end_clean();
            }
            /*Response::header('Content-Description','File Transfer');
            Response::header('Content-Type','application/octet-stream');
            Response::header('Content-Disposition',' attachment; filename='.$backupName);
            Response::header('Content-Transfer-Encoding',' binary');
            Response::header('Expires',' 0');
            Response::header('Cache-Control',' must-revalidate');
            Response::header('Pragma',' public');
            Response::header('Content-Length',Storage::disk('backups')->size($backupName));*/
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$backupName);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: '.Storage::disk('backups')->size($backupName));
            // читаем файл и отправляем его пользователю
            readfile($_SERVER['DOCUMENT_ROOT'].'/core/storage/backups/'.$backupName);
        }   else    {
            abort(404);
        }
    }
}