<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:44
 */

namespace App\Http\Controllers\Core\Admin;


use App\Http\Controllers\Controller;
use Session;
use Config;
use Cache;
use Gate;
use File;
use Storage;
use Response;
use DB;
use SiteModule;

class SiteController extends MainController
{
    public function buildSubMenu()
    {
        $menu = [];
        if (Gate::check("site_pages_show")){
            $menu[] = [
                'text'=>'Страницы',
                'href'=>'/admin/site/pages'
            ];
        }
        if (Gate::check("site_templates")){
            $menu[] = [
                'text'=>'Шаблоны',
                'href'=>'/admin/site/templates'
            ];
        }
        if (Gate::check("site_toggle_enabled")){
            $menu[] = [
                'text'=>(SiteModule::isSiteEnabled()?'Отключить сайт':'Включить сайт'),
                'href'=>'/admin/site/toggle_enabled'
            ];
        }

        return $menu;
    }

    public static function getRoutes()
    {
        return [
            'toggle_enabled' => 'toggle_enabled',
            'pages' => 'pages_list',
            'pages/create' => 'pages_create',
            'pages/{$id}/list' => 'pages_list',
            'pages/{$id}/edit' => 'pages_edit',
            'templates' => 'templates_list',
            'templates/create' => 'templates_create',
            'templates/{$id}/edit' => 'templates_edit',
            'templates/refresh' => 'templates_refresh',
            '.*' => 'pages_list'
        ];
    }

    public static function toggle_enabled($opt){
        if (!Gate::check("site_toggle_enabled")){
            abort(403);
        }
        $conf = Config::get('site');
        $conf['enabled'] = !$conf['enabled'];
        $data = var_export($conf, true);
        File::put(base_path() . DIRECTORY_SEPARATOR. 'config'.DIRECTORY_SEPARATOR.'site.php', "<?php\n return $data;");
        return redirect()->back();
    }

    public static function pages_list($opt){
        if (!Gate::check("site_pages_show")){
            abort(403);
        }
        $parent = 0;
        if (isset($opt[1])){
            $parent = intval($opt[1]);
        }
        $pages = SiteModule::getPages($parent);
        $cur_id = false;
        if ($parent>0){
            $cur_id = SiteModule::getParentPageByID($parent);
        }
        return view('core.admin.site.pages_list', ['cur_id'=>$cur_id,'pages'=>$pages]);
    }

    public static function pages_create($opt){
        if (!Gate::check("site_pages_show")){
            abort(403);
        }
        $parent_pages = SiteModule::getPagesTree();
        $templates = SiteModule::getTemplates();
        return view('core.admin.site.page_create',['scripts'=>['/admin/common/js/ace.js'],'parent_pages'=>$parent_pages,'templates'=>$templates]);
    }

    public static function pages_edit($opt){
        if (!Gate::check("site_pages_show")){
            abort(403);
        }
        if (!isset($opt[1])) abort(404);
        $page = SiteModule::getPageByID($opt[1], true);
        $parent_pages = SiteModule::getPagesTree();
        $templates = SiteModule::getTemplates();
        return view('core.admin.site.page_edit',['scripts'=>['/admin/common/js/ace.js'],'page'=>$page,'templates'=>$templates,'parent_pages'=>$parent_pages]);
    }

    public static function templates_list($opt){
        if (!Gate::check("site_templates")){
            abort(403);
        }
        $templates = SiteModule::getTemplates();
        return view('core.admin.site.templates_list',['templates'=>$templates]);
    }

    public static function templates_refresh(){
        if (!Gate::check("site_templates")){
            abort(403);
        }
        $templates = SiteModule::getTemplates();
        $templatesDisk = Storage::disk('templates')->directories();
        $templatesRemove = [];
        foreach($templates as $template){
            $found = false;
            foreach($templatesDisk as $templateDiskIndex=>$templateDisk){
                if ($template->alias==$templateDisk){
                    $found = true;
                    unset($templatesDisk[$templateDiskIndex]);
                    break;
                }
            }
            if (!$found){
                $templatesRemove[] = $template->alias;
            }
        }
        $templatesAdd = $templatesDisk;
        foreach($templatesAdd as $template){
            SiteModule::addTemplate([
                'name'=>'Без названия',
                'alias'=>$template,
                'developer'=>'',
                'description'=>''
            ]);
        }
        foreach($templatesRemove as $template){
            SiteModule::removeTemplateByAlias($template);
        }
        return redirect()->back();
    }

    public static function templates_create($opt){
        if (!Gate::check("site_templates")){
            abort(403);
        }
        return view('core.admin.site.template_create',[
            'scripts'=>['/admin/common/js/ace.js']
        ]);
    }
    public static function templates_edit($opt){
        if (!Gate::check("site_templates")){
            abort(403);
        }
        if (!isset($opt[1])){
            abort(404);
        }
        $template = SiteModule::getTemplateByID($opt[1], true);
        if ($template===false) abort(404);
        return view('core.admin.site.template_edit',[
            'template'=>$template,
            'scripts'=>['/admin/common/js/ace.js']
        ]);
    }

}