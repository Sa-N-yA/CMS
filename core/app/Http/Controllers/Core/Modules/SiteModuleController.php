<?php

namespace App\Http\Controllers\Core\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CacheController;
use App\Page;
use App\Template;
use Gate;
use Config;
use Storage;
use Validator;

class SiteModuleController extends ModuleController{
    private static $pages = null;

    public static function isSiteEnabled(){
        return Config::get('site.enabled');
    }

    public static function getPages($parent_pages = -1){
        if (static::$pages!=null){
            $pages = static::$pages;
        }   else {
            $cache = CacheController::get('pages');
            if ($cache) {
                $pages = unserialize($cache);
            } else {
                $pages = Page::orderBy('sort')->get();
                foreach ($pages as $page) {
                    $page->count = 0;
                }
                foreach ($pages as $page) {
                    $parent = $page->parent_id;
                    if ($parent > 0) {
                        foreach ($pages as $page2) {
                            if ($page2->id == $parent) {
                                $page2->count++;
                            }
                        }
                    }
                }
                CacheController::put('pages', serialize($pages), ['uri']);
            }
        }
        $rPages = [];
        foreach($pages as $page){
            if ($page->parent_id==$parent_pages || $parent_pages==-1){
                $rPages[] = $page;
            }
        }
        return $rPages;
    }

    public static function getPageByID($id, $source = false){
        $pages = static::getPages(-1);
        foreach($pages as $page){
            if ($page->id==$id){
                if ($source){
                    $fileName = str_replace('.',DIRECTORY_SEPARATOR,$page->view).'.blade.php';
                    if (Storage::disk('site')->exists($fileName)) {
                        $page->source = Storage::disk('site')->get(str_replace('.', DIRECTORY_SEPARATOR, $page->view) . '.blade.php');
                    }   else    {
                        $page->source = "";
                    }
                }
                return $page;
            }
        }
        return null;
    }

    public static function getPagesByParentID($parent_id){
        $pages = static::getPages();
        $rPages = [];
        foreach($pages as $page){
            if ($page->parent_id == $parent_id){
                $rPages[] = $page;
            }
        }
        return $rPages;
    }

    public static function getParentPageByID($id){
        $page = static::getPageByID($id);
        if ($page!=null){
            return $page->parent_id;
        }   else    {
            return false;
        }
    }

    public static function getPagesTree($id = 0){
        $res = static::getPagesByParentID($id);
        foreach($res as $r){
            $r->children = static::getPagesTree($r->id);
        }
        return $res;
    }

    public static function getTemplates(){
        $cache = CacheController::get('templates');
        if ($cache){
            return unserialize($cache);
        }
        $templates = Template::all();
        CacheController::put('templates',serialize($templates), ['templates']);
        return $templates;
    }

    public static function createPage($opt){
        if ($opt['parent']==0) {
            $v = Validator::make($opt, [
                'name' => 'required|min:1|max:255',
                'alias' => 'required|regex:([a-zA-Z][a-zA-Z0-9_]*)',
                'view' => 'required|min:3',
                'sort' => 'required|numeric',
                'template' => 'required|numeric|exists:templates,id',
                'complex'=> 'required|in:N,Y'
            ]);
        }   else    {
            $v = Validator::make($opt, [
                'name' => 'required|min:1|max:255',
                'alias' => 'required|regex:([a-zA-Z][a-zA-Z0-9_]*)',
                'view' => 'required|min:3',
                'sort' => 'required|numeric',
                'parent' => 'required|numeric|exists:pages,id',
                'template' => 'required|numeric|exists:templates,id',
                'complex'=> 'required|in:N,Y'
            ]);
        }
        if ($v->fails()) return $v->errors();
        $page = new Page;
        $page->name = $opt['name'];
        $page->view = $opt['view'];
        $page->alias = $opt['alias'];
        $page->sort = $opt['sort'];
        $page->sort = $opt['sort'];
        $page->parent_id = $opt['parent'];
        $page->template = $opt['template'];
        $page->description = $opt['meta_description'];
        $page->keywords = $opt['meta_keywords'];
        $page->title = $opt['title'];
        $page->complex = $opt['complex'];
        $resDB = $page->save();
        if (!$resDB){
            return 'DB Error';
        }   else    {
            Storage::disk('site')->put(str_replace('.',DIRECTORY_SEPARATOR,$opt['view']).'.blade.php',$opt['source']);
            CacheController::deleteByTag('uri');
            return true;
        }
    }

    public static function editPage($opt){
        if ($opt['parent']==0) {
            $v = Validator::make($opt, [
                'name' => 'required|min:1|max:255',
                'alias' => 'required|regex:([a-zA-Z][a-zA-Z0-9_]*)',
                'view' => 'required|min:3',
                'sort' => 'required|numeric',
                'template' => 'required|numeric|exists:templates,id',
                'complex'=> 'required|in:N,Y'
            ]);
        }   else    {
            $v = Validator::make($opt, [
                'name' => 'required|min:1|max:255',
                'alias' => 'required|regex:([a-zA-Z][a-zA-Z0-9_]*)',
                'view' => 'required|min:3',
                'sort' => 'required|numeric',
                'parent' => 'required|numeric|exists:pages,id',
                'template' => 'required|numeric|exists:templates,id',
                'complex'=> 'required|in:N,Y'
            ]);
        }
        if ($v->fails()) return $v->errors();
        $page = Page::find($opt['id']);
        if ($page==null) return false;
        $page->name = $opt['name'];
        $page->view = $opt['view'];
        $page->alias = $opt['alias'];
        $page->sort = $opt['sort'];
        $page->parent_id = $opt['parent'];
        $page->template = $opt['template'];
        $page->description = $opt['meta_description'];
        $page->keywords = $opt['meta_keywords'];
        $page->title = $opt['title'];
        $page->complex = $opt['complex'];
        $resDB = $page->save();
        if (!$resDB){
            return 'DB Error';
        }   else    {
            Storage::disk('site')->put(str_replace('.',DIRECTORY_SEPARATOR,$opt['view']).'.blade.php',$opt['source']);
            CacheController::deleteByTag('uri');
            return true;
        }
    }

    public static function removePageByID($id, $remove_file = false){
        if (!empty($id)){
            CacheController::deleteByTag('uri');
            $page = static::getPageByID($id);
            if ($page==null) return false;
            if ($remove_file){
                Storage::disk('site')->delete(str_replace('.',DIRECTORY_SEPARATOR,$page->view).'.blade.php');
            }
            return $page->delete();
        }   else    {
            return false;
        }
    }

    public static function addTemplate($opt){
        if (isset($opt['name'],$opt['alias'],$opt['description'],$opt['developer'])){
            $template = new Template();
            $template->name = $opt['name'];
            $template->alias = $opt['alias'];
            $template->description = $opt['description'];
            $template->developer = $opt['developer'];
            CacheController::deleteByTag('templates');
            return $template->save();
        }   else    {
            return false;
        }
    }

    public static function removeTemplateByAlias($alias, $remove_folder = false){
        if (!empty($alias)){
            CacheController::deleteByTag('templates');
            if ($remove_folder){
                Storage::disk('templates')->deleteDirectory($alias);
            }
            return Template::where('alias',$alias)->delete();
        }   else    {
            return false;
        }
    }

    public static function createTemplate($opt){
        $v = Validator::make($opt, [
            'name' => 'required|min:1|max:255',
            'alias' => 'required|regex:([a-zA-Z][a-zA-Z0-9_]*)',
        ]);
        if ($v->fails()) return $v->errors();
        if (Storage::disk('templates')->exists($opt['alias'])) {
            return [
                'alias'=>['Template use exists']
            ];
        }
        $resDB = static::addTemplate($opt);
        if (!$resDB){
            return 'DB Error';
        }   else    {
            Storage::disk('templates')->makeDirectory($opt['alias']);
            Storage::disk('templates')->put($opt['alias'].DIRECTORY_SEPARATOR.'main.blade.php',$opt['source']);
            CacheController::deleteByTag('templates');
            return true;
        }
    }

    public static function editTemplate($opt){
        $v = Validator::make($opt, [
            'id'=>'required|numeric|min:0',
            'name' => 'required|min:1|max:255',
        ]);
        if ($v->fails()) return $v->errors();
        $resDB = Template::find($opt['id']);
        if ($resDB==null) return "Template not found";
        $resDB->name = $opt['name'];
        $resDB->description = $opt['description'];
        $resDB->developer = $opt['developer'];
        $opt['alias'] = $resDB->alias;
        $resDB = $resDB->save();
        if (!$resDB){
            return 'DB Error';
        }   else    {
            Storage::disk('templates')->makeDirectory($opt['alias']);
            Storage::disk('templates')->put($opt['alias'].DIRECTORY_SEPARATOR.'main.blade.php',$opt['source']);
            CacheController::deleteByTag('templates');
            return true;
        }
    }

    public static function getTemplateByID($id, $source = false){
        $template = Template::find($id);
        if ($template==null) return false;
        $resTemplate = [
            'id'=>$template->id,
            'name'=>$template->name,
            'alias'=>$template->alias,
            'developer'=>$template->developer,
            'description'=>$template->description,
        ];
        if ($source){
            $resTemplate['source'] = Storage::disk('templates')->get($template->alias.DIRECTORY_SEPARATOR.'main.blade.php');
        }
        return $resTemplate;
    }
}