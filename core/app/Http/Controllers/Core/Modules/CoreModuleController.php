<?php

namespace App\Http\Controllers\Core\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CacheController;
use Gate;

class CoreModuleController extends ModuleController{
    public function BuildTopMenu()
    {
        if (Gate::check('core_module')){
            $menu = [];
            $menu[] = [
                'text'=>'Статистика сайта',
                'href'=>'/admin/core/statistic/'
            ];
            $menu[] = [
                'text'=>'-'
            ];
            if (Gate::check('cache_manage')){
                $menu[] = [
                    'text'=>'Очистить кеш',
                    'href'=>'/admin/core/clear_cache'
                ];
                $menu[] = [
                    'text'=>(CacheController::useCache()?'Отключить':'Включить').' кеш',
                    'href'=>'/admin/core/toogle_cache'
                ];
            }
            if (Gate::check('access_system_file')){
                if ($menu!=[]) $menu[] = ['text'=>'-'];
                $menu[] = [
                    'text'=>'Системные файлы',
                    'href'=>'/admin/core/files'
                ];
            }
            if (Gate::check('backup_manage')){
                if ($menu!=[]) $menu[] = ['text'=>'-'];
                $menu[] = [
                    'text'=>'Резервные копии',
                    'href'=>'/admin/core/backups'
                ];
            }
            return $menu;
        }   else    {
            return [];
        }
        /*return [
            [
                'text'=>'Список',
                'href'=>'/admin/users/list'
            ],
            [
                'text'=>'Группы',
                'href'=>'/admin/users/groups/list'
            ],
            [
                'text'=>'Гейты',
                'href'=>'/admin/users/gates/list'
            ]
        ];*/
    }
}