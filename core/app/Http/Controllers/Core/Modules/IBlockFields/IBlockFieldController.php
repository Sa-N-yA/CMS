<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:47
 */
namespace App\Http\Controllers\Core\Modules\IBlockFields;

use App\Http\Controllers\Controller;

abstract class IBlockFieldController extends Controller{
    public static $fields = [
        "number" => NumberFieldController::class,
        "string" => StringFieldController::class,
        "file" => FileFieldController::class,
        "iblock_item" => IBlockItemFieldController::class,
        "iblock_section" => IBlockSectionFieldController::class,
        "visual" => VisualFieldController::class,
        "text" => TextFieldController::class,
        "select" => SelectFieldController::class,
    ];
    public abstract function getName();
    public abstract function getAlias();
    public function getFormView(){
        return 'core.admin.iblock.fields.'.$this->getAlias().'.form';
    }
    public function getFormCreateView(){
        return 'core.admin.iblock.fields.'.$this->getAlias().'.create';
    }
    public function getManyView(){
        return 'core.admin.iblock.fields.'.$this->getAlias().'.many';
    }
    public function getConfig(){
        return [];
    }
    public abstract function getCreateValidation();
    public abstract function getFormValidation($config);
}