<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\IBlockFields;


class SelectFieldController extends IBLockFieldController
{
    public function getCreateValidation()
    {
        return [];
    }
    public function getConfig(){
        return ['select_values'];
    }
    public function getName()
    {
        return "Список";
    }
    public function getAlias()
    {
        return "select";
    }
    public function getFormValidation($config)
    {
        $validation = ['in:'.$config['select_values']];
        return $validation;
    }
}