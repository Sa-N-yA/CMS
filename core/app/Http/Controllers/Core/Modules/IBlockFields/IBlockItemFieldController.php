<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\IBlockFields;

use App\IBlock;

class IBlockItemFieldController extends IBLockFieldController
{
    public static $types = [];

    public function __construct()
    {
        if (self::$types==[]){
            $iblocks = IBlock::all();
            foreach($iblocks as $iblock){
                self::$types[$iblock->id] = $iblock->name;
            }
        }
    }

    public function getCreateValidation()
    {
        return [
            'iblock_item_type'=>'required|in:'.implode(',',array_keys(self::$types))
        ];
    }
    public function getConfig(){
        return ['iblock_item_type'];
    }
    public function getName()
    {
        return "Привязка к элементу инфоблока";
    }
    public function getAlias()
    {
        return "iblock_item";
    }

    public function getFormValidation($config)
    {
        return ['numeric'];
    }
}