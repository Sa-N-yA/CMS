<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\IBlockFields;


class FileFieldController extends IBlockFieldController
{
    public static $types = [
        'image'=>'Изображение',
    ];

    public function getCreateValidation()
    {
        return [
            'file_type'=>'required|in:'.implode(',',array_keys(self::$types))
        ];
    }
    public function getConfig(){
        return ['file_type'];
    }
    public function getName()
    {
        return "Файл";
    }
    public function getAlias()
    {
        return "file";
    }
    public function getFormValidation($config)
    {
        return [];
    }
}