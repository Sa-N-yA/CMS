<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\IBlockFields;


class VisualFieldController extends IBLockFieldController
{
    public function getCreateValidation()
    {
        return [];
    }
    public function getConfig(){
        return [];
    }
    public function getName()
    {
        return "Виз. текст";
    }
    public function getAlias()
    {
        return "visual";
    }

    public function getFormValidation($config)
    {
        return [];
    }
}