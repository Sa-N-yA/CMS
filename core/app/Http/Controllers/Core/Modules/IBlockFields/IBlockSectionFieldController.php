<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\IBlockFields;

use App\IBlock;

class IBlockSectionFieldController extends IBLockFieldController
{
    public static $types = [];

    public function __construct()
    {
        if (self::$types==[]){
            $iblocks = IBlock::all();
            foreach($iblocks as $iblock){
                self::$types[$iblock->id] = $iblock->name;
            }
        }
    }

    public function getCreateValidation()
    {
        return [
            'iblock_section_type'=>'required|in:'.implode(',',array_keys(self::$types))
        ];
    }
    public function getConfig(){
        return ['iblock_section_type'];
    }
    public function getName()
    {
        return "Привязка к разделу инфоблока";
    }
    public function getAlias()
    {
        return "iblock_section";
    }

    public function getFormValidation($config)
    {
        return ['numeric'];
    }
}