<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 13:48
 */

namespace App\Http\Controllers\Core\Modules;
use App\File;
use App\Folder;
use App\Http\Controllers\Core\CacheController;
use Composer\Cache;
use Storage;
use Validator;
use Image;

class FilesModuleController extends ModuleController
{
    private static $cache_files = false;
    public function BuildTopMenu()
    {
        return [];
    }

    private static function TranslitURL($str)
    {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ё"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " -"=> "", ","=> "", " "=> "-", "."=> "", "/"=> "_",
            "-"=> ""
        );
        return preg_replace('/[^A-Za-z0-9_\-]/', '', strtr($str,$tr));
    }

    public static function GetPath($folder_id){
        $folder_name = md5($folder_id);
        $folder_name_1 = substr($folder_name, 0, 2);
        $folder_name_2 = substr($folder_name, 2, 2);
        return $folder_name_1.DIRECTORY_SEPARATOR.$folder_name_2.DIRECTORY_SEPARATOR.$folder_name;
    }

    private static function GetUniqueFileName($path){
        $info = pathinfo($path);
        $dir = $info['dirname'];
        $filename = self::TranslitURL($info['filename']);
        $ext = $info['extension'];
        $i = 0;
        while(true){
            $tmp_filename = $filename.(($i==0)?'':'_('.($i+1).')').'.'.$ext;
            if (!file_exists($dir.DIRECTORY_SEPARATOR.$tmp_filename)){
                return $tmp_filename;
            }
            $i++;
        }
    }

    public static function GetItems($folder_id){
        $res = [];
        $folders = Folder::where('parent_id',$folder_id)->get();
        foreach($folders as $folder){
            $res[] = [
                'id'=>$folder->id,
                'name'=>$folder->name,
                'parent_id'=>$folder->parent_id,
                'count_folders'=>$folder->subfolders->count(),
                'count_files'=>$folder->files->count(),
                'type'=>'folder'
            ];
        }
        $files = File::where('folder_id',$folder_id)->get();
        foreach($files as $file){
            $ext = pathinfo($file->filename)['extension'];
            if (in_array($ext,['jpg','png','gif'])){
                $src = self::GetResizeImagesFile($file, [48]);
            }   elseif(in_array($ext,['mp3']))    {
                $src = '/admin/common/img/file-music.png';
            }   else    {

            }
            $res[] = [
                'id' => $file->id,
                'name' => $file->name,
                'parent_id' => $file->folder_id,
                'src_orig' => '/upload/' . str_replace(DIRECTORY_SEPARATOR, '/', self::GetPath($file->folder_id)) . '/' . $file->filename,
                'src' => $src,
                'filesize' => $file->filesize,
                'type' => 'file'
            ];
        }
        return $res;
    }

    public static function CreateFolder($parent_id, $folder_name){
        $folder = new Folder();
        $folder->name = $folder_name;
        $folder->parent_id = $parent_id;
        $s = $folder->save();
        if ($s===true){
            Storage::disk('upload')->makeDirectory(self::GetPath($folder->id));
            return $folder;
        }   else    {
            return $s;
        }
    }

    public static function CreateFile($folder_id, $file){
        $v = Validator::make(['folder_id'=>$folder_id,'file'=>$file],[
            'folder_id'=>'required|numeric|min:0',
            'file'=>'required'
        ]);
        if ($v->fails()){
            return $v->errors();
        }
        $directory = 'upload'.DIRECTORY_SEPARATOR.self::GetPath($folder_id);
        $filename = self::GetUniqueFileName($directory.DIRECTORY_SEPARATOR.$file->getClientOriginalName());
        $file->move($directory,$filename);
        $fileModel = new File();
        $fileModel->name = $file->getClientOriginalName();
        $fileModel->filename = $filename;
        $fileModel->folder_id = $folder_id;
        $fileModel->filesize = filesize($directory.DIRECTORY_SEPARATOR.$filename);
        $s = $fileModel->save();
        return $s;
    }

    public static function GetResizeImagesFile(File $file, $size){
        if (count($size)==1) $size[1] = $size[0];
        $serSize = serialize($size);
        $arFile = unserialize($file->sizes);
        if (isset($arFile[$serSize])){
            $fileName = str_replace(DIRECTORY_SEPARATOR,'/',DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$arFile[$serSize]);
            if (file_exists(dirname(base_path()).DIRECTORY_SEPARATOR.$fileName)) return $fileName;
        }

        $finfo = pathinfo('upload'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$file->filename);
        Storage::disk('upload')->makeDirectory('cache'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id));
        Image::make('upload'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$finfo['basename'])
            ->fit($size[0],$size[1])
            ->save('upload'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$finfo['filename'].'_'.$size[0].'x'.$size[1].'.'.$finfo['extension']);
        $arFile[$serSize] = $finfo['filename'].'_'.$size[0].'x'.$size[1].'.'.$finfo['extension'];
        $file->sizes = serialize($arFile);
        $file->save();

        return str_replace(DIRECTORY_SEPARATOR,'/',DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$finfo['filename'].'_'.$size[0].'x'.$size[1].'.'.$finfo['extension']);
    }

    public static function GetFiles($ids){
        $result = unserialize(CacheController::get('files'));

        $res = [];
        $notCacheIDs = [];
        foreach($ids as $id){
            if (!isset($result[$id])){
                $notCacheIDs[] = $id;
            }   else    {
                $res[$id] = $result[$id];
            }
        }
        if ($notCacheIDs!=[]) {
            $files = File::whereIn('id', $notCacheIDs)->get();
            foreach($files as $file){
                $result[$file->id] = $file;
                $res[$file->id] = $file;
            }
        }

        CacheController::put('files',serialize($result),['files']);
        return $res;
    }

    public static function GetResizeImages($ids, $size){
        $result = unserialize(CacheController::get('files_'.md5(serialize([$ids,$size]))));
        if ($result!=null){
            return $result;
        }
        $files = self::GetFiles($ids);
        $res = [];
        foreach($files as $file){
            $res[$file->id] = ['resized'=>self::GetResizeImagesFile($file, $size),'original'=>'upload'.DIRECTORY_SEPARATOR.self::GetPath($file->folder_id).DIRECTORY_SEPARATOR.$file->filename];
        }
        CacheController::put('files_'.md5(serialize([$ids,$size])),serialize($res),['files']);
        return $res;
    }
}