<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\WebFormsFields;


class TextFieldController extends WebFormsFieldController
{
    public function getCreateValidation()
    {
        return [];
    }
    public function getConfig(){
        return [];
    }
    public function getName()
    {
        return "Текст";
    }
    public function getAlias()
    {
        return "text";
    }

    public function getFormValidation($config)
    {
        return [];
    }
}