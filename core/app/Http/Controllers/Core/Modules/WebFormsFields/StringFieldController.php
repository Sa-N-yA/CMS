<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\WebFormsFields;


class StringFieldController extends WebFormsFieldController
{
    public static $types = [
        'any'=>'Любая строка',
        'email'=>'E-mail адрес',
        'url'=>'URL адрес'
    ];

    public function getCreateValidation()
    {
        return [
            'string_min'=>'numeric|min:0',
            'string_max'=>'numeric|min:0',
            'string_type'=>'required|in:'.implode(',',array_keys(self::$types))
        ];
    }
    public function getConfig(){
        return ['string_min', 'string_max', 'string_type'];
    }
    public function getName()
    {
        return "Строка";
    }
    public function getAlias()
    {
        return "string";
    }

    public function getFormValidation($config)
    {
        $validation = [];
        if (!empty($config['string_min'])){
            $validation[] = 'min:'.$config['string_min'];
        }
        if (!empty($config['string_max'])){
            $validation[] = 'max:'.$config['string_max'];
        }
        if ($config['string_type']=='url'){
            $validation[] = 'url';
        }
        if ($config['string_type']=='email'){
            $validation[] = 'email';
        }
        return $validation;
    }
}