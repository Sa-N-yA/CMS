<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:53
 */

namespace App\Http\Controllers\Core\Modules\WebFormsFields;


class NumberFieldController extends WebFormsFieldController
{

    public function getCreateValidation()
    {
        return [
            'number_min'=>'numeric',
            'number_max'=>'numeric'
        ];
    }
    public function getConfig(){
        return ['number_min', 'number_max'];
    }
    public function getName()
    {
        return "Число";
    }
    public function getAlias()
    {
        return "number";
    }

    public function getFormValidation($config)
    {
        $validation = ['numeric'];
        if (!empty($config['number_min'])){
            $validation[] = 'min:'.$config['number_min'];
        }
        if (!empty($config['number_max'])){
            $validation[] = 'max:'.$config['number_max'];
        }
        return $validation;
    }
}