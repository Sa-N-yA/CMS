<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 13.03.2017
 * Time: 19:47
 */
namespace App\Http\Controllers\Core\Modules\WebFormsFields;

use App\Http\Controllers\Controller;

abstract class WebFormsFieldController extends Controller{
    public static $fields = [
        "number" => NumberFieldController::class,
        "string" => StringFieldController::class,
//        "file" => FileFieldController::class,
        "text" => TextFieldController::class,
        "select" => SelectFieldController::class,
    ];
    public abstract function getName();
    public abstract function getAlias();
    public function getFormCreateView(){
        return 'core.admin.webforms.fields.'.$this->getAlias().'.create';
    }
    public function getConfig(){
        return [];
    }
    public abstract function getCreateValidation();
    public abstract function getFormValidation($config);
}