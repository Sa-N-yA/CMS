<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 13:48
 */

namespace App\Http\Controllers\Core\Modules;
use App\Http\Controllers\Core\CacheController;
use App\Http\Controllers\Core\Modules\WebFormsFields\WebFormsFieldController;
use Storage;
use Validator;
use Image;
use App\WebForm;
use App\WebFormField;
use App\WebFormResult;
use App\WebFormResultAnswer;
use DB;
use Mail;
use Config;
use Gate;
use View;
use Auth;

class WebFormsModuleController extends ModuleController
{

    public static function getWebForms($count = false){
        $cache = CacheController::get('webforms_'.($count?'count':'not_count'));
        if ($cache!==null){
            return $cache;
        }
        if ($count) {
            $dbResult = WebForm::withCount(['fields','results'])->get();
        }   else    {
            $dbResult = WebForm::all();
        }
        $result = [];
        foreach($dbResult as $res){
            $result[] = $res->toArray();
        }
        CacheController::put('webforms_'.($count?'count':'not_count'),$result, ['webforms']);
        return $result;
    }

    public static function getWebFormByID($id){
        $cache = CacheController::get('webforms_'.$id);
        if ($cache!==null){
            return $cache;
        }
        $result = WebForm::where('id',$id)->withCount(['fields','results'])->first();
        if ($result!=null){
            $result = $result->toArray();
            CacheController::put('webforms_'.$id, $result, ['webforms']);
        }
        return $result;
    }

    public static function getFields($webform_id){
        $cache = CacheController::get('webforms_field_'.$webform_id);
        if ($cache!==null){
            return unserialize($cache);
        }   else    {
            $dbResult = WebFormField::where('webform_id',$webform_id)->get();
            $result = [];
            foreach($dbResult as $res){
                $result[] = [
                    'id'=>$res->id,
                    'name'=>$res->name,
                    'alias'=>$res->alias,
                    'type'=>$res->type,
                    'config'=>unserialize($res->config),
                    'created_at'=>$res->created_at,
                    'updated_at'=>$res->updated_at,
                ];
            }
            CacheController::put('webforms_field_'.$webform_id, serialize($result), ['webforms']);
            return $result;
        }
    }

    public static function getFieldByID($field_id){
        $field = WebFormField::find($field_id);
        if ($field==null) return false;
        return $field->toArray();
    }

    public static function addResult($webform_id, $fields){
        $webform = WebForm::with('fields')->where('id',$webform_id)->first();
        if ($webform==null) return false;

        $webform_result = new WebFormResult;
        $webform_result->webform_id = $webform_id;
        $webform_result->ip = $_SERVER['REMOTE_ADDR'];
        if (Auth::check()){
            $webform_result->user_id = Auth::user()->id;
        }   else    {
            $webform_result->user_id = 0;
        }
        foreach($webform->fields as $field){
            if (isset($fields[$field->alias])){
                $alias = 'field_'.$field->alias;
                $webform_result->$alias = $fields[$field->alias];
            }
        }
        $s = $webform_result->save();
        if ($s!==true){
            return $s;
        }
        if ($webform->email!=null && !empty($webform->email)){
            if (View::exists('email.webforms.'.$webform->alias)) {
                Mail::send('email.webforms.' . $webform->alias, array('webform' => $webform, 'fields' => $fields), function ($message) use ($webform) {
                    $message->to($webform->email)->subject('Заполнена веб-форма на сайте ' . config('site.name'));
                });
            }
        }
        CacheController::deleteByTag('webforms');
        return true;
    }

    public static function removeWebForm($id){
        WebForm::destroy($id);
        CacheController::deleteByTag('webforms');
    }

    public static function createWebForm($opt){
        if (isset($opt['name'], $opt['alias'], $opt['email'])){
            $webform = new WebForm;
            $webform->name = $opt['name'];
            $webform->alias = $opt['alias'];
            $webform->email = $opt['email'];
            CacheController::deleteByTag('webforms');
            return $webform->save();
        }
        return false;
    }

    public static function editWebForm($opt){
        if (isset($opt['id'],$opt['name'], $opt['alias'], $opt['email'])){
            $webform = WebForm::find($opt['id']);
            if ($webform==null) return false;
            $webform->name = $opt['name'];
            $webform->alias = $opt['alias'];
            $webform->email = $opt['email'];
            CacheController::deleteByTag('webforms');
            return $webform->save();
        }
        return false;
    }

    public static function createField($opt){
        if (isset($opt['webform_id'])){
            $prop = new WebFormField();
            $prop->webform_id = $opt['webform_id'];
            $prop->name = $opt['name'];
            $prop->alias = $opt['alias'];
            $prop->type = $opt['type'];
            $prop->required = $opt['required'];
            $fields = WebFormsFieldController::$fields;
            foreach($fields as $field){
                $field = new $field;
                foreach($field->getConfig() as $t){
                    $prop->$t = $opt[$t];
                }
            }
            CacheController::deleteByTag('webforms');
            return $prop->save();
        }
    }

    public static function editField($opt){
        if (isset($opt['field_id'])){
            $prop = WebFormField::find($opt['field_id']);
            if ($prop==null) return false;
            $prop->name = $opt['name'];
            $prop->required = $opt['required'];
            $fields = WebFormsFieldController::$fields;
            foreach($fields as $field){
                $field = new $field;
                foreach($field->getConfig() as $t){
                    $prop->$t = $opt[$t];
                }
            }
            CacheController::deleteByTag('webforms');
            return $prop->save();
        }
    }

    public static function removeField($id){
        WebFormField::destroy($id);
        CacheController::deleteByTag('webforms');
        return true;
    }

    public static function getResults($webform_id){
        $cache = CacheController::get('webform_result_'.$webform_id);
        if ($cache!==null){
            return $cache;
        }
        $result = WebFormResult::where('webform_id',$webform_id)->with('user')->get()->toArray();
        CacheController::put('webform_result_'.$webform_id, $result, ['webforms']);
        return $result;
    }

    public static function getResultByID($result_id){
        $cache = CacheController::get('webform_result_r'.$result_id);
        if ($cache!==null){
            return $cache;
        }
        $result = WebFormResult::with(['answers','answers.field','user'])->where('id',$result_id)->first();
        if ($result==null) return null;
        $result = $result->toArray();
        CacheController::put('webform_result_r'.$result_id, $result, ['webforms']);
        return $result;
    }
}