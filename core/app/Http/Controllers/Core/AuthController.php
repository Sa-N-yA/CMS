<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.12.2016
 * Time: 22:20
 */

namespace App\Http\Controllers\Core;


use \App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller{
    public static function authorise($email, $password, $remember = false){
        if (Auth::attempt(['email'=>$email, 'password'=>$password],$remember)){
            return true;
        }   else    {
            return false;
        }
    }
    public static function logout(){
        Auth::logout();
    }
}