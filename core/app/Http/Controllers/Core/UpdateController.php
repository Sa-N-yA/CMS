<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.01.2017
 * Time: 21:26
 */
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Config;
class UpdateController extends Controller{
    private static $serverAddress;
    private static $updateServerData;
    private static $error = "";

    public static function init(){
        self::$serverAddress = Config::get('cms.server');
    }
    public static function getError(){
        return self::$error;
    }
    public static function getCurrentVersion(){
        return Config::get('cms.version',0);
    }
    public static function getCurrentVersionServer(){
        if (self::$updateServerData!=null){
            return self::$updateServerData->version;
        }
        $curl = curl_init(self::$serverAddress."core-cms/version.json");
        if ($curl!==false){
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($curl);
            if ($res===false){
                self::$error = "Failed to connect to the update server";
                return false;
            }
            curl_close($curl);
            $res = json_decode($res);
            self::$updateServerData = $res;
            return $res->version;
        }   else    {
            self::$error = "Failed to connect to the update server";
            return false;
        }
    }

    public static function checkNewVersion(){
        $currentVersion = self::getCurrentVersion();
        $lastVersion = self::getCurrentVersionServer();
        if (intval($lastVersion)>intval($currentVersion)){
            return true;
        }
        return false;
    }

    public static function getCurrentVersionName()
    {
        return Config::get('cms.versionName',"0.0.0");
    }
    public static function getCurrentVersionNameServer()
    {
        if (self::$updateServerData!=null){
            return self::$updateServerData->versionName;
        }
        return false;
    }
}