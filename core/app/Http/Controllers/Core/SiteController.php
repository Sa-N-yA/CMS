<?php
namespace App\Http\Controllers\Core;
use Debug;
use App\Http\Controllers\Controller;
use App\Page;
use App\Template;
use Cache;
use Route;
use Gate;
use WebFormsModule;
use SiteModule;
use Config;
use Auth;

class SiteController extends Controller{
    private static $uriPrefix = 'uri_';
    private static $breadCrumbs = [];
    private static $template = "";
    private static $uri = [];
    private static $title = "HEAD";
    private static $meta = [];

    public static function getTemplate(){
        return self::$template;
    }

    public static function run($u){
        define('INSERT_LOAD_TIME','Y');
        if (Config::get('site.enabled') === false && !Auth::check()){
            return view('errors.disabled');
        }
        $cache = false;
        $meta = [];
        $title = "";
        $complex = false;
        self::$uri = explode('/',$u);
        if ($u=='/') self::$uri = [];

        $uri = CacheController::get(self::$uriPrefix.base64_encode($u));
        if ($uri) {
            $uri = unserialize($uri);
            $view = $uri['view'];
            $template = $uri['template'];
            self::$breadCrumbs = $uri['breadCrumbs'];
            $title = $uri['title'];
            $meta = $uri['meta'];

            $cache = true;
        }   else {
            if ($u == '/') {
                $page = Page::where('alias', 'general')->firstOrFail();
                $template = Template::where('id', $page->template)->firstOrFail()->alias;
                $view = $page->view;
                $title = $page->title;
                $meta = [
                    'description'=>$page->description,
                    'keywords'=>$page->keywords
                ];
            } else {
                $pages = SiteModule::getPages();
                foreach($pages as $page){
                    if ($page['parent_id']==0 && $page['alias'] == 'general'){
                        self::pushBreadCrumb($page['name'],'/');
                    }
                }
                $arUri = self::$uri;
                $parent = 0;
                $view = null;
                $path = '';
                foreach($arUri as $a){
                    $found = false;
                    foreach($pages as $page){
                        if ($page->alias==$a && $page->parent_id==$parent){
                            $view = $page->view;
                            $template = $page->template;
                            $parent = $page->id;
                            $path.= '/'.$page->alias;
                            self::pushBreadCrumb($page->name,$path);
                            $found = true;
                            $title = $page->title;
                            $meta = [
                                'description'=>$page->description,
                                'keywords'=>$page->keywords
                            ];
                            $complex = $page->complex=='Y';
                            break;
                        }
                    }
                    if (!$found){
                        if (!$complex) abort(404);
                        break;
                    }
                    $template = Template::where('id',$template)->firstOrFail()->alias;
                }
            }
        }
        if ($view==null) abort(404);
        if (!view()->exists('templates.'.$template.'.main')) abort(404);
        if (!view()->exists('site.'.$view)) abort(404);
        if (!$cache){
            CacheController::put(self::$uriPrefix.base64_encode($u), serialize(['view'=>$view,'title'=>$title,'meta'=>$meta, 'template'=>$template, 'breadCrumbs'=>self::$breadCrumbs]),['uri']);
        }
        self::$template = $template;
        self::$meta = $meta;
        self::$title = $title;
        return view('core/site',['template'=>$template, 'view'=>$view]);
    }

    public static function pushBreadCrumb($title, $link = ""){
        self::$breadCrumbs[] = ['title'=>$title, 'link'=>$link];
    }

    public static function getBreadCrumbs(){
        $breadcrumbs = self::$breadCrumbs;
        $breadcrumbs[count($breadcrumbs)-1]['link'] = "";
        return self::$breadCrumbs;
    }

    public static function getUri(){
        return self::$uri;
    }

    public static function showHead(){
        $head = "";
        if (empty(self::$title)){
            $head.="<title>".Config::get('site.name')."</title>";
        }   else    {
            $head.="<title>".self::$title." - ".Config::get('site.name')."</title>";
        }
        foreach(self::$meta as $key=>$val){
            $head.='<meta name="'.$key.'" content="'.$val.'"/>';
        }
        return $head;
    }

    public static function setTitle($title){
        self::$title = $title;
    }

    public static function setMeta($meta, $value){
        self::$meta[$meta] = $value;
    }
}