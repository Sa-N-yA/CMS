<?php

namespace App\Http\Controllers\Components;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\SiteController;
use App\Http\Requests;
use App\Http\Controllers\Core\CacheController;

class ComponentController extends Controller{

    public static function IncludeComponent($params){
        $startTime = microtime(true);
        if (isset($params['name'])){
            $componentNameParts = explode(':', $params['name']);
            if (count($componentNameParts)!=2) return "";

            $componentController = "App\\Http\\Controllers\\Components\\{$componentNameParts[0]}\\{$componentNameParts[1]}Controller";
            if (!class_exists($componentController)) return "";
            $component = new $componentController;

            if (isset($params['params']['paginate']) && $params['params']['paginate']>0){
                $params['page'] = (isset($_GET['page'])?$_GET['page']:1);
            }
            if (!isset($params['cache'])) $params['cache'] = true;

            if ($params['cache'] && $component->cache){
                $cache = CacheController::get(serialize($params));
                if ($cache!=null){
                    return $cache;
                }
            }


            $componentResult = $component->run($params['params']);

            if (isset($params['modificators'])){
                foreach($params['modificators'] as $modificator){
                    $modificatorController = "App\\Site\\Modificators\\".$modificator."ModificatorController";
                    if (!class_exists($modificatorController)){
                        $modificatorController = "App\\Http\\Controllers\\Modificators\\".$modificator."ModificatorController";
                        if (!class_exists($modificatorController)) continue;
                    }
                    $modificatorClass = new $modificatorController;
                    if (isset($params['params'][$modificator.'Modificator'])){
                        $componentResult = $modificatorClass->run($params['params'][$modificator.'Modificator'],$componentResult);
                    }   else    {
                        $componentResult = $modificatorClass->run([],$componentResult);
                    }
                }
            }

            $result = ['params'=>$params['params'], 'result' =>$componentResult];
            if (!isset($params['template'])){
                return $result;
            }   else    {
                return static::GetView($params, $result, ['startTime'=>$startTime]);
            }
        }
    }

    private static function GetView($params, $result, $debugParams){
        $componentNameParts = explode(':', $params['name']);
        $componentNameParts[1] = snake_case($componentNameParts[1]);
        if ($componentNameParts[1]!='main'){

            $viewName1 = $componentNameParts[0].'.'.$componentNameParts[1];
            if (view()->exists('templates.'.SiteController::getTemplate().'.components.'.$params['template'].'.'.$viewName1)){
                $viewName = 'templates.'.SiteController::getTemplate().'.components.'.$params['template'].'.'.$viewName1;
            }   else    {
                $viewName = 'components.'.$viewName1;
            }
        }   else    {
            if (view()->exists('site.'.$result['result']['view'])){
                $viewName = 'site.'.$result['result']['view'];
            }   else    {
                abort(404);
            }
        }
        $rendered = view($viewName, $result)->render();
        $tags = [];
        if (isset($params['params']['iblock_id'])){
            $tags[] = 'iblock_'.$params['params']['iblock_id'];
        }
        CacheController::put(serialize($params), $rendered, $tags);

        return $rendered;
    }

    protected final function applyDefault(&$params, $default){
        foreach($default as $key=>$val){
            if (!isset($params[$key])) $params[$key] = $val;
        }
        return $params;
    }
}
