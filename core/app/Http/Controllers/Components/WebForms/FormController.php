<?php

namespace App\Http\Controllers\Components\WebForms;

use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\Modules\WebFormsModuleController;
use Route;
use App\Http\Requests;
use Request;

class FormController extends ComponentController
{
    protected static $default = [
    ];

    public $cache = false;

    public function run($params){
        $values = [];
        $fields = [];
        $result = '';

        if ($params['webform_id']){
            $this->applyDefault($params, self::$default);
            $fields = WebFormsModuleController::getFields($params['webform_id']);
            if (!isset($params['handle']) || $params['handle']===true){
                foreach($fields as $field){
                    $values[$field['alias']] = Request::input($field['alias']);
                }

                if (Request::input('webform_id')==$params['webform_id']){
                    $result = WebFormsModuleController::addResult($params['webform_id'], $values);
                    if ($result===true){
                        $result = 'success';
                        $values = [];
                    }
                }
            }
        }

        foreach($fields as $field){
            if (!isset($values[$field['alias']]))
                $values[$field['alias']] = "";
        }
        return [
            'result'=>$result,
            'values'=>$values,
            'fields'=>$fields
        ];
    }
}
