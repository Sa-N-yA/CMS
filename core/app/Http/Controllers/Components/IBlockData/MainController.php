<?php

namespace App\Http\Controllers\Components\IBlockData;

use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\CacheController;
use App\Http\Controllers\Core\SiteController;
use App;

class MainController extends ComponentController
{
    private static $replacement = [
        '#SECTION_ID#'=>'([0-9]+)',
        '#SECTION_ALIAS#'=>'([a-zA-Z][a-zA-Z\\-_0-9]*)',
        '#ITEM_ID#'=>'([0-9]+)',
        '/'=>'\\/',
        '#ITEM_ALIAS#'=>'([a-zA-Z][a-zA-Z\\-_0-9]*)',
    ];

    public $cache = true;

    public function run($params){
        $res = [];
        if (isset($params['iblock_id'])){
            $uri = implode('/',SiteController::getUri())."/";
            $sections = $items = $item = false;
            if (isset($params['sections_uri'])){
                $sections = self::checkMask($uri, $params['base_uri'], $params['sections_uri']);
            }
            if (isset($params['items_uri'])){
                $items = self::checkMask($uri, $params['base_uri'], $params['items_uri']);
            }
            if (isset($params['item_uri'])){
                $item = self::checkMask($uri, $params['base_uri'], $params['item_uri']);
            }
            if ($sections) {
                $res['view'] = $params['sections_view'];
                $res['params'] = [
                    'iblock_id'=>$params['iblock_id']
                ];
            }   else if($items) {
                $id = (isset($items['SECTION_ID']))?$items['SECTION_ID']:'';
                $alias = (isset($items['SECTION_ALIAS']))?$items['SECTION_ALIAS']:'';

                if ($id){
                    $section = App\Http\Controllers\Core\Modules\IBlockModuleController::GetSectionById($params['iblock_id'],$id, $params['items_section_fields']);
                }   else    {
                    $section = App\Http\Controllers\Core\Modules\IBlockModuleController::GetSectionByAlias($params['iblock_id'],$alias, $params['items_section_fields']);
                }
                if ($section===false){
                    abort(404);
                }
                $sectionPath = App\Http\Controllers\Core\Modules\IBlockModuleController::GetPathSection($params['iblock_id'], $section['id'],['name']);
                foreach($sectionPath as $s){
                    SiteController::pushBreadCrumb($s,'/alias/');
                }
                $res['view'] = $params['items_view'];
                $res['params'] = [
                    'iblock_id'=>$params['iblock_id'],
                    'section'=>$section
                ];
            }   else if($item)  {
                $res['view'] = $params['item_view'];
                $res['iblock_id'] = $params['iblock_id'];
                $res['item'] = [
                    'id'=>(isset($item['ITEM_ID']))?$item['ITEM_ID']:'',
                    'alias'=>(isset($item['ITEM_ALIAS']))?$item['ITEM_ALIAS']:''
                ];
            }   else    {
                abort(404);
            }
        }   else    {
            return "";
        }
        return $res;
    }

    private static function handleMask($mask){
        foreach (self::$replacement as $key=>$value){
            $mask = str_replace($key, $value, $mask);
        }
        return $mask;
    }

    private static function checkMask($uri, $prefix, $mask){
        $maskRep = self::handleMask($prefix.$mask);
        $r = [
            'ITEM_ID'=>'',
            'ITEM_ALIAS'=>'',
            'SECTION_ID'=>'',
            'SECTION_ALIAS'=>'',
        ];
        $res = preg_match_all("/^$maskRep$/", $uri, $match);

        if ($res>0){
            $pos = [
                'ITEM_ID'=>strpos($mask,'#ITEM_ID#'),
                'ITEM_ALIAS'=>strpos($mask,'#ITEM_ALIAS#'),
                'SECTION_ID'=>strpos($mask,'#SECTION_ID#'),
                'SECTION_ALIAS'=>strpos($mask,'#SECTION_ALIAS#'),
            ];
            foreach($pos as $k=>$v){
                if ($v===false) unset($pos[$k]);
            }
            asort($pos);
            $i = 1;
            foreach($pos as $k=>$v){
                $r[$k] = $match[$i++][0];
            }
            return $r;
        }   else    {
            return false;
        }
    }
}
