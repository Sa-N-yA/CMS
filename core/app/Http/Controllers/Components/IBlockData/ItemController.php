<?php

namespace App\Http\Controllers\Components\IBlockData;

use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\SiteController;
use App\IBlockItem;
use App\IBlockSection;
use Route;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use IBlockModule;

class ItemController extends ComponentController
{
    public $cache = true;

    public function run($params){
        $res = [];
        if ($params['iblock_id']){
            if (!isset($params['fields'])) $params['fields'] = [];
            $res['item'] = false;
            if (isset($params['item_id'])){
                $res['item'] = IBlockModule::GetItemByID($params['iblock_id'], $params['item_id'],$params['fields']);
            }   else if(isset($params['item_alias'])){
                $res['item'] = IBlockModule::GetItemByAlias($params['iblock_id'], $params['item_alias'],$params['fields']);
            }
            if ($res['item']!==false){

            }   else    {
                abort(404);
            }
        }
        return $res;
    }
}
