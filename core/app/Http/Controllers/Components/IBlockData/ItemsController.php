<?php

namespace App\Http\Controllers\Components\IBlockData;

use App\Http\Controllers\Components\ComponentController;
use App\Http\Controllers\Core\SiteController;
use App\IBlockItem;
use App\IBlockSection;
use Route;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use IBlockModule;

class ItemsController extends ComponentController
{
    protected $default = [
        'fields'=>[],
        'filter'=>[],
        'paginate'=>[],
        'order'=>'sort'
    ];

    public $cache = true;

    public function run($params){
        $res = [];
        if ($params['iblock_id']){
            $this->applyDefault($params, $this->default);
            $res = IBlockModule::GetItems($params['iblock_id'],$params['fields'],$params['filter'],[],$params['order']);
        }
        return $res;
    }
}
