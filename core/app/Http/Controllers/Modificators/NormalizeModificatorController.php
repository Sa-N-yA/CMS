<?php

namespace App\Http\Controllers\Modificators;



class NormalizeModificatorController extends ModificatorController
{
    public function run($params, $result)
    {
        $res = [];
        foreach($result as $key=>$val){
            if (!in_array($key,$params)){
                $res[$key] = $val;
            }   else    {
                foreach($result[$key] as $item){
                    $res[$key][] = $item;
                }
            }
        }
        return $res;
    }

    private function getFileIDs($arPath, $result){
        if ($arPath == null) return $result;
        $path = array_last($arPath);
        array_pop($arPath);
        if ($path == '*'){
            $res = [];
            foreach($result as $r){
                $res[] = $this->getFileIDs($arPath,$r);
            }
            $result = $res;
        }   else    {
            if (isset($result[$path])) {
                $result = $this->getFileIDs($arPath, $result[$path]);
            }
        }
        return $result;
    }
}
