<?php

namespace App\Http\Controllers\Modificators;


use App\Http\Controllers\Core\Modules\FilesModuleController;

class FilesModificatorController extends ModificatorController
{
    public function run($params, $result)
    {
        $files = [];
        foreach($params as $value){
            $fileIDs = array_unique(array_flatten($this->getFileIDs(array_reverse(explode('.', $value)), $result)));

            $filesDB = FilesModuleController::GetFiles($fileIDs);
            foreach($filesDB as $file){
                $files[$file->id] = [
                    'id'=>$file->id,
                    'name'=>$file->name,
                    'url'=>'/upload/'.FilesModuleController::GetPath($file->folder_id).'/'.$file->filename
                ];
            }
        }
        $result['files'] = $files;
        return $result;
    }

    private function getFileIDs($arPath, $result){
        if ($arPath == null) return $result;
        $path = array_last($arPath);
        array_pop($arPath);
        if ($path == '*'){
            $res = [];
            foreach($result as $r){
                $res[] = $this->getFileIDs($arPath,$r);
            }
            $result = $res;
        }   else    {
            if (isset($result[$path])) {
                $result = $this->getFileIDs($arPath, $result[$path]);
            }
        }
        return $result;
    }
}
