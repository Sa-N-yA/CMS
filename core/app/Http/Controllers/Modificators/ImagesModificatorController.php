<?php

namespace App\Http\Controllers\Modificators;


use App\Http\Controllers\Core\Modules\FilesModuleController;

class ImagesModificatorController extends ModificatorController
{
    public function run($params, $result)
    {
        $files = [];
        foreach($params as $key=>$value){
            foreach($value as $v) {
                $fileIDs = array_unique(array_flatten($this->getFileIDs(array_reverse(explode('.', $key)), $result)));

                $resizeFiles = FilesModuleController::GetResizeImages($fileIDs, [$v['width'], $v['height']]);
                foreach ($fileIDs as $id) {
                    if (isset($resizeFiles[$id])){
                        if (!isset($files[$id])) {
                            $files[$id] = ['original' => str_replace(DIRECTORY_SEPARATOR,'/','/'.$resizeFiles[$id]['original'])];
                        }
                        $files[$id][$v['name']] = $resizeFiles[$id]['resized'];
                    }
                }
            }
        }
        $result['files'] = $files;
        return $result;
    }

    private function getFileIDs($arPath, $result){
        if ($arPath == null) return $result;
        $path = array_last($arPath);
        array_pop($arPath);
        if ($path == '*'){
            $res = [];
            foreach($result as $r){
                $res[] = $this->getFileIDs($arPath,$r);
            }
            $result = $res;
        }   else    {
            if (isset($result[$path])) {
                $result = $this->getFileIDs($arPath, $result[$path]);
            }
        }
        return $result;
    }
}
