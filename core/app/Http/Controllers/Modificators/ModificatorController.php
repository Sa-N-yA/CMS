<?php

namespace App\Http\Controllers\Modificators;

use App\Http\Controllers\Controller;

abstract class ModificatorController extends Controller
{
    abstract public function run($params, $result);
}
