<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 12:49
 */

namespace App\Http\Controllers\Ajax;
use Request;
use Storage;
use FilesModule;
use Gate;
use Cache;
use Queue;
use Config;

class BackupAjaxController extends AjaxController
{
    public function run($opt){
        if (!Gate::check('backup_manage')) $this->_403();
        if (Cache::has('backup')) {
            $cache = unserialize(Cache::get('backup'));
            if ($cache['status']!='success') {
                $this->res = "A backup is already being created";
                $this->error = true;
                return $this->finish();
            }
        }
        Cache::forget('backup');
        Queue::push(new \App\Jobs\BackupJob(['/core'],false));
        exec('php '.$_SERVER['DOCUMENT_ROOT'].'/core/artisan queue:work > /dev/null &');
        $this->res = "Success";
        return $this->finish();
    }

    public function status($opt){
        if (!Gate::check('backup_manage')) $this->_403();
        if (Cache::has('backup')){
            $this->data = unserialize(Cache::get('backup'));
        }
        return $this->finish();
    }
}