<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 12:49
 */

namespace App\Http\Controllers\Ajax;
use Request;
use Storage;
use Gate;
use SiteModule;
use FilesModule;

class SiteAjaxController extends AjaxController
{
    public function template_remove($opt){
        if (!Gate::check("site_templates")){
            return $this->_403();
        }
        if (!isset($opt['alias'])){
            return $this->_404();
        }
        $result = SiteModule::removeTemplateByAlias($opt['alias'],true);
        $this->data = $result;
        return $this->finish();
    }

    public function template_create($opt){
        if (!Gate::check("site_templates")){
            return $this->_403();
        }
        $result = SiteModule::createTemplate($opt);
        if ($result===true){
            $this->data = "Success";
        }   else    {
            $this->error = true;
            $this->data = $result;
        }
        return $this->finish();
    }

    public function template_edit($opt){
        if (!Gate::check("site_templates")){
            return $this->_403();
        }
        $result = SiteModule::editTemplate($opt);
        if ($result===true){
            $this->data = "Success";
        }   else    {
            $this->error = true;
            $this->data = $result;
        }
        return $this->finish();
    }

    public function page_remove($opt){
        if (!Gate::check("site_pages_show")){
            return $this->_403();
        }
        if (!isset($opt['id'])){
            return $this->_404();
        }
        $result = SiteModule::removePageByID($opt['id'],true);
        $this->data = $result;
        return $this->finish();
    }

    public function page_create($opt){
        if (!Gate::check("site_pages_show")){
            return $this->_403();
        }
        $result = SiteModule::createPage($opt);
        if ($result===true){
            $this->data = "Success";
        }   else    {
            $this->error = true;
            $this->data = $result;
        }
        return $this->finish();
    }

    public function page_edit($opt){
        if (!Gate::check("site_pages_show")){
            return $this->_403();
        }
        $result = SiteModule::editPage($opt);
        if ($result===true){
            $this->data = "Success";
        }   else    {
            $this->error = true;
            $this->data = $result;
        }
        return $this->finish();
    }
}