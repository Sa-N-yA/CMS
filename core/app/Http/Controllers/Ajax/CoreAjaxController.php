<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.01.2017
 * Time: 12:49
 */

namespace App\Http\Controllers\Ajax;
use Request;
use Storage;
use FilesModule;
use Gate;
use File;
use Queue;
use Config;

class CoreAjaxController extends AjaxController
{
    public function get_files($opt){
        if (!Gate::check('access_system_file')) $this->_403();
        if (isset($opt['path'])){
            $path = $_SERVER['DOCUMENT_ROOT'].$opt['path'];
            $files = scandir($path);
            $this->data = [];
            foreach($files as $file){
                if ($file!=='.' && $file!='..'){
                    $this->data[] = [
                        'name'=>$file,
                        'type'=>is_dir($path.DIRECTORY_SEPARATOR.$file)?'dir':'file'
                    ];
                }
            }
            usort($this->data, function($e1, $e2){
                if ($e1['type']==$e2['type']){
                    return $e1['name']<$e2['name']?-1:1;
                }   else    {
                    return $e1['type']=='dir'?-1:1;
                }
            });
            $this->res = "Success";
            return $this->finish();
        }   else    {
            return $this->_404();
        }
    }

    public function get_file_data($opt){
        if (!Gate::check('access_system_file')) $this->_403();
        if (isset($opt['path'])){
            $path = $_SERVER['DOCUMENT_ROOT'].$opt['path'];
            if (file_exists($path) && is_file($path)){
                $data = file_get_contents($path);
                $file_ext = pathinfo($path);
                $this->data = [
                    'data'=>$data,
                    'name'=>@$file_ext['basename'],
                    'extension'=>@$file_ext['extension']
                ];
                return $this->finish();
            }   else    {
                return $this->_404();
            }
        }   else    {
            return $this->_404();
        }
    }
}