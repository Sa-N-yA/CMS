<?php
namespace App\Http\Controllers\Ajax;

use App\Http\Controllers;
use App\Http\Requests;
use WebFormsModule;
use Config;
use Gate;

class WebFormsAjaxController extends AjaxController
{
    public function add_result($opt){
        if (isset($opt['webform_id'])){
            $res = WebFormsModule::addResult($opt['webform_id'],$opt);
            if ($res===true){
                $this->res = "success";
            }   else    {
                $this->error = true;
                $this->res = "error";
                $this->data = $res;
            }
        }   else    {
            return $this->_404();
        }
        return $this->finish();
    }

    public function remove_webform($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        if (isset($opt['id'])){
            WebFormsModule::removeWebForm($opt['id']);
            $this->res = 'success';
            return $this->finish();
        }   else    {
            return $this->_404();
        }
    }

    public function create_webform($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        $this->data = WebFormsModule::createWebForm($opt);
        $this->res = 'success';
        if ($this->data!==true){
            $this->res = 'Ошибка создания веб-формы';
            $this->error = true;
        }
        return $this->finish();
    }

    public function edit_webform($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        $this->data = WebFormsModule::editWebForm($opt);
        $this->res = 'success';
        if ($this->data!==true){
            $this->res = 'Ошибка создания веб-формы';
            $this->error = true;
        }
        return $this->finish();
    }

    public function create_field($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        $this->data = WebFormsModule::createField($opt);
        $this->res = 'success';
        if ($this->data!==true){
            $this->res = 'Ошибка создания поля';
            $this->error = true;
        }
        return $this->finish();
    }

    public function edit_field($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        $this->data = WebFormsModule::editField($opt);
        $this->res = 'success';
        if ($this->data!==true){
            $this->res = 'Ошибка создания поля';
            $this->error = true;
        }
        return $this->finish();
    }

    public function remove_field($opt){
        if (!Gate::check('webforms_module')) return $this->_403();
        if (!isset($opt['id'])) return $this->_404();
        if (WebFormsModule::removeField($opt['id'])){
            $this->res = 'success';
        }   else    {
            $this->res = 'unknown error';
            $this->error = true;
        }
        return $this->finish();
    }
}