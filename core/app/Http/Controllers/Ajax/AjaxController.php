<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AjaxController extends \App\Http\Controllers\Controller
{
    protected $data = [];
    protected $res = "";
    protected $error = false;

    final protected function getUserID(){
        if (Auth::check()){
            return Auth::user()->id;
        }
        return -1;
    }

    final protected function start($section, $method){
        $sectionClass = "App\\Site\\Ajax\\{$section}AjaxController";
        if (!class_exists($sectionClass)){
            $sectionClass = "App\\Http\\Controllers\\Ajax\\{$section}AjaxController";
            if (!class_exists($sectionClass)) {
                return $this->_404();
            }
        }
        $a = new $sectionClass();
        if (!method_exists($a, $method)) return $this->_404();
        unset($_POST['_token']);
        return $a->$method($_POST);
    }

    final protected function finish(){
        return json_encode([
            'DATA'=>$this->data,
            'RESULT'=>$this->res,
            'ERROR'=>$this->error
        ]);
    }

    final protected function _404(){
        return  json_encode([
            'DATA'=>[],
            'RESULT'=>'404 Not found',
            'ERROR'=>true
        ]);
    }

    final protected function _403(){
        return  json_encode([
            'DATA'=>[],
            'RESULT'=>'403 Access denied',
            'ERROR'=>true
        ]);
    }
    final protected function _200(){
        return json_encode([
            'DATA'=>[],
            'RESULT'=>'Success',
            'ERROR'=>false
        ]);
    }
}
