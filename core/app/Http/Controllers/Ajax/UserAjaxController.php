<?php

namespace App\Http\Controllers\Ajax;

use App\Group;
use App\Http\Controllers;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Core\AuthController;
use Validator;
use Gate;

class UserAjaxController extends AjaxController
{
    final protected function auth($opt){
        if (isset($opt['email'],$opt['password'],$opt['remember'])){
            if (AuthController::authorise($opt['email'], $opt['password'], $opt['remember'])){
                $this->res = 'Успешная авторизация';
            }   else    {
                $this->data = [
                    'email'=>'Неверный логин или пароль'
                ];
                $this->error = true;
            }
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
        return $this->finish();
    }

    final protected function create_user($opt){
        if (!Gate::check('users_module')) return $this->_403();
        if (isset($opt['name'],$opt['email'],$opt['password'],$opt['password_confirmation'])){
            $user = new User();
            $user->name = $opt['name'];
            $user->email = $opt['email'];
            $user->password = $opt['password'];
            $user->password_confirmation = $opt['password_confirmation'];
            foreach($opt as $k=>$v){
                if (strpos($k,'group_')!==false){
                    $user->$k = $v;
                }
            }
            $s = $user->save();
            if ($s===true){
                $this->res = 'Пользователь успешно создан';
            }   else    {
                $this->data = $s;
                $this->error = true;
            }
            return $this->finish();
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
    }

    final protected function create_group($opt){
        if (!Gate::check('users_module')) return $this->_403();
        if (isset($opt['name'])){
            $user = new Group();
            $user->name = $opt['name'];
            foreach($opt as $k=>$v){
                if (strpos($k,'gate_')!==false){
                    $user->$k = $v;
                }
            }
            $s = $user->save();
            if ($s===true){
                $this->res = 'Группа успешно создана';
            }   else    {
                $this->data = $s;
                $this->error = true;
            }
            return $this->finish();
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
    }

    final protected function update_user($opt){
        if (!Gate::check('users_module')) return $this->_403();
        if (isset($opt['id'],$opt['name'],$opt['email'],$opt['password'],$opt['password_confirmation'])){
            $user = User::find($opt['id']);
            $user->name = $opt['name'];
            $user->email = $opt['email'];
            $user->change_password = false;
            if (!empty($opt['password']) || !empty($opt['password_confirmation'])){
                $user->password = $opt['password'];
                $user->password_confirmation = $opt['password_confirmation'];
                $user->change_password = true;
            }
            foreach($opt as $k=>$v){
                if (strpos($k,'group_')!==false){
                    $user->$k = $v;
                }
            }
            $s = $user->save();
            if ($s===true){
                $this->res = 'Пользователь успешно изменен';
            }   else    {
                $this->data = $s;
                $this->error = true;
            }
            return $this->finish();
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
    }
    final protected function update_group($opt){
        if (!Gate::check('users_module')) return $this->_403();
        if (isset($opt['id'],$opt['name'])){
            $user = Group::find($opt['id']);
            $user->name = $opt['name'];
            foreach($opt as $k=>$v){
                if (strpos($k,'gate_')!==false){
                    $user->$k = $v;
                }
            }
            $s = $user->save();
            if ($s===true){
                $this->res = 'Группа успешно изменена';
            }   else    {
                $this->data = $s;
                $this->error = true;
            }
            return $this->finish();
        }   else    {
            $this->res = "Неверный формат данных";
            $this->error = true;
        }
    }
}
