<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/ajax/{section}/{method}', 'Ajax\AjaxController@start');
Route::post('/ajax/{section}/{method}', 'Ajax\AjaxController@start');

Route::get('admin',function(){
    return redirect('admin/main');
});
Route::group(['prefix'=>'admin'],function(){
    Route::get('{any}', 'Core\\Admin\\MainController@main');
});

Route::get('{any}', "Core\\SiteController@run");
Route::post('{any}', "Core\\SiteController@run");