<?php

namespace App\Providers;

use App\Http\Controllers\Core\CacheController;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Gate as GateModel;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    private static $user_groups = false;

    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gates = CacheController::get('gates');
        if ($gates===null){
            $gates = GateModel::with('groups')->get();
            $tmpGates = [];
            foreach($gates as $g){
                $tmpGates[] = [
                    'id'=>$g->id,
                    'alias'=>$g->alias
                ];
            }
            $gates = $tmpGates;
            CacheController::put('gates',serialize($gates),['rights']);
        }   else    {
            $gates = unserialize($gates);
        }

        foreach($gates as $g){
            $gate->define($g['alias'], function($user) use($g){
                if (self::$user_groups===false){
                    self::$user_groups = $user->groups;
                }
                foreach(self::$user_groups as $group){
                    if ($group->id=='1'){
                        return true;
                    }
                    foreach($g->groups as $ggroup) {
                        if($group->id==$ggroup->id) return true;
                    }
                }
                return false;
            });
        }
    }
}
