<?php

namespace App\Providers;

use App\IBlockSectionProp;
use Blade;
use App\Events\IBlockChanged;
use Event;
use App\GroupOlympic;
use App\Olympic;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use App\IBlockItem;
use App\IBlockSection;
use DB;
use App\Http\Controllers\Core\Agents\AgentController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('component', function($params) {
            return "<?php echo \\App\\Http\\Controllers\\Components\\ComponentController::IncludeComponent($params);?>";
        });
        Blade::directive('head',function($params){
            return "<?php echo \\App\\Http\\Controllers\\Core\\SiteController::ShowHead();?>";
        });
        Blade::directive('meta_description',function($params){
            return "<?php \\App\\Http\\Controllers\\Core\\SiteController::SetMeta('description',$params);?>";
        });
        Blade::directive('meta_keywords',function($params){
            return "<?php \\App\\Http\\Controllers\\Core\\SiteController::SetMeta('keywords',$params);?>";
        });
        Blade::directive('title',function($params){
            return "<?php \\App\\Http\\Controllers\\Core\\SiteController::SetTitle($params);?>";
        });
        IBlockItem::saved(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockSection::saved(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockItem::deleting(function($item){
            Event::fire(new IBlockChanged($item));
        });
        IBlockSection::deleting(function($item){
            Event::fire(new IBlockChanged($item));
        });
        AgentController::exec();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
