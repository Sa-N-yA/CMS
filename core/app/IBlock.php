<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class IBlock extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'iblock';

    public function items(){
        return $this->hasMany('App\\IBlockItem', 'iblock_id');
    }

    public function sections(){
        return $this->hasMany('App\\IBlockSection', 'iblock_id');
    }

    public function itemProps(){
        return $this->hasMany('App\\IBlockItemProp', 'iblock_id');
    }

    public function sectionProps(){
        return $this->hasMany('App\\IBlockSectionProp', 'iblock_id');
    }

    public function save(array $options = Array()){
        $v = Validator::make($this->attributes, [
            'name' => 'required|min:3|max:255',
            'alias' => 'required|min:3|max:255|unique:iblock,alias'.(($this->id?','.$this->id:'')).'|regex:([a-zA-Z][a-zA-Z0-9_]*)'
        ]);
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}
