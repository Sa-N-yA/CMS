<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Cache;
use Config;
use Symfony\Component\Console\Input\InputOption;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:backup {--nodb} {--ignore=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make site\'s backup';

    private $pathIgnore = [
        '/core'
    ];
    private $path;
    private $dbBackup;

    public function __construct()
    {
        $this->path = dirname(base_path());
        parent::__construct();
    }

    private function getFiles($path){
        if (in_array($path,$this->pathIgnore)) return [];
        $dirs = scandir($this->path.DIRECTORY_SEPARATOR.$path);
        $files = [];
        foreach($dirs as $dir){
            if ($dir=='.' || $dir=='..') continue;
            $ePath = $path.DIRECTORY_SEPARATOR.$dir;
            if (is_dir($this->path.$ePath)){
                $files = array_merge($files, $this->getFiles($ePath));
            }   else    {
                $files[] = $ePath;
            }
        }
        return $files;
    }

    public function handle()
    {
        $this->pathIgnore = $this->option('ignore');
        foreach($this->pathIgnore as &$ignore){
            $ignore = str_replace("\\",DIRECTORY_SEPARATOR,str_replace("/",DIRECTORY_SEPARATOR,$ignore));
        }
        $cbackup = Cache::get('backup');
        if ($cbackup!==null){
            if (unserialize($cbackup)['status']!='success'){
                return "";
            }
        }
        Cache::put('backup',serialize([
            'status'=>'start'
        ]),10);
        $isBackupDB = !$this->option('nodb');

        $dbFileName = $this->path.DIRECTORY_SEPARATOR."core/backup.sql";
        if ($isBackupDB){
            Cache::put('backup',serialize([
                'status'=>'database'
            ]),10);
            $execStr = Config::get('backup.mysqldump');
            $dbUsername = Config::get('database.connections.mysql.username');//DB::connection()->getConfig('username');
            $dbPassword = Config::get('database.connections.mysql.password');//DB::connection()->getConfig('password');
            $dbName = Config::get('database.connections.mysql.database');//DB::connection()->getConfig('database');
            if (!empty($dbUsername)){
                $execStr.=" -u ".$dbUsername;
            }
            if (!empty($dbPassword)){
                $execStr.=" -p".$dbPassword;
            }
            $execStr.=" ".$dbName;
            exec($execStr. " > ".$dbFileName);
        }
        Cache::put('backup',serialize([
            'status'=>'count'
        ]),10);
        $files = $this->getFiles('');
        $countFiles = count($files);
        Cache::put('backup',serialize([
            'status'=>'file',
            'count'=>$countFiles,
            'ready'=>0
        ]),10);
        $archive = new \ZipArchive();
        $archiveName = '/core/storage/backups/backup'.date('Ymd_His').'.zip';
        if (!$archive->open($this->path.$archiveName, \ZipArchive::CREATE)){
            return false;
        }
        $i = 0;
        foreach ($files as $file) {
            $i++;
            if (is_readable($this->path.$file)){
                $archive->addFile($this->path.$file, substr($file,1));
            }   else    {
                Log::warning('Backup. Can\'t read file: '.$file);
            }
            if ($i%10==0){
                $archive->close();
                $archive->open($this->path.$archiveName, \ZipArchive::CREATE);
                Cache::put('backup',serialize([
                    'status'=>'file',
                    'count'=>$countFiles,
                    'ready'=>$i
                ]),10);
            }
        }

        if ($isBackupDB && file_exists($dbFileName)) unlink($dbFileName);

        Cache::put('backup',serialize([
            'status'=>'file',
            'count'=>$countFiles,
            'ready'=>$countFiles,
        ]),10);

        $backupFiles = scandir($this->path.'/core/storage/backups');
        $backupFilesFilter = [];
        foreach($backupFiles as $backupFile){
            if ($backupFile!='.' && $backupFile!='..'){
                $backupFilesFilter[] = [
                    'filename'=>$backupFile,
                    'last_mod'=>stat($this->path.'/core/storage/backups/'.$backupFile)['ctime']
                ];
            }
        }
        usort($backupFilesFilter, function($f1,$f2){
            return $f1['last_mod']-$f2['last_mod'];
        });
        for($i = 0; $i<count($backupFilesFilter)-Config::get('backup.limit'); $i++){
            unlink($this->path.'/core/storage/backups/'.$backupFilesFilter[$i]['filename']);
        }
        Cache::put('backup',serialize([
            'status'=>'success',
            'count'=>$countFiles,
            'ready'=>$i,
        ]),10);
    }
}
