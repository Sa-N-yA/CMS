<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\HttpKernel\Tests\Fragment\Bar;
use DB;
use Config;
use File;
class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:install {email} {password} {siteName=""}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Apply parameters site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $email = base64_decode($this->argument('email'));
        $password = base64_decode($this->argument('password'));
        $siteName = base64_decode($this->argument('siteName'));
        $user = new \App\User();
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();
        if (!empty($siteName)) {
            $conf = Config::get('site');
            $conf['name'] = $siteName;
            $data = var_export($conf, true);
            File::put(base_path() . DIRECTORY_SEPARATOR. 'config'.DIRECTORY_SEPARATOR.'site.php', "<?php\n return $data ;");
        }
    }
}
