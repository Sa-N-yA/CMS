<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class File extends Model
{
    protected $table = 'files';

    public function folder(){
        return $this->belongsTo('App\\Folder', 'folder_id');
    }

    public function save(array $options = Array()){
        $v = Validator::make($this->attributes, [
            'name' => 'required|min:1|max:255',
            'filename' => 'required|min:1|max:255',
            'folder_id'=>'required|numeric|min:0'
        ]);
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}