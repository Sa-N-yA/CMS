<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Folder extends Model
{
    protected $table = 'folders';

    public function files(){
        return $this->hasMany('App\\File', 'folder_id');
    }

    public function subfolders(){
        return $this->hasMany('App\\Folder','parent_id');
    }

    public function save(array $options = Array()){
        $v = Validator::make($this->attributes, [
            'name' => 'required|min:1|max:255|unique:folders,name,NULL,id,parent_id,'.$this->parent_id,
            'parent_id'=>'required|numeric|min:0'
        ]);
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}
