<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class WebFormResultAnswer extends Model
{
    protected $table = 'webforms_result_answer';
    public $timestamps = false;

    public function result(){
        return $this->belongsTo('\\App\\WebFormResult', 'result_id');
    }

    public function field(){
        return $this->belongsTo('\\App\\WebFormField');
    }
}