<?php

namespace App;

use App\Http\Controllers\Core\Modules\WebFormsFields\WebFormsFieldController;
use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;

class WebFormResult extends Model
{
    protected $table = 'webforms_result';

    public function webform(){
        return $this->belongsTo('\\App\\WebForm', 'webform_id');
    }

    public function answers(){
        return $this->hasMany('\\App\\WebFormResultAnswer', 'result_id');
    }

    public function user(){
        return $this->belongsTo('\\App\\User');
    }

    public function save(array $options = Array()){
        //Валидаторы для основных свойств
        $validation = [];
        $validation['webform_id'] = "required|numeric|exists:webforms,id";

        $fields = WebFormField::where('webform_id',$this->webform_id)->get();

        //Делаем валидаторы для пользовательских свойств
        foreach($fields as $field){
            $prop_name = 'field_'.$field->alias;
            $prop_config = unserialize($field->config);
            $validation[$prop_name] = [];
            $_field = new WebFormsFieldController::$fields[$field->type];
            $fieldValidations = $_field->getFormValidation($prop_config);
            if ($field->required=='1'){
                $fieldValidations[] = 'required';
            }
            $validation[$prop_name] = implode('|', $fieldValidations);
        }
        //Проверяем валидацию
        $v = Validator::make($this->attributes, $validation);
        if ($v->fails()){
            return $v->errors();
        }
        //Удаляем свойства из объекта, что бы не было ошибки
        $fields_values = [];
        foreach($fields as $field) {
            $field_name = 'field_' . $field->alias;
            $fields_values['field_' . $field->alias] = $this->$field_name;
            unset($this->$field_name);
        }
        //Начинаем транзакцию
        DB::beginTransaction();

        if ($this->id) WebFormResultAnswer::where('result_id',$this->id)->delete();

        //Сохраняем раздел
        $s = parent::save($options);

        //Сохраняем свойства, если раздел сохранен успешно
        if ($s===true){
            foreach($fields as $field){
                $model = new WebFormResultAnswer();
                $model->result_id = $this->id;
                $model->field_id = $field->id;
                $model->value = $fields_values['field_'.$field->alias];
                if (!$model->save()){
                    DB::rollback();
                    return false;
                }
            }
            DB::commit();
            return true;
        }   else    {
            DB::rollback();
            return $s;
        }
    }
}