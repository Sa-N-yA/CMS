<?php
namespace App\Events;

use App\Http\Controllers\Core\CacheController;
use App\File;
use Cache;


class FileChanged extends Event{

    public function __construct(File $file)
    {
        Cache::forget('files_'.$file->id);
    }
}