<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;

class Group extends Model
{
    protected $table = "groups";
    public function gates(){
        return $this->belongsToMany('App\\Gate','gate_groups');
    }
    public function save(array $options = Array()){
        $validations = [
            'name' => 'required|min:3|max:255',
        ];
        foreach($this->attributes as $key=>$attr){
            if (strpos($key,'gate_')!==false){
                $validations[$key] = 'required|boolean';
            }
        }
        $v = Validator::make($this->attributes, $validations);
        if ($v->fails()) return $v->errors();

        DB::beginTransaction();
        if ($this->id) DB::table('gate_groups')->where('group_id',$this->id)->delete();

        $gates = [];
        foreach($this->attributes as $key=>$attr){
            if (strpos($key,'gate_')!==false){
                if ($attr=='1') $gates[] = str_replace('gate_','',$key);
                unset($this->$key);
            }
        }
        if (!parent::save()){
            DB::rollback();
            return false;
        }

        foreach($gates as $key=>$gate){
            DB::table('gate_groups')->insert(['group_id'=>$this->id, 'gate_id'=>$gate]);
        }

        DB::commit();
        return true;
    }
}
