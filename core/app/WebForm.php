<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class WebForm extends Model
{
    protected $table = 'webforms';

    public function fields(){
        return $this->hasMany('App\\WebFormField', 'webform_id');
    }

    public function results(){
        return $this->hasMany('App\\WebFormResult', 'webform_id');
    }

    public function save(array $options = Array()){
        $v = Validator::make($this->attributes, [
            'name' => 'required|min:3|max:255',
            'alias' => 'required|min:3|max:255|unique:webforms,alias'.(($this->id?','.$this->id:'')).'|regex:([a-zA-Z][a-zA-Z0-9_]*)',
            'email' => 'email'
        ]);
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}
