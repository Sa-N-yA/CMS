<?php

namespace App;

use App\Http\Controllers\Core\Modules\WebFormsFields\WebFormsFieldController;
use Illuminate\Database\Eloquent\Model;
use Validator;

class WebFormField extends Model
{
    protected $table = 'webforms_fields';
    public $timestamps = false;

    public function webform(){
        return $this->belongsTo('\\App\\WebForm', 'webform_id');
    }

    public function save(array $options = Array()){
        $type = $this->type;
        $validations = [
            'name' => 'required|min:3|max:255',
            'webform_id'=>'exists:webforms,id',
            'required'=>'required|in:0,1',
            'type' => 'required|in:'.implode(',',array_keys(WebFormsFieldController::$fields)),
            'alias' => 'required|min:3|max:255|unique:webforms_fields,alias'.(($this->id?','.$this->id:',null')).',id,webform_id,'.$this->webform_id.'|regex:([a-zA-Z][a-zA-Z0-9_]*)'
        ];

        if (isset(WebFormsFieldController::$fields[$type])){
            $field = new WebFormsFieldController::$fields[$type];
            $fieldValidations = $field->getCreateValidation();
            foreach($fieldValidations as $k=>$v) $validations[$k] = $v;
            $configs = $field->getConfig();
            $arConfig = [];
            foreach($configs as $config){
                $arConfig[$config] = $this->$config;
            }
            $this->config = serialize($arConfig);
        }


        $v = Validator::make($this->attributes, $validations);

        foreach(WebFormsFieldController::$fields as $field){
            $field = new $field;
            foreach($field->getConfig() as $t){
                unset($this->$t);
            }
        }
        if ($v->fails()) return $v->errors();
        return parent::save();
    }
}