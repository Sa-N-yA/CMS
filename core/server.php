<?php
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */
/*
$uri = urldecode(
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

// This file allows us to emulate Apache's "mod_rewrite" functionality from the
// built-in PHP web server. This provides a convenient way to test a Laravel
// application without having installed a "real" web server software here.
$file = __DIR__.'../public'.$uri;
if ($uri !== '/' && file_exists($file) && is_file($file)) {
//    mime_content_type(__DIR__.'/public'.$uri);
    $mimes = [
        'css'=>'text/css',
        'js'=>'text/js',
    ];
    $ext = pathinfo($file)['extension'];
    if (array_key_exists($ext, $mimes)) $mime = $mimes[$ext];
    if (empty($mime)) $mime = mime_content_type($file);
    header('Content-Type: '.$mime);
    readfile($file);
    exit;
}*/

require_once 'public/index.php';
